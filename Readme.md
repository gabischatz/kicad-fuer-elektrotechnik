<figure>
  <img src="images/KiCAD.png" />
  <figcaption>Logo für diese Seite</figcaption>
</figure>
        <figure>  
          <a href="https://youtu.be/Qe9RePLDURU"><img src="images/Netzklassen_erstellen.png" height="120"/></a>
          <h3><figcaption> YouTube, Netzklassen erstellen</figcaption></h3>
        </figure>
        <figure>  
          <a href="https://youtu.be/pbILxaTSk28"><img src="images/Netzklassen_zuweisen.png" height="120"/></a>
          <h3><figcaption> YouTube, Netzklassen zuweisen</figcaption></h3>
        </figure>
        <figure>  
          <a href="https://youtu.be/tfkk2taGlpY"><img src="images/Strompfad_erstellen.png" height="120"/></a>
          <h3><figcaption> YouTube, Strompfad erstellen</figcaption></h3>
        </figure>
        <figure>  
          <a href="https://youtu.be/CqX7qE8Y4kQ"><img src="images/Netzbezeichner_zuweisen.png" height="120"/></a>
          <h3><figcaption> YouTube, Netzbezeichner zuweisen</figcaption></h3>
        </figure>
        <figure>  
          <a href="https://youtu.be/982Ni5iHPnY"><img src="images/Kabel_mit_5_Adern_erstellen.png" height="120"/></a>
          <h3><figcaption> YouTube, Stromleiter als Kabel darstellen</figcaption></h3>
        </figure>
        <figure>  
          <a href="https://youtu.be/dzFFCHNpx20"><img src="images/Kabel_kennzeichnen.png" height="120"/></a>
          <h3><figcaption> YouTube, Kabel kennzeichnen, das drei äußere Leiter, einen Schutzleiter und einen Neutralleiter enthält.</figcaption></h3>
        </figure>
<table width="100%">
<thead>
<tr>
<th width="55%"><h1>IEC-Symbole</h1></th>
<th width="45%"><a id="user-content-iec-symbole"></th>
</tr>
</thead>
  <tbody>  
     <tr>
        <td><p>Standardmäßige elektrische IEC-Symbole, auch bekannt als IEC 60617 (britischer Standard BS 3939), werden zur Darstellung verschiedener Geräte verwendet, darunter Kontrollleuchten, Relais, Zeitgeber und Schalter zur Verwendung in elektrischen Schaltplänen.
</p><p> 
Ich biete Ihnen die Möglichkeit, Schaltpläne in KiCad zu erstellen, indem ich Ihnen meine selbstgezeichneten Symbole zur Verfügung stelle. Bitte beachten Sie, dass diese Symbole nicht zu 100 % dem Original entsprechen und möglicherweise nicht vollständig sind.
</p><p>
Sie haben die Möglichkeit, mir Ihre Symbole zuzusenden. Nach einer Überprüfung und gegebenenfalls notwendigen Anpassungen werde ich sie hier veröffentlichen. Ich freue mich auch über Hinweise zu fehlerhaften Symbolen und bitte um klare Angaben für die erforderlichen Korrekturen.</p> </td>
        <td><p>Standard IEC electrical symbols, also known as IEC 60617 (British Standard BS 3939), are used to represent various devices including indicator lights, relays, timers and switches for use in electrical circuit diagrams.
 </p><p>
I offer you the opportunity to create circuit diagrams in KiCad by providing you with my self-drawn symbols. Please note that these symbols are not 100% original and may not be complete.
</p><p>
You have the option of sending me your symbols. After checking and making any necessary adjustments, I will publish them here. I would also be happy to receive information about incorrect symbols and ask for clear information about the necessary corrections.</p>
        </td>
     </tr>
  <tr>
      <td>[Seitendesigns](../../public/Pages/Readme.md#seitendesigns)</td>
      <td>[Page designs](../../public/Pages/Readme.md#user-content-page-designs)</td>
  </tr>
  <tr>
      <td>[Mechanische und andere Stellteile](../../public/Control/Readme.md)</td>
      <td>[Mechanical and other controls](../../public/Control/Readme.md)</td>
  </tr>
  <tr>
      <td>[Fertige Schalter](../../public/Switch/Readme.md#user-content-fertige-schalterfinished-switches)</td>
      <td>[Finished switches](../../public/Switch/Readme.md#user-content-fertige-schalterfinished-switches)</td>
  </tr>
  <tr>
      <td>[Stromversorgung](../../public/Power_supply/Readme.md)</td>
      <td>[Power symbols](../../public/Power_supply/Readme.md)</td>
  </tr>
  <tr>
      <td>[Sicherungen](../../public/Fuses/Readme.md)</td>
      <td>[Fuses](../../public/Fuses/Readme.md)</td>
  </tr>
  <tr>
      <td>[Relais](../../public/Relays/Readme.md)</td>
      <td>[Relays](../../public/Relays/Readme.md)</td>
  </tr>
  <tr>
      <td>[Klemmen](../../public/Clamp/Readme.md)</td>
      <td>[Clamps](../../public/Clamp/Readme.md)</td>
  </tr>
  <tr>
      <td>[Diverses](../../public/Miscellaneous/Readme.md)</td>
      <td>[Miscellaneous](../../public/Miscellaneous/Readme.md)</td>
  </tr>
   <tr>
      <td>[Schütze](../../public/Protect/Readme.md)</td>
      <td>[Protect](../../public/Protect/Readme.md)</td>
  </tr>
   <tr>
      <td>[Messinstrumente](../../public/Measuring-instruments/Readme.md)</td>
      <td>[Measuring instruments](../../public/Measuring-instruments/Readme.md)</td>
  </tr>
   <tr>
      <td>[Motoren Generatoren](../../public/Motor_Generator/Readme.md)</td>
      <td>[Motore Generatore](../../public/Motor_Generator/Readme.md)</td>
  </tr>
   <tr>
      <td>[Transformatoren](../../public/Transformers/Readme.md)</td>
      <td>[Transformers](../../public/Transformers/Readme.md)</td>
  </tr>
<!--  <tr>
      <td>[](../../)</td>
      <td>[](../../)</td>
  </tr>-->
  <tr>
      <td>[Verteiler und Richtungskoppler](../../public/11.7 Verteiler und Richtungskoppler/Readme.md)</td>
      <td>[Répartiteurs et coupleurs directifs](../../public/11.7 Verteiler und Richtungskoppler/Readme.md)</td>
  </tr>
  <tr>
      <td>[Dosen](../../public/11.8 Dosen/Readme.md)</td>
      <td>[Dérivations d'usager et prises du réseau](../../public/11.8 Dosen/Readme.md)</td>
  </tr>
  <tr>
      <td>[Entzerrer und Dämpfungsglied](../../public/11.9 Entzerrer und Dämpfungsglied/Readme.md)</td>
      <td>[Égaliseurs et affaiblisseurs](../../public/11.9 Entzerrer und Dämpfungsglied/Readme.md)</td>
  </tr>
  <tr>
      <td>[Fern-Stromversorgungen](../../public/11.10 Fern-Stromversorgungen/Readme.md)</td>
      <td>[Dispositifs d'alimentation](../../public/11.10 Fern-Stromversorgungen/Readme.md)</td>
  </tr>
  <tr>
      <td>[Kennzeichnung für besondere Leiter](../../public/11.11 Kennzeichnung für besondere Leiter/Readme.md)</td>
      <td>[Identification de conducteurs particuliers](../../public/11.11 Kennzeichnung für besondere Leiter/Readme.md)</td>
  </tr>
  <tr>
      <td>[Einspeisungen](../../public/11.12 Einspeisungen/Readme.md)</td>
      <td>[Einspeisungen](../../public/11.12 Einspeisungen/Readme.md)</td>
  </tr>
   <tr>
      <td>[Steckdosen](../../public/11.13 Steckdosen/Readme.md)</td>
      <td>[Socles de prises de courant](../../public/11.13 Steckdosen/Readme.md)</td>
  </tr>
  <tr>
      <td>[Schalter](../../public/11.14 Schalter/Readme.md)</td>
      <td>[Interrupteurs](../../public/11.14 Schalter/Readme.md)</td>
  </tr>
  <tr>
      <td>[Auslässe und Installationen für Leuchten](../../public/11.15 Auslässe und Installationen für Leuchten/Readme.md)</td>
      <td>[Installations d'éclairage](../../public/11.15 Auslässe und Installationen für Leuchten/Readme.md)</td>
  </tr>
 <tr>
      <td>[Verschiedene Geräte](../../public/11.16 Verschiedene Geräte/Readme.md)</td>
      <td>[Appareils divers](../../public/11.16 Verschiedene Geräte/Readme.md)</td>
  </tr>
<tr>
      <td>[Fertigteile für Kabel-Verteilsysteme](../../public/11.17 Fertigteile für Kabel-Verteilsysteme/Readme.md)</td>
      <td>[Canalisations préfabriquées](../../public/11.17 Fertigteile für Kabel-Verteilsysteme/Readme.md)</td>
  </tr>

 <tr>
      <td>[Flugplatzbefeuerung und Anzeiger](../../public/11.18 Flugplatzbefeuerung und Anzeiger/Readme.md)</td>
      <td>[Feux et indicateurs de navigation pour aéroports](../../public/11.18 Flugplatzbefeuerung und Anzeiger/Readme.md)</td>
  </tr>
  <tbody>
</table>



