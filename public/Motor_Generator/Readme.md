<table width="100%">
    <thead>
        <tr>
            <th width="50%">
                <h1>Motore Generatore</h1>
            </th>
            <th width="50%">
                <h1>&ensp;</h1>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <p>Ein Elektromotor wandelt elektrische Energie in mechanische Bewegung um. Er besteht aus einem Rotor, der sich im Magnetfeld des stationären Stators dreht. Die Wechselwirkung erzeugt ein Drehmoment, das den Motor antreibt. Es gibt verschiedene Typen wie Gleichstrom- und Wechselstrommotoren, die in zahlreichen Anwendungen von Haushaltsgeräten bis zu industriellen Systemen verwendet werden. Elektromotoren sind effizient, vielseitig einsetzbar und lassen sich durch Steuerung und Regelung an unterschiedliche Anforderungen anpassen.</p>
            </td>
            <td>
                <p>An electric motor converts electrical energy into mechanical motion using the principle of electromagnetic induction. It consists of a rotor that rotates within a magnetic field generated by a stationary stator. This interaction produces torque, initiating the motor's rotation. Electric motors come in various types and find applications in a wide range of devices, from household appliances to industrial machinery. They are efficient, versatile, and can be controlled to adapt to different requirements.</p>
            </td>
        </tr>
        <tr>
            <td>[Zu den Symbolen](#start)</td>
            <td>[To the symbols](#start)</td>
        </tr>
        <tr>
            <td>
                <p>Es gibt verschiedene Arten von Gleichstrommotoren (DC-Motoren), die je nach Anwendungen und Konstruktionsmerkmalen unterschiedliche Eigenschaften aufweisen. Hier sind einige der häufigsten Arten von Gleichstrommotoren:</p>
                <ol>
                    <li>
                        <p>
                            <strong>Bürstenlose Gleichstrommotoren (BLDC-Motoren):</strong>
                        </p>
                        <ul>
                            <li>Diese Motoren verwenden Magnete und elektronische Steuerungen anstelle von Bürsten und Kommutatoren.</li>
                            <li>BLDC-Motoren sind wartungsfrei und weisen eine höhere Effizienz auf.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Bürstenbehaftete Gleichstrommotoren:</strong>
                        </p>
                        <ul>
                            <li>Dies sind die klassischen Gleichstrommotoren mit Bürsten und einem Kommutator.</li>
                            <li>Bürsten sorgen für den Kontakt zwischen dem Rotor und der Stromquelle, während der Kommutator die Stromrichtung ändert.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Schrittmotoren:</strong>
                        </p>
                        <ul>
                            <li>Schrittmotoren bewegen sich in diskreten Schritten und werden häufig in Anwendungen eingesetzt, bei denen präzise Positionierung erforderlich ist.</li>
                            <li>Sie werden in CNC-Maschinen, Druckern und Robotern verwendet.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Getriebemotoren:</strong>
                        </p>
                        <ul>
                            <li>Getriebemotoren integrieren eine mechanische Getriebeeinheit, um das Drehmoment zu erhöhen und die Geschwindigkeit zu verringern.</li>
                            <li>Diese Motoren werden in Anwendungen mit niedriger Drehzahl und hohem Drehmoment eingesetzt.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Linearmotoren:</strong>
                        </p>
                        <ul>
                            <li>Linearmotoren erzeugen eine lineare Bewegung anstelle einer rotierenden Bewegung.</li>
                            <li>Sie werden in Anwendungen wie Förderbändern, Schienenverkehrssystemen und Positioniersystemen eingesetzt.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Universalmotoren:</strong>
                        </p>
                        <ul>
                            <li>Universalmotoren können sowohl mit Gleichstrom als auch mit Wechselstrom betrieben werden.</li>
                            <li>Sie werden oft in Haushaltsgeräten wie Mixer und Staubsauger eingesetzt.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Kernlose Gleichstrommotoren:</strong>
                        </p>
                        <ul>
                            <li>Diese Motoren haben keinen herkömmlichen Eisenkern im Rotor, was zu einer leichteren Bauweise führt.</li>
                            <li>Sie werden in Anwendungen mit begrenztem Platzbedarf eingesetzt.</li>
                        </ul>
                    </li>
                </ol>
                <p>Jede Art von Gleichstrommotor hat ihre spezifischen Vor- und Nachteile, und die Auswahl hängt von den Anforderungen der jeweiligen Anwendung ab.</p>
            </td>
            <td>
                <p>There are various types of direct current motors (DC motors), each with different characteristics and applications. Here are some of the most common types of DC motors:</p>
                <ol>
                    <li>
                        <p>
                            <strong>Brushless DC Motors (BLDC Motors):</strong>
                        </p>
                        <ul>
                            <li>These motors use magnets and electronic controls instead of brushes and commutators.</li>
                            <li>BLDC motors are maintenance-free and have higher efficiency.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Brushed DC Motors:</strong>
                        </p>
                        <ul>
                            <li>These are classic DC motors with brushes and a commutator.</li>
                            <li>Brushes provide the contact between the rotor and the power source, while the commutator changes the direction of the current.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Stepper Motors:</strong>
                        </p>
                        <ul>
                            <li>Stepper motors move in discrete steps and are often used in applications requiring precise positioning.</li>
                            <li>They find applications in CNC machines, printers, and robotics.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Geared Motors:</strong>
                        </p>
                        <ul>
                            <li>Geared motors incorporate a mechanical gearbox to increase torque and reduce speed.</li>
                            <li>These motors are used in applications requiring low speed and high torque.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Linear Motors:</strong>
                        </p>
                        <ul>
                            <li>Linear motors produce linear motion instead of rotational motion.</li>
                            <li>They are used in applications such as conveyor belts, rail transport systems, and positioning systems.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Universal Motors:</strong>
                        </p>
                        <ul>
                            <li>Universal motors can operate on both direct current and alternating current.</li>
                            <li>They are often used in household appliances like mixers and vacuum cleaners.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Coreless DC Motors:</strong>
                        </p>
                        <ul>
                            <li>These motors lack a conventional iron core in the rotor, resulting in a lighter construction.</li>
                            <li>They are used in applications with limited space requirements.</li>
                        </ul>
                    </li>
                </ol>
                <p>Each type of DC motor has specific advantages and disadvantages, and the selection depends on the requirements of the particular application.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Es gibt verschiedene Arten von Wechselstrommotoren, die je nach Konstruktion und Anwendung unterschiedliche Merkmale aufweisen. Hier sind einige gängige Typen von Wechselstrommotoren:</p>
                <ol>
                    <li>
                        <p>
                            <strong>Einphasen-Induktionsmotor:</strong>
                        </p>
                        <ul>
                            <li>Dieser Motor ist für den Betrieb mit einphasigem Wechselstrom ausgelegt.</li>
                            <li>Wird häufig in Haushaltsgeräten wie Ventilatoren, Pumpen und kleinen Werkzeugen eingesetzt.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Dreiphasen-Induktionsmotor:</strong>
                        </p>
                        <ul>
                            <li>Arbeitet mit dreiphasigem Wechselstrom.</li>
                            <li>Weit verbreitet in industriellen Anwendungen, einschließlich Förderbändern, Pumpen, Kompressoren und Elektrofahrzeugantrieben.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Synchronmotor:</strong>
                        </p>
                        <ul>
                            <li>Synchronmotoren haben einen festen Bezug zur Frequenz des Wechselstroms.</li>
                            <li>Verwendet in Anwendungen, bei denen präzise Synchronisation erforderlich ist, wie beispielsweise in Uhren oder bestimmten industriellen Prozessen.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Bürstenloser Gleichstrommotor (BLDC):</strong>
                        </p>
                        <ul>
                            <li>Auch wenn es "Gleichstrom" im Namen hat, kann BLDC mit Wechselstrom betrieben werden.</li>
                            <li>Häufig in modernen Anwendungen wie Elektrofahrzeugantrieben und Lüftern aufgrund ihrer Effizienz und Zuverlässigkeit.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Universalmotor:</strong>
                        </p>
                        <ul>
                            <li>Kann mit Wechselstrom oder Gleichstrom betrieben werden.</li>
                            <li>Findet Verwendung in Haushaltsgeräten wie Staubsaugern, Bohrmaschinen und Küchenmaschinen.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Schrittmotor:</strong>
                        </p>
                        <ul>
                            <li>Speziell für präzise Positionierung entwickelt.</li>
                            <li>Wird in Anwendungen wie CNC-Maschinen, 3D-Druckern und Robotern eingesetzt.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Kondensatormotor:</strong>
                        </p>
                        <ul>
                            <li>Einphasenmotor mit zusätzlichem Kondensator, um ein Drehmoment zu erzeugen.</li>
                            <li>Oft in Lüftern, Pumpen und kleinen Antrieben verwendet.</li>
                        </ul>
                    </li>
                </ol>
                <p>Diese sind nur einige Beispiele, und es gibt viele weitere spezialisierte Arten von Wechselstrommotoren, die für bestimmte Anwendungen optimiert sind. Jeder Motortyp hat seine eigenen Vor- und Nachteile, abhängig von den Anforderungen der jeweiligen Anwendung.</p>
            </td>
            <td>
                <p>There are various types of alternating current (AC) motors, each with different characteristics based on their construction and intended applications. Here are some common types of AC motors:</p>
                <ol>
                    <li>
                        <p>
                            <strong>Single-Phase Induction Motor:</strong>
                        </p>
                        <ul>
                            <li>Designed to operate on single-phase AC power.</li>
                            <li>Commonly used in household appliances like fans, pumps, and small tools.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Three-Phase Induction Motor:</strong>
                        </p>
                        <ul>
                            <li>Operates on three-phase AC power.</li>
                            <li>Widely used in industrial applications, including conveyors, pumps, compressors, and electric vehicle drives.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Synchronous Motor:</strong>
                        </p>
                        <ul>
                            <li>Synchronous motors maintain a fixed relationship with the frequency of the AC power.</li>
                            <li>Used in applications requiring precise synchronization, such as clocks or specific industrial processes.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Brushless Direct Current Motor (BLDC):</strong>
                        </p>
                        <ul>
                            <li>Despite having "direct current" in its name, a BLDC motor can be powered by AC.</li>
                            <li>Common in modern applications like electric vehicle drives and fans due to their efficiency and reliability.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Universal Motor:</strong>
                        </p>
                        <ul>
                            <li>Can operate on both AC and DC power.</li>
                            <li>Found in household appliances like vacuum cleaners, drills, and kitchen appliances.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Stepper Motor:</strong>
                        </p>
                        <ul>
                            <li>Specifically designed for precise positioning.</li>
                            <li>Used in applications such as CNC machines, 3D printers, and robotics.</li>
                        </ul>
                    </li>
                    <li>
                        <p>
                            <strong>Capacitor Motor:</strong>
                        </p>
                        <ul>
                            <li>Single-phase motor with an additional capacitor to generate torque.</li>
                            <li>Often used in fans, pumps, and small drives.</li>
                        </ul>
                    </li>
                </ol>
                <p>These are just some examples, and there are many other specialized types of AC motors optimized for specific applications. Each motor type has its own advantages and disadvantages, depending on the requirements of the particular application.</p>
            </td>
        </tr>
    <tbody>
</table>
<p>
    
<a name="start" /><a href="https://youtu.be/AtlsFXeLMVY">
        <img src="images/Motor_PE_Pin_hinzufuegen.png" height="200"/>
        <br>YouTube, einen Motor einen PE kontakt hinzufügen</a>
</p>
    <ol>
        <li>
            <figure>
                <img src="images/DC_Motor.png" height="120"/>
                <h3>
                    <figcaption>
                        <a href="public/Motor_Generator/DC_Motor.kicad_sym">DC Motor</a>
                    </figcaption>
                </h3>
                <p></p>
                <h4></h4>
                <p></p>
            </figure>
        </li>
        <p>
            <a href="#top">Zum Anfang / above</a>
        <hr>
</p>
<li>
    <figure>
        <img src="images/AC_Motor.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/AC_Motor.kicad_sym">AC Motor</a>
            </figcaption>
        </h3>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/DC_Generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/DC_Generator.kicad_sym">DC Generator</a>
            </figcaption>
        </h3>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/AC_Generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/AC_Generator.kicad_sym">AC Generator</a>
            </figcaption>
        </h3>
        <p>Geringe Frequenz</p>
        <h4></h4>
        <p>Low frequency </p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Pulse_Generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Pulse_Generator.kicad_sym">Pulse Generator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4></h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Waveform_Generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Waveform_Generator.kicad_sym">Waveform Generator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4></h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/AC_generator_Medium_frequencies.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/AC_generator_Medium_frequencies.kicad_sym">AC Generator, Medium Frequenz</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>AC generator Medium frequencies</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Ideal_current_generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Ideal_current_generator.kicad_sym">Idealer Stromgenerator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>Ideal current generator</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Ideal_voltage_generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Ideal_voltage_generator.kicad_sym">Idealer Spannungsgenerator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>Ideal voltage generator</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Voltage_generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Voltage_generator.kicad_sym">Spannungsgenerator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>Voltage generator</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Exponential_current_source.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Exponential_current_source.kicad_sym">Exponentielle Stromquelle</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>Exponential current source</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Exponential_voltage_source.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Exponential_voltage_source.kicad_sym">Exponentielle Spannungsquelle</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>Exponential voltage source</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Motor_generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Motor_generator.kicad_sym">Motor–Generator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4></h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Non-rotating_AC_generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Non-rotating_AC_generator.kicad_sym">Nicht rotierender Wechselstromgenerator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>Non-rotating AC generator</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Photovoltaikgenerator_Solarzelle.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Photovoltaikgenerator_Solarzelle.kicad_sym">Photovoltaikgenerator Solarzelle</a>
            </figcaption>
        </h3>
        <p></p>
        <h4></h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Controlled_voltage_generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Controlled_voltage_generator.kicad_sym">Geregelter Spannungsgenerator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>Controlled voltage generator</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
    <figure>
        <img src="images/Controlled_current_generator.png" height="120"/>
        <h3>
            <figcaption>
                <a href="public/Motor_Generator/Controlled_current_generator.kicad_sym">Geregelter Stromgenerator</a>
            </figcaption>
        </h3>
        <p></p>
        <h4>Controlled current generator</h4>
        <p></p>
    </figure>
</li>
<p>
    <a href="#top">Zum Anfang / above</a>
<hr>
</p>
<li>
        <figure>  
        <img src="images/AC_Motor_3p.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/AC_Motor_3p.kicad_sym">AC Motor 3p</a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Stepper_motor.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Stepper_motor.kicad_sym">Schrittmotor</a></figcaption></h3>    
        <p></p>
        <h4>Stepper motor</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Schrittmotor.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Schrittmotor.kicad_sym">Schrittmotor</a></figcaption></h3>    
        <p></p>
        <h4>Stepper motor</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Linear_motor.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Linear_motor.kicad_sym">Linearmotor</a></figcaption></h3>    
        <p></p>
        <h4>Linear motor</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Linear_three-phase_motor_with_rotation_in_one_direction_only.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Linear_three-phase_motor_with_rotation_in_one_direction_only.kicad_sym">Linearer Drehstrommotor mit Drehung nur in eine Richtung</a></figcaption></h3>    
        <p></p>
        <h4>Linear three-phase motor with rotation in one direction only</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Three-phase_motor.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Three-phase_motor.kicad_sym">Drehstrommotor</a></figcaption></h3>    
        <p></p>
        <h4>Three-phase motor</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Three-phase_motor_PE.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Three-phase_motor_PE.kicad_sym">Drehstrommotor</a></figcaption></h3>    
        <p></p>
        <h4>Three-phase motor</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Three_phase_motor_PE.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Three_phase_motor_PE.kicad_sym">Drehstrommotor</a></figcaption></h3>    
        <p></p>
        <h4>Three_phase motor PE</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Three-phase_motor_N.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Three-phase_motor_N.kicad_sym">Drehstrommotor</a></figcaption></h3>    
        <p></p>
        <h4>Three-phase motor N</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Three-phase_motor_PE_N.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Three-phase_motor_PE_N.kicad_sym">Drehstrommotor</a></figcaption></h3>    
        <p></p>
        <h4>Three-phase motor PE N</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Three-phase_motor_PE_N_U_V_W.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Three-phase_motor_PE_N_U_V_W.kicad_sym">Drehstrommotor</a></figcaption></h3>    
        <p></p>
        <h4>Three-phase motor PE N U V W</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="images/Star-shaped_motor_with_automatic_start.png" height="120"/>
        <h3><figcaption><a href="public/Motor_Generator/Star-shaped_motor_with_automatic_start.kicad_sym">Sternmotor mit automatischem Start</a></figcaption></h3>    
        <p></p>
        <h4>Star-shaped motor with automatic start</h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<!--
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
<p><a href="#top">Zum Anfang / above</a><hr></p>-->
</ul>
