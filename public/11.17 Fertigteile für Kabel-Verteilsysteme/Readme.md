<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Fertigteile für Kabel-Verteilsysteme</h1></th>
<th width="50%"><h1>Canalisations préfabriquées</h1></th>
</tr>
</thead><tbody><!--
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> 
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.17</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-17-01.png" height="120"/>
        <h3><figcaption><a href="11-17-01.kicad_sym">Elektro-Installationskanal, allgemein</a></figcaption></h3>    
        <p>11-17-01</p>
        <h4>Élément droit, symbole général</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-02.png" height="120"/>
        <h3><figcaption><a href="11-17-02.kicad_sym">Elektro-Installationskanäle, zusammengesetzt</a></figcaption></h3>    
        <p>11-17-02</p>
        <h4>Élément droit assemblé</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-03.png" height="120"/>
        <h3><figcaption><a href="11-17-03.kicad_sym">Endabdeckung</a></figcaption></h3>    
        <p>11-17-03</p>
        <h4>Obturateur d'extrémité</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-04.png" height="120"/>
        <h3><figcaption><a href="11-17-04.kicad_sym">Winkelabzweig</a></figcaption></h3>    
        <p>11-17-04</p>
        <h4>Coude</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-05.png" height="120"/>
        <h3><figcaption><a href="11-17-05.kicad_sym">T-Abzweig</a></figcaption></h3>    
        <p>11-17-05</p>
        <h4>Té, branchement à trois voies</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-06.png" height="120"/>
        <h3><figcaption><a href="11-17-06.kicad_sym">Kreuzabzweig</a></figcaption></h3>    
        <p>11-17-06</p>
        <h4>Croix, branchement à quatre voies</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-07.png" height="120"/>
        <h3><figcaption><a href="11-17-07.kicad_sym">Kreuzung von zwei Verteilsystemen ohne
Verbindung</a></figcaption></h3>    
        <p>11-17-07</p>
        <h4>Croisement de deux canalisations sans branchement</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-08.png" height="120"/>
        <h3><figcaption><a href="11-17-08.kicad_sym">Kreuzung zweier unabhängiger Verteilsysteme</a></figcaption></h3>    
        <p>11-17-08</p>
        <h4>Croisement de deux canalisations indépendantes</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-09.png" height="120"/>
        <h3><figcaption><a href="11-17-09.kicad_sym">Ausgleichsstück, Länge einstellbar</a></figcaption></h3>    
        <p>11-17-09</p>
        <h4>Élément droit à longueur ajustable</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-10.png" height="120"/>
        <h3><figcaption><a href="11-17-10.kicad_sym">Elktro-Installationskanal mit interner Blockierung</a></figcaption></h3>    
        <p>11-17-10</p>
        <h4>Élément droit à blocage interne</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-11.png" height="120"/>
        <h3><figcaption><a href="11-17-11.kicad_sym">Ausdehnungsstück für Gehäuse</a></figcaption></h3>    
        <p>11-17-11</p>
        <h4>Élément de dilatation pour enveloppe</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-12.png" height="120"/>
        <h3><figcaption><a href="11-17-12.kicad_sym">Ausdehnungsstück für Leiter</a></figcaption></h3>    
        <p>11-17-12</p>
        <h4>Élément de dilatation pour conducteurs</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-13.png" height="120"/>
        <h3><figcaption><a href="11-17-13.kicad_sym">Ausdehnungsstück für Gehäuse und Leiter</a></figcaption></h3>    
        <p>11-17-13</p>
        <h4>Élément de dilatation pour enveloppe et conducteurs</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-14.png" height="120"/>
        <h3><figcaption><a href="11-17-14.kicad_sym">Flexibler Elektro-Installationskanal</a></figcaption></h3>    
        <p>11-17-14 </p>
        <h4>Élément flexible</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-15.png" height="120"/>
        <h3><figcaption><a href="11-17-15.kicad_sym">Reduzierstück</a></figcaption></h3>    
        <p>11-17-15</p>
        <h4>Élément de réduction</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-16.png" height="120"/>
        <h3><figcaption><a href="11-17-16.kicad_sym">Gerader Elektro-Installationskanal mit innerer Drucktrennwand</a></figcaption></h3>    
        <p>11-17-16</p>
        <h4>Élément droit avec traversée étanche,</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-17.png" height="120"/>
        <h3><figcaption><a href="11-17-17.kicad_sym">Phasenwender</a></figcaption></h3>    
        <p>11-17-17</p>
        <h4>Élément de permutation des conducteurs de phase</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-18.png" height="120"/>
        <h3><figcaption><a href="11-17-18.kicad_sym">Geräte-Einbaukasten</a></figcaption></h3>    
        <p>11-17-18</p>
        <h4>Coffret d'appareillage en ligne</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-19.png" height="120"/>
        <h3><figcaption><a href="11-17-19.kicad_sym">Gerader Elektro-Installationskanal mit Brandabschottung</a></figcaption></h3>    
        <p>11-17-19</p>
        <h4>Élément droit avec barrière interne coupe-feu</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-20.png" height="120"/>
        <h3><figcaption><a href="11-17-20.kicad_sym">Endeinspeisung</a></figcaption></h3>    
        <p>11-17-20</p>
        <h4>Élément d'alimentation d'extrémité</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-21.png" height="120"/>
        <h3><figcaption><a href="11-17-21.png">Mitteneinspeisung</a></figcaption></h3>    
        <p>11-17-21</p>
        <h4>Élément d'alimentation central</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-22.png" height="120"/>
        <h3><figcaption><a href="11-17-22.kicad_sym">Endeispeisung mit Geräte-Einbaukasten</a></figcaption></h3>    
        <p>11-17-22</p>
        <h4>Élément d'alimentation en extrémité avec coffret d'appareillage</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-23.png" height="120"/>
        <h3><figcaption><a href="11-17-23.kicad_sym">Mitteneinspeisung mit Gräte-Einbaukasten</a></figcaption></h3>    
        <p>11-17-23</p>
        <h4>Élément central d'alimentation avec coffret d'appareillage</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-24.png" height="120"/>
        <h3><figcaption><a href="11-17-24.kicad_sym">Gerader Elektro-Installationskanal mit festem Abzweig</a></figcaption></h3>    
        <p>11-17-24</p>
        <h4>Élément droit avec dérivation fixe</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-25.png" height="120"/>
        <h3><figcaption><a href="11-17-25.kicad_sym">Gerader Elektro-Installationskanal mit mehreren Abzweigen</a></figcaption></h3>    
        <p>11-17-25</p>
        <h4>Élément droit avec plusieurs dérivations</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-26.png" height="120"/>
        <h3><figcaption><a href="11-17-26.kicad_sym">Gerader Elektro-Installationskanal mit beweglichem Abzweig</a></figcaption></h3>    
        <p>11-17-26 </p>
        <h4>Élément droit avec dérivation déplaçable de façon continue</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-27.png" height="120"/>
        <h3><figcaption><a href="11-17-27.kicad_sym">Gerader Elektro-Installationskanal mit stufig (1m) einstellbarem Abzweig</a></figcaption></h3>    
        <p>11-17-27</p>
        <h4>Élément droit avec dérivation déplaçable par pas</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-28.png" height="120"/>
        <h3><figcaption><a href="11-17-28.kicad_sym">Gerader Elektro-Installationskanal mit Abzweigen durch beweglichen Kontakt</a></figcaption></h3>    
        <p>11-17-28</p>
        <h4>Élément droit avec dérivation par contact mobile, par exemple contact glissant</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-29.png" height="120"/>
        <h3><figcaption><a href="11-17-29.kicad_sym">Gerader Elektro-Installationskanal mit festem Abzweig und Geräte-Einbaukasten</a></figcaption></h3>    
        <p>11-17-29</p>
        <h4>Élément droit avec dérivation fixe comprenant un coffret d'appareillage</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-30.png" height="120"/>
        <h3><figcaption><a href="11-17-30.kicad_sym">Gerader Elektro-Installationskanal mit verstellbarem Abzweig und Geräte Einbaukasten</a></figcaption></h3>    
        <p>11-17-30</p>
        <h4>Élément droit avec dérivation déplacable comprenant un coffret d'appareillage</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-31.png" height="120"/>
        <h3><figcaption><a href="11-17-31.kicad_sym">Gerader Elektro-Installationskanal mit festem Abzweig und Geräte-Steckdose mit Schutzkontakt</a></figcaption></h3>    
        <p>11-17-31</p>
        <h4>Élément droit avec dérivation fixe comprenant un socle de prise de courant, avec contact pour conducteur de protection</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-32.png" height="120"/>
        <h3><figcaption><a href="11-17-32.kicad_sym">Gerader Elektro-Installationskanal, bestehend aus zwei Verdrahtungskanal-Systemen, A und B</a></figcaption></h3>    
        <p>11-17-32</p>
        <h4>Élément droit comprenant deux systèmes de canalisations A et B</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-33.png" height="120"/>
        <h3><figcaption><a href="11-17-33.kicad_sym">Gerader Elektro-Installationskanal, bestehend aus zwei Verdrahtungskanal-Systemen, A und B</a></figcaption></h3>    
        <p>11-17-33</p>
        <h4>Élément droit comprenant deux systèmes de canalisations A et B</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-34.png" height="120"/>
        <h3><figcaption><a href="11-17-34.kicad_sym">Gerader Elektro-Installationskanal, bestehend aus drei getrennten Kammern</a></figcaption></h3>    
        <p>11-17-34</p>
        <h4>Elément droit comprenant trois compartiments séparés</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-17-35.png" height="120"/>
        <h3><figcaption><a href="11-17-35.kicad_sym">Gerader Elektro-Installationskanal, bestehend aus drei getrennten Kammern</a></figcaption></h3>    
        <p>11-17-35</p>
        <h4>Elément droit comprenant trois compartiments séparés</h4>      
        <p></p>
        </figure>
</li> 
</ul>
