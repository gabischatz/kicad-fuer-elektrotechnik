<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Sicherungen</h1></th>
<th width="50%"><h1>Fuses</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
        <p>Elektrische Sicherungen sind Schutzelemente in elektrischen Schaltkreisen, die dazu dienen, elektrische Geräte und Leitungen vor Überlastung oder Kurzschlüssen zu schützen. Hier sind einige grundlegende Merkmale und Funktionen elektrischer Sicherungen:</p><ol><li><p><strong>Überlastschutz:</strong> Die Hauptfunktion einer elektrischen Sicherung besteht darin, elektrische Geräte und Leitungen vor Überlastung zu schützen. Dies geschieht, indem die Sicherung den Stromfluss unterbricht, wenn der Strom die zulässige Nennstromstärke überschreitet.</p></li><li><p><strong>Kurzschlussschutz:</strong> Elektrische Sicherungen reagieren auch auf Kurzschlüsse, indem sie den Stromkreis sofort unterbrechen. Dies verhindert Schäden an Geräten und Leitungen sowie das Risiko von Bränden.</p></li><li><p><strong>Arbeitsprinzip:</strong> Die meisten elektrischen Sicherungen basieren auf dem Prinzip der Schmelzsicherung. Dabei besteht die Sicherung aus einem schmelzbaren Draht oder einem anderen Material, das bei Überlastung oder Kurzschluss schmilzt und den Stromfluss unterbricht.</p></li><li><p><strong>Nennstrom:</strong> Jede Sicherung ist für einen bestimmten Nennstrom ausgelegt, der angibt, wie viel Strom sie über einen bestimmten Zeitraum tragen kann, ohne auszulösen. Dieser Nennstrom ist auf der Sicherung angegeben.</p></li><li><p><strong>Nennspannung:</strong> Die Nennspannung gibt an, für welche Spannung die Sicherung ausgelegt ist. Es ist wichtig sicherzustellen, dass die Sicherung mit der betreffenden Spannung kompatibel ist.</p></li><li><p><strong>Typen:</strong> Es gibt verschiedene Arten von Sicherungen, darunter Schmelzsicherungen, Leitungsschutzschalter (LS-Schalter), Leistungsschalter und Sicherungsautomaten. Jeder Typ hat spezifische Anwendungen und Funktionen.</p></li><li><p><strong>Wechselbarkeit:</strong> Einige Sicherungen sind austauschbar, während andere, insbesondere Schmelzsicherungen, nach dem Auslösen ersetzt werden müssen.</p></li><li><p><strong>Anwendungen:</strong> Elektrische Sicherungen werden in Wohn- und Gewerbegebäuden, Industrieanlagen, Fahrzeugen und vielen anderen Anwendungen eingesetzt, um elektrische Geräte und Installationen zu schützen.</p></li></ol><p>Es ist wichtig, die richtige Art und Größe der Sicherung gemäß den elektrischen Anforderungen und den örtlichen Vorschriften zu wählen, um einen effektiven Schutz zu gewährleisten.</p>
      </td>
      <td>
        <p>Electrical fuses are protective devices in electrical circuits designed to safeguard electrical devices and wiring from overload or short circuits. Here are some fundamental features and functions of electrical fuses:</p><ol><li><p><strong>Overload Protection:</strong> The primary function of an electrical fuse is to protect electrical devices and wiring from overload. This is achieved by the fuse interrupting the flow of current when it exceeds the rated current.</p></li><li><p><strong>Short Circuit Protection:</strong> Electrical fuses also respond to short circuits by quickly interrupting the circuit. This prevents damage to devices and wiring, as well as the risk of fires.</p></li><li><p><strong>Operating Principle:</strong> Most electrical fuses are based on the principle of a fusible link. This involves the fuse containing a fusible wire or other material that melts during overload or short circuit conditions, thereby breaking the circuit.</p></li><li><p><strong>Rated Current:</strong> Each fuse is designed for a specific rated current, indicating how much current it can carry for a particular time without tripping. This rated current is marked on the fuse.</p></li><li><p><strong>Rated Voltage:</strong> The rated voltage indicates the voltage for which the fuse is designed. It is crucial to ensure that the fuse is compatible with the applicable voltage.</p></li><li><p><strong>Working Voltage:</strong> Fuses are designed for specific working voltages, and it's important to use them within their specified voltage range.</p></li><li><p><strong>Types:</strong> There are various types of fuses, including cartridge fuses, circuit breakers, miniature circuit breakers (MCBs), and fuse links. Each type has specific applications and functions.</p></li><li><p><strong>Replaceability:</strong> Some fuses are replaceable, while others, especially fusible links, need to be replaced after tripping.</p></li><li><p><strong>Applications:</strong> Electrical fuses find applications in residential and commercial buildings, industrial facilities, vehicles, and various other settings to protect electrical devices and installations.</p></li></ol><p>It's essential to select the correct type and size of fuse according to electrical requirements and local regulations to ensure effective protection.</p>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2></h2>
<ul>
<li>
        <figure>  
        <img src="images/House_junction_box.png" height="120"/>
        <img src="images/Kabelhausanschlusskasten_KH00_nach_DIN_43627.png" title="Kabelhausanschlusskasten KH00 nach DIN 43627" height="120"/>
        <h3><figcaption><a href="public/Fuses/House_junction_box.kicad_sym">Haus Anschlußkasten</a></figcaption></h3>    
        <p>Der Hausanschlusskasten dient als zentraler Anschlusspunkt für die elektrische Versorgung des Gebäudes. Hier wird das Stromnetz des Energieversorgungsunternehmens mit der elektrischen Installation des Hauses verbunden. Dieser befindet sich vor der Hauptsicherung die vor dem Zähler im Zählerschrank installiert wird.</p>
        <p>Für die verschiedenen Netz-Systeme kann man durch doppel-klick die Pinbezeichnung ändern. Der nicht benötigte Pin im TT-System sollte als "Keine Verbindung" markiert werden.</p>
        <h4>House junction box</h4>      
        <p>The house connection box serves as a central connection point for the building's electrical supply. Here the power supply network of the energy supply company is connected to the electrical installation of the house. This is located in front of the main fuse, which is installed in front of the meter in the meter cabinet.</p><p>For the different network systems you can change the pin name by double-clicking. The unnecessary pin in the TT system should be marked as "No connection".</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Main_fuse_SLS_DSH.png" height="120"/>
        <img src="images/Hager_HTS363E_SLS-Schalter.png" title="Hager HTS363E SLS-Schalter" height="120"/>
        <img src="images/DEHN_DSH_ZP_B2_SG_TT_255_Kombiableiter.png" title="DEHN DSH ZP B2 SG TT 255 Kombiableiter" height="120"/>
        <h3><figcaption><a href="public/Fuses/Main_fuse_SLS_DSH.kicad_sym">Hauptsicherung SLS Dehn DSH</a></figcaption></h3>    
        <p></p>
        <h4>Main fuse SLS Dehn DSH</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Main_fuse_SLS.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Main_fuse_SLS.kicad_sym">Hauptsicherung SLS</a></figcaption></h3>    
        <p></p>
        <h4>Main fuse SLS</h4>      
        <p><a href="https://youtu.be/UGPfal_kYQ4"><img src="images/Main_fuse_SLS_YouTube.png" height="200"/><br> YouTube, Symbol "Main fuse SLS" einfügen und diverse Einstellungen vornehmen.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/LS_switch.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/LS_switch.kicad_sym">LS-Schalter</a></figcaption></h3>    
        <p></p>
        <h4>LS switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Circuit_breaker_with_thermal_and_magnetic_overcurrent_release.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/Circuit_breaker_with_thermal_and_magnetic_overcurrent_release.kicad_sym">Schutzschalter mit thermischer und magnetischer Überstromauslösung</a></figcaption></h3>    
        <p></p>
        <h4>Circuit breaker with thermal and magnetic overcurrent release</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Circuit_breaker.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/Circuit_breaker.kicad_sym">Sicherungsschalter</a></figcaption></h3>    
        <p></p>
        <h4>Circuit breaker</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Fuse_disconnect_switch.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/Fuse_disconnect_switch.kicad_sym">Sicherungstrennschalter</a></figcaption></h3>    
        <p></p>
        <h4>Fuse disconnect switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Fuse_switch_disconnector.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/Fuse_switch_disconnector.kicad_sym">Sicherungslasttrennschalter</a></figcaption></h3>    
        <p></p>
        <h4>Fuse switch disconnector</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/NH_fuse.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/NH_fuse.kicad_sym">NH-Sicherung</a></figcaption></h3>    
        <p></p>
        <h4>NH fuse</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Surge_arresters.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/Surge_arresters.kicad_sym">Überspannungsableiter</a></figcaption></h3>    
        <p></p>
        <h4>Surge arresters</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Motor_protection_switch.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/Motor_protection_switch.kicad_sym">Motorschutzschalter</a></figcaption></h3>    
        <p></p>
        <h4>Motor protection switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Motor_protection_relay.png" height="120"/>
        <h3><figcaption><a href="public/Fuses/Motor_protection_relay.kicad_sym">Motorschutzrelais</a></figcaption></h3>    
        <p></p>
        <h4>Motor protection relay</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> <!--
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> -->
</ul>
