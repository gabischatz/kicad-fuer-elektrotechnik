<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Verschiedene Geräte</h1></th>
<th width="50%"><h1>Appareils divers</h1></th>
</tr>
</thead><tbody> <!--
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr>
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.16</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-16-01.png" height="120"/>
        <h3><figcaption><a href="11-16-01.kicad_sym">Heißwassergerät</a></figcaption></h3>    
        <p>11-16-01</p>
        <h4>Chauffe-eau</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-16-02.png" height="120"/>
        <h3><figcaption><a href="11-16-02.kicad_sym">Ventilator</a></figcaption></h3>    
        <p>11-16-02</p>
        <h4>Ventilateur</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-16-03.png" height="120"/>
        <h3><figcaption><a href="11-16-03.kicad_sym">Zeiterfassungsgerät</a></figcaption></h3>    
        <p>11-16-03</p>
        <h4>Horloge de pointage, Enregistreur horaire</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-16-04.png" height="120"/>
        <h3><figcaption><a href="11-16-04.kicad_sym">Türöffner</a></figcaption></h3>    
        <p>11-16-04</p>
        <h4>Gâche électrique</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-16-05.png" height="120"/>
        <h3><figcaption><a href="11-16-05.kicad_sym">Wechselsprechstelle, z.B. Haus- oder Torsprechstelle</a></figcaption></h3>    
        <p>11-16-05</p>
        <h4>Interphone, par exemple: portier audio d'immeuble</h4>      
        <p></p>
        </figure>
</li> 
</ul>
