<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Messinstrumente</h1></th>
<th width="50%"><h1>Measuring instruments</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
        <figure>  
        <p><a href="https://youtu.be/TECJr_i887M"><img src="images/Messinstrumente_drehen.png" height="200"/></a></p>
        <h3><figcaption>YouTube, Messinstrumente drehen</figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
[Zum Anfang](#top)/[above](#top)<hr> 
<h2></h2>
<ul>
<li>
        <figure>  
        <img src="images/Voltmeter.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Voltmeter.kicad_sym">Spannungsmessgerät</a></figcaption></h3>    
        <p></p>
        <h4>Voltmeter</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Amperemeter.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Amperemeter.kicad_sym">Strommessgerät</a></figcaption></h3>    
        <p></p>
        <h4>Amperemeter</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Wattmeter.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Wattmeter.kicad_sym">Wirkleistungsmesser</a></figcaption></h3>    
        <p></p>
        <h4>Wattmeter</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Varmeter.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Varmeter.kicad_sym">Blindleistungsmesser</a></figcaption></h3>    
        <p></p>
        <h4>Varmeter</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Cos_Phi_Meter.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/cos_phi_meter.kicad_sym">Leistungsfaktoranzeige</a></figcaption></h3>    
        <p></p>
        <h4>Cos Phi – Meter</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Frequenzmesser.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Frequenzmesser.kicad_sym">Frequenzmesser</a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Thermometer.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Thermometer.kicad_sym">Wärmemesser</a></figcaption></h3>    
        <p></p>
        <h4>Thermometer</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Thermometer1.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Thermometer1.kicad_sym">Wärmemesser</a></figcaption></h3>    
        <p></p>
        <h4>Thermometer</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Thermometer_Fahrenheit.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Thermometer_Fahrenheit.kicad_sym"> Fahrenheit</a></figcaption></h3>    
        <p></p>
        <h4>Thermometer Fahrenheit</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Thermometer_Kelvin.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Thermometer_Kelvin.kicad_sym">Wärmemesser Kelvin</a></figcaption></h3>    
        <p></p>
        <h4>Thermometer Kelvin</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Thermometer_theta.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Thermometer_theta.kicad_sym">Wärmemesser</a></figcaption></h3>    
        <p>In der Physik und Mathematik wird "theta" manchmal zur Darstellung von Temperaturen verwendet. Es ist wichtig zu beachten, dass dies nicht das offizielle Symbol für Temperatureinheiten ist, sondern eher in bestimmten Kontexten verwendet wird. Normalerweise werden für Temperaturen Einheiten wie Celsius (°C) oder Kelvin (K) verwendet.</p>
        <h4>Thermometer</h4>      
        <p>In physics and mathematics, "theta" is sometimes used to represent temperatures. It is important to note that this is not the official symbol for temperature units, but rather is used in specific contexts. Typically, units such as Celsius (°C) or Kelvin (K) are used for temperatures.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Tachometer.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Tachometer.kicad_sym">Drehzahlmesser</a></figcaption></h3>    
        <p></p>
        <h4>Tachometer</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Oscilloscope.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Oscilloscope.kicad_sym">Oszilloskop</a></figcaption></h3>    
        <p></p>
        <h4>Oscilloscope</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Galvanometer.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Galvanometer.kicad_sym">Galvanometer</a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <p><a href="https://youtu.be/b5Mhg-OXId8"><img src="images/Eine_Uhr_drehen.png" height="200"/></a></p>
        <h3><figcaption>YouTube, Eine Uhr drehen</figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clock.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Clock.kicad_sym">Uhr</a></figcaption></h3>    
        <p></p>
        <h4>Clock</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Mothers_watch.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Mothers_watch.kicad_sym">Mutteruhr</a></figcaption></h3>    
        <p></p>
        <h4>Mother's watch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Timer.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Timer.kicad_sym">Schaltuhr</a></figcaption></h3>    
        <p></p>
        <h4>Timer</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Fernmesssender.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Telemetry_transmitter.kicad_sym">Fernmesssender</a></figcaption></h3>    
        <p></p>
        <h4>Telemetry transmitter</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Telemetry_receiver.png" height="120"/>
        <h3><figcaption><a href="public/Measuring-instruments/Telemetry_receiver.kicad_sym">Fernmessempfänger</a></figcaption></h3>    
        <p></p>
        <h4>Telemetry receiver</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> <!--
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> -->
</ul>
