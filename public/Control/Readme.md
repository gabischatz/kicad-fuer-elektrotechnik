<h1>Mechanische und andere Stellteile/Mechanical and other controls</h1>
<p>Diese mechanischen und anderen Stellteile spielen eine entscheidende Rolle bei der Steuerung und Regelung von elektrischen Systemen, wobei die mechanischen Komponenten oft in Kombination mit elektronischen Schaltungen verwendet werden, um bestimmte Funktionen zu realisieren.</p>
<p>These mechanical and other actuators play a crucial role in the control and regulation of electrical systems, with the mechanical components often being used in combination with electronic circuits to realize certain functions.</p>
[Fertige Schalter](../../public/Switch/Readme.md#user-content-fertige-schalterfinished-switches)/[Finished switches](../../public/Switch/Readme.md#user-content-fertige-schalterfinished-switches)
<h2>Schaltzeichen für algemeine Anwendung<br>Other Symbols Having General Application</h2>
<ul><li>
        <figure>  
          <img src="images/02-12-01.png" height="120"/>
          <h3><figcaption><a href="public/Control/02-12-01.kicad_sym">Wirkverbindung</a></figcaption></h3>
<p> -mechanisch, -pneumatisch, -hydraulisch, -optisch, -funktional. Die Länge des Wirkverbindungssymbols darf demLayout des Schaltplans angepaßt werden.</p>
<h4>Active connection</h4>         
<p> -mechanical, -pneumatic, -hydraulic, -optical, -functional. The length of the active connection symbol can be adapted to the layout of the circuit diagram.</p> 
        </figure>
    </li> 
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
        <figure>  
          <img src="images/02-12-02.png" height="120"/>
          <h3><figcaption><a href="public/Control/02-12-02.kicad_sym">Mechanische Wirkung</a></figcaption></h3>
<p>Mechanische Wirkung mit Angabe der Richtung von Kraft und Bewegung</p>
<h4>Mechanical action</h4>   
<p>Mechanical action indicating the direction of force and movement</p>       
        </figure>
    </li> 
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
        <figure>  
          <img src="images/02-12-03.png" height="120"/>
          <h3><figcaption><a href="public/Control/02-12-03.kicad_sym">Mechanische Wirkverbindung mit Angabe der Drehrichtung.</a></figcaption></h3>
<p>Es ist davon auszugehen, daß der Pfeil im Vordergrund des Wirkverbindungssymbols plaziert ist.</p>
<h4>Mechanical active connection with indication of the direction of rotation.</h4>          
<p>It can be assumed that the arrow is placed in the foreground of the active connection symbol.</p>       
 </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/02-12-04.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-04.kicad_sym">Platzmangel Schaltzeichen</a></figcaption></h3>    
        <p>Dieses Schaltzeichen muß angewendet werden,wenn aus Platzmangel das Schaltzeichen 02-12-01 nicht angewendet werden kann.</p>
        <h4>Lack of space for the circuit symbol</h4>      
        <p>This circuit symbol must be used if the circuit symbol 02-12-01 cannot be used due to lack of space.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>    
<li>
        <figure>  
        <img src="images/02-12-05.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-05.kicad_sym">Verzögerte Wirkung</a></figcaption></h3>    
        <p>Wirkung ist verzögert, wenn die Bewegung vom Bogen zu dessen Mittelpunkt gerichtet ist (Fallschirmwirkung).</p>
        <h4>Delayed effect</h4>      
        <p>The effect is delayed if the movement is directed from the arc to its center (parachute effect).</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-06.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-06.kicad_sym">Verzögerte Aktion</a></figcaption></h3>    
        <p></p>
        <h4>Delayed action</h4>      
        <p>Delayed action
It delays as the movement occurs toward the center of the arc</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-07.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-07.kicad_sym">Selbsttätiger Rückgang</a></figcaption></h3>    
        <p>Die Spitze des Dreiecks muss in Richtung des Rückgangs zeigen.</p>
        <h4>Automatic decline</h4>      
        <p>The tip of the triangle must point in the direction of the decline.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-08.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-08.kicad_sym">Raste Nicht selbsttätiger Rückgang</a></figcaption></h3>    
        <p>Enrichtung zum Beibhalten einer gegebenen Stellung</p>
        <h4>Rest Non-automatic return</h4>      
        <p>Device for maintaining a given position</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-09.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-09.kicad_sym">Raste, Nicht eingerastet</a></figcaption></h3>    
        <h4>Latched, Not latched</h4>      
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-10.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-10.kicad_sym">Raste, eingerastet</a></figcaption></h3>    
        <h4>Latched, latched</h4>      
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-11.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-11.kicad_sym">Mechanische Verrigelung zweier Geräte</a></figcaption></h3>    
        <h4>Mechanical interlocking of two devices</h4>    
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-12.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-12.kicad_sym">Sperre, nicht verrigelt</a></figcaption></h3> 
        <h4>Lock, not locked</h4> 
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-13.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-13.kicad_sym">Sperre verrigelt</a></figcaption></h3> 
        <h4>Lock locked</h4>      
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-14.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-14.kicad_sym">Blockiereinrichtung</a></figcaption></h3>    
        <p></p>
        <h4>Blocking device</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-15.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-15.kicad_sym">Blockiereinrichtung, verklinkt</a></figcaption></h3>    
        <p> Bewegung nach links ist blockiert.</p>
        <h4>Blocking device, linked </h4>      
        <p>Movement to the left is blocked.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-16.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-16.kicad_sym">Kupplung Mechanische Koppelung</a></figcaption></h3>
        <h4>Coupling Mechanical coupling</h4>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-17.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-17.kicad_sym">Kupplung, gelöst</a></figcaption></h3> 
        <h4>Mechanical coupling, disengaged</h4>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-18.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-18.kicad_sym">Mechanische Kupplung, eingekuppelt</a></figcaption></h3>
        <h4>Mechanical coupling, engaged</h4> 
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-19.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-19.kicad_sym">Kupplung für Mitnahme in eine Drehrichtung</a></figcaption></h3>   
        <h4>Unidirectional coupling device for rotation Free wheel</h4> 
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-20.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-20.kicad_sym">Bremse</a></figcaption></h3>   
        <h4>Brake</h4>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-21.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-21.kicad_sym">Elektro Motor mit eingelegter Bremse </a></figcaption></h3>    
        <h4>Electric motor with brake applied.</h4>      
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-22.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-22.kicad_sym">Elektro Motor mit gelöster Bremse </a></figcaption></h3> 
        <h4>Electric motor with brake released</h4>  
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-12-23.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-12-23.kicad_sym">Getriebe</a></figcaption></h3>    
        <p></p>
        <h4>Gearing</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-01.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-01.kicad_sym">Handantrieb, algemein</a></figcaption></h3>    
        <p></p>
        <h4>Manual actuator, general symbol</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-02.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-02.kicad_sym">Handantrieb mit beschränktem Zugriff</a></figcaption></h3>    
        <p></p>
        <h4>Manual actuator protected against unintentional operation</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-03.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-03.kicad_sym">Bestätigng durch Ziehen</a></figcaption></h3>    
        <p></p>
        <h4>Operated by pulling</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-04.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-04.kicad_sym">Bestätigng durch Drehen</a></figcaption></h3>    
        <p></p>
        <h4>Operated by turning</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-05.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-05.kicad_sym">Bestätigng durch Drücken</a></figcaption></h3>    
        <p></p>
        <h4>Operated by pushing</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-06.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-06.kicad_sym">Bestätigng durch Annähern</a></figcaption></h3>    
        <p></p>
        <h4>Operated by proximity effect</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-07.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-07.kicad_sym">Bestätigng durch Berühren</a></figcaption></h3>    
        <p></p>
        <h4>Operated by toching</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-08.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-08.kicad_sym">Notschalter, Typ "Pilz-Notdrucktaster"</a></figcaption></h3>    
        <p></p>
        <h4>Emergency actuator, type "mushroom-head"</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-09.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-09.kicad_sym">Bestätigung durch Handrad</a></figcaption></h3>    
        <p></p>
        <h4>Operated bei handwheel</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-10.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-10.kicad_sym">Bestätigung durch Pedal</a></figcaption></h3>    
        <p></p>
        <h4>Operated by pedal</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-11.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-11.kicad_sym">Bestätigung durch Hebel</a></figcaption></h3>    
        <p></p>
        <h4>Operated by lever</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-12.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-12.kicad_sym">Bestätigng durch abnehmbaren Griff</a></figcaption></h3>    
        <p></p>
        <h4>Operated by removable handle</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-13.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-13.kicad_sym">Bestätigng durch Schlüssel</a></figcaption></h3>    
        <p></p>
        <h4>Operated by removable key</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-14.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-14.kicad_sym">Bestätigng durch Kurbel</a></figcaption></h3>    
        <p></p>
        <h4>Operated by crank</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-15.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-15.kicad_sym">Bestätigng durch Rolle</a></figcaption></h3>    
        <p></p>
        <h4>Operated by roller</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-16.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-16.kicad_sym">Bestätigng durch Nocken</a></figcaption></h3>    
        <p>Nocken und Nockenscheibe dürfen im Profil detailliert dargestellt sein.</p>
        <h4>Operated by cam</h4>      
        <p> If desired, amore detailed drawing of the cam may be show. This applies also to a profile plate.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-19.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-19.kicad_sym">Bestätigng durch Nocken und Rolle</a></figcaption></h3>    
        <p></p>
        <h4>Operated by cam and roller</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-20.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-20.kicad_sym">Kraftantrieb, allgemein</a></figcaption></h3>    
        <p>Betätigung durch gespeicherte mechanische Energie. Information, die die Art der gespeicherten Energie zeigt. darf in das Quadrat eingezeichnet werden. </p>
        <h4>Operated by stored mechanical energy</h4>      
        <p>Information showing the from of stored energy may be added in the square.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-21.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-21.kicad_sym">Ausgelöst durch pneumatische oder hydraulische Kraft in Pfeilrichtung</a></figcaption></h3>    
        <p></p>
        <h4>Actuated by pneumatic or hydraulic power, single acting</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-22.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-22.kicad_sym">Ausgelöst durch pneumatische oder hydraulische Kraft in beide Richtungen</a></figcaption></h3>    
        <p></p>
        <h4>Actuated by pneumatic or hydraulic power, double acting</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-23.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-23.kicad_sym">Ausgelöst durch elektromagnetischen Effekt</a></figcaption></h3>    
        <p></p>
        <h4>Actuated by electromagnetic effect</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-24.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-24.kicad_sym">Ausgelöst durch elektromagnetisches Gerät z.B. Überstromschutz</a></figcaption></h3>    
        <p></p>
        <h4>Actuated by electromagnetic device, for example for protection against overcurrent</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-25.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-25.kicad_sym">Ausgelöst durch thermisches Gerät z.B. Überstromschutz</a></figcaption></h3>    
        <p></p>
        <h4>Actuated by thermal device, for example for protection against overcurrent</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-26.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-26.kicad_sym">Betätigung durch Elektro-Motor</a></figcaption></h3>    
        <p></p>
        <h4>Operated bei electric motor</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-27.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-27.kicad_sym">Betätigung durch elektrische Uhr</a></figcaption></h3>    
        <p></p>
        <h4>Operated bei electric clock</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-13-28.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-13-28.kicad_sym">Halbleiter-Antrieb</a></figcaption></h3>    
        <p></p>
        <h4>Semiconductor actuator</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-14-01.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-14-01.kicad_sym">Auslöser durch Flüssigkeits-Pegel</a></figcaption></h3>    
        <p></p>
        <h4>Actuator by liquid level</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-14-02.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-14-02.kicad_sym">Auslöser durch Zähler</a></figcaption></h3>    
        <p></p>
        <h4>Actuator by a counter</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-14-03.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-14-03.kicad_sym">Auslöser durch Strömung</a></figcaption></h3>    
        <p></p>
        <h4>Actuator by fluid flow</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-14-04.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-14-04.kicad_sym">Auslöser durch Gas-Strömung</a></figcaption></h3>    
        <p></p>
        <h4>Actuator by gas flow</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/02-14-05.png" height="120"/>
        <h3><figcaption><a href="public/Control/02-14-05.kicad_sym">Auslöst durch relative Feuchte</a></figcaption></h3>    
        <p></p>
        <h4>Actuator by relative huidity</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  </ul>

