<table width="100%">
<thead>
<tr>
<th width="55%"><h1>Schalter</h1></th>
<th width="45%"><h1>Switch</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
<p>In der Elektrotechnik fungiert ein Schalter als elektrisches Bauteil, das einen Stromkreis unterbrechen kann und in der Lage ist, den Stromfluss zu unterbrechen oder von einem Leiter auf einen anderen umzuleiten. Die gängigste Form eines Schalters ist ein manuell betätigtes elektromechanisches Gerät mit einer oder mehreren Gruppen elektrischer Kontakte, die mit externen Schaltkreisen verbunden sind.</p>
<p>Ein Schalter kann von einer Person direkt als Steuersignal für ein System verwendet werden, beispielsweise als Lichtschalter, um den Stromfluss in einem Stromkreis zu steuern. Automatisch betätigte Schalter können zur Steuerung von Maschinenbewegungen eingesetzt werden, um beispielsweise anzuzeigen, dass ein Garagentor vollständig geöffnet ist oder dass eine Werkzeugmaschine bereit ist, ein neues Werkstück aufzunehmen.</p>
<p>Schalter können durch Prozessvariablen wie Druck, Temperatur, Durchfluss, Strom, Spannung und Kraft betätigt werden. Sie fungieren als Sensoren in einem Prozess und werden zur automatischen Steuerung eines Systems verwendet. Ein Beispiel hierfür ist ein thermostatisch gesteuerter Schalter, auch bekannt als Thermostat, der zur Regelung eines Heizvorgangs verwendet wird. Ein Schalter, der von einem anderen Stromkreis betätigt wird, wird als Relais bezeichnet.<br>[Quelle: Schalter, Wikipedia]</p></td>
      <td>
<p>In electrical engineering, a switch acts as an electrical component that can break a circuit and is able to interrupt the flow of current or redirect it from one conductor to another. The most common form of a switch is a manually operated electromechanical device with one or more groups of electrical contacts connected to external circuits.</p>
<p>A switch can be used by a person directly as a control signal for a system, such as a light switch to control the flow of electricity in a circuit. Automatically operated switches can be used to control machine movements, such as indicating that a garage door is fully open or that a machine tool is ready to pick up a new workpiece.</p>
<p>Switches can be operated by process variables such as pressure, temperature, flow, current, voltage and force. They act as sensors in a process and are used to automatically control a system. An example of this is a thermostatically controlled switch, also known as a thermostat, which is used to control a heating process. A switch that is operated by another circuit is called a relay.<br>[Source: Switch, Wikipedia]</p>
</td>
  </tr>
  <tr>
      <td>[Fertige Schalter](#user-content-fertige-schalterfinished-switches)</td>
      <td>[Finished switches](#user-content-fertige-schalterfinished-switches)</td>
  </tr>
  <tbody>
</table>
<h2>Fertige Schalter / Finished switches</h2>
<p><a href="https://youtu.be/HuSfteyt5PA"><img src="images/Switch_operated.png" height="200"/><br> YouTube Einen Öffner betätigt erstellen.</p>
<ol>
<li>
        <figure>  
        <img src="images/operated.png" height="120"/>
        <h3><figcaption><a href="public/Switch/operated.kicad_sym">betätigt</a></figcaption></h3>    
        <p></p>
        <h4>operated</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_SPST_On.png" height="120"/>
        <h3><figcaption><a href="public/Control/Switch_SPST_On.kicad_sym">SPST Schalter An</a></figcaption></h3>    
        <p>Ein einpoliger Single-Throw-Schalter (SPST) steuert den Stromfluss in einem einzigen Pfad mit einem Eingangs- und einem Ausgangsanschluss. Wenn SPST geschlossen ist, ist der Stromkreis geschlossen und der Strom kann durchfließen. Andererseits wird der Stromkreis unterbrochen, wenn SPST geöffnet ist, wodurch der Stromfluss verhindert wird.
</p><p>
Der SPST-Schalter wird häufig für einfache Ein-/Aus-Anwendungen verwendet, bei denen eine einfache Steuerung des Stromkreises erforderlich ist. In elektrischen Schaltplänen wird er durch eine einzelne Linie dargestellt, die den Strompfad angibt, wenn der Schalter geschlossen ist.</p>
        <h4>Switch SPST On</h4>      
        <p>A single-pole, single-throw (SPST) switch controls the current flow in a single path, with one input and one output connection. When SPST is closed, the circuit is complete, allowing the current to flow through. On the other hand, the circuit is interrupted when SPST is open, preventing the flow of the current.
</p><p>
The SPST switch is commonly used for basic on/off applications, where simple control over the circuit is required. It is represented by a single line in electrical circuit diagrams, indicating the current path when the switch is closed.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_SPST_Off.png" height="120"/>
        <h3><figcaption><a href="public/Control/Switch_SPST_Off.kicad_sym">SPST Schalter Aus</a></figcaption></h3>    
        <p></p>
        <h4>Switch SPST Off</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_SPST_On_Circuit_breaker.png" height="120"/>
        <h3><figcaption><a href="public/Control/Switch_SPST_On_Circuit_breaker.kicad_sym">SPST Leistungsschalter An </a></figcaption></h3>    
        <p></p>
        <h4>SPST-switch On Circuit breaker</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_SPST_Off_Circuit_breaker.png" height="120"/>
        <h3><figcaption><a href="public/Control/Switch_SPST_Off_Circuit_breaker.kicad_sym">SPST Leistungsschalter Aus </a></figcaption></h3>    
        <p></p>
        <h4>SPST-switch Off Circuit breaker</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> <!-- 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  -->
<li>
        <figure>  
        <img src="images/Switch_SPST_Off_disconnector.png" height="120"/>
        <h3><figcaption><a href="public/Control/Switch_SPST_Off_disconnector.kicad_sym">SPST Trennschalter Aus</a></figcaption></h3>    
        <p></p>
        <h4>SPST-switch Off disconnector</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  <!--  
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>    -->
<li>
        <figure>  
        <img src="images/Switch_SPST_Limit__Off.png" height="120"/>
        <h3><figcaption><a href="images/Switch_SPST_Limit__Off.png">SPST Endschalter Aus</a></figcaption></h3>    
        <p></p>
        <h4>SPST limit switch off</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>    <!-- 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>    -->
<li>
        <figure>  
        <img src="images/Switch_SPST_Stay_put.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_SPST_Stay_put.kicad_sym">SPST Schalter, bleibt dran</a></figcaption></h3>    
        <p></p>
        <h4>SPST Stay put Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_SPST_Isolator_Off.png" height="120"/>
        <h3><figcaption><a href="public/Control/Switch_SPST_Isolator_Off.kicad_sym">SPST Isolierschalter Aus</a></figcaption></h3>    
        <p></p>
        <h4>SPST-switch Off Isolator</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   <!-- 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>    -->
<li>
        <figure>  
        <img src="images/Switch_SPST_Spring_Return_Off.png" height="120"/>
        <h3><figcaption><a href="public/Control/Switch_SPST_Spring_Return_Off.kicad_sym">SPST Schalter Aus Federrückstellung </a></figcaption></h3>    
        <p></p>
        <h4>SPST-switch Off spring return</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   <!-- 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>    -->
<li>
        <figure>  
        <img src="images/Switch__SPDT.png" height="120"/>
        <h3><figcaption><a href="public/Control/Switch__SPDT.kicad_sym">Einpoliger, doppelpoliger SPDT-Schalter um An 1 oder An 2 zu schalten</a></figcaption></h3>    
        <p>Ein einpoliger Umschalter (SPDT) kann einen Eingang mit einem von zwei Ausgängen verbinden. Es verfügt über einen Eingangs- und zwei Ausgangsanschlüsse. Die Verbindung von Eingang und Ausgang hängt von der Stellung des Schalters ab.
</p><p>
Der SPDT-Schalter ermöglicht alternative Verbindungen oder Funktionen und bietet so Flexibilität beim Schaltungsdesign. Es wird häufig verwendet, wenn zwischen zwei verschiedenen Pfaden oder Schaltkreisen gewählt werden soll. In elektrischen Schaltplänen wird der SPDT-Schalter durch eine einzelne Linie mit einer „T“-Form an einem Ende dargestellt.</p>
        <h4>Single-pole, double-throw SPDT switch on on</h4>     <p>A single-pole, double-throw (SPDT) can connect one input to either of two outputs. It has one input and two output connections. The connection of input to output depends upon the position of the switch.
</p><p>
The SPDT switch allows for alternate connections or functions, providing flexibility in circuit design. It is commonly used when choosing between two different paths or circuits. In electrical circuit diagrams, the SPDT switch is represented by a single line with a “T” shape at one end.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Motion_detector.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Motion_detector.kicad_sym">Bewegungsmelder</a></figcaption></h3>    
        <p>Ein Bewegungsmelder, auch als Präsenzmelder oder Infrarotsensor bekannt, ist ein elektronisches Gerät, das auf Bewegungen in seiner Umgebung reagiert. </p>
        <h4>Motion detector</h4>      
        <p>A motion detector, also known as a presence detector or infrared sensor, is an electronic device that reacts to movements in its environment.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Limit_switch.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Limit_switch.kicad_sym">Grenzschalter</a></figcaption></h3>    
        <p></p>
        <h4>Limit switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Taster.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Button.kicad_sym">Taster</a></figcaption></h3>    
        <p></p>
        <h4>Button</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Switch_auto_return.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_auto_return.kicad_sym">Auto Return Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch auto return</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_automatic_limit_temperature.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_automatic_limit_temperature.kicad_sym">Automatik Limit Temperatur Schalter </a></figcaption></h3>    
        <p></p>
        <h4>Switch automatic limit temperature</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Switch_automatic_temperature.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_automatic_temperature.kicad_sym">Automatik Temperatur Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Automatic temperature switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_Off.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_Off.kicad_sym">Ausschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch Off</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_Off_p2.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_Off_p2.kicad_sym">Ausschalter 2p</a></figcaption></h3>    
        <p></p>
        <h4>Switch Off p2</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_cross.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_cross.kicad_sym">Kreuzschalter</a></figcaption></h3>    
        <p></p>
        <h4>Cross switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Switch_Changeover.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_Changeover.kicad_sym">Wechselschalter</a></figcaption></h3>    
        <p></p>
        <h4>Changeover Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>    
<li>
        <figure>  
        <img src="images/Switch_Control.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_Control.kicad_sym">Kontrollschalter</a></figcaption></h3>    
        <p></p>
        <h4>Control Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_series.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_series.kicad_sym">Serienschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch series</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_Group.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_Group.kicad_sym">Gruppenschalter</a></figcaption></h3>    
        <p></p>
        <h4>Group Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Button_push.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Button_push.kicad_sym">Taster</a></figcaption></h3>    
        <p></p>
        <h4>Button push</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Button_with_two_buttons.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Button_with_two_buttons.kicad_sym">Taster mit zwei Tasten</a></figcaption></h3>    
        <p></p>
        <h4>Button with two buttons</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_Two_level.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_Two_level.kicad_sym">Schalter mit zwei Stufen</a></figcaption></h3>    
        <p></p>
        <h4>Switch Two level</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_selector.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_selector.kicad_sym">Wahlschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch selector</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Switch_joystick.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_joystick.kicad_sym">Joystick-Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Joystick Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Button_positive_make_contact_push.png" height="120"/>
        <h3><figcaption><a href="public/Control/Button_positive_make_contact_push.kicad_sym">Positiv schließender Druckknopf</a></figcaption></h3>    
        <p></p>
        <h4>Positive make Contact Push Button</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switching_cam.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switching_cam.kicad_sym">Schaltnocken</a></figcaption></h3>    
        <p></p>
        <h4>Switching cam</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_timer.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_timer.kicad_sym">Zeitschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch timer</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_radio.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_radio.kicad_sym">Funkschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch radio</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_turnng.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_turnng.kicad_sym">Drehschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch turnng</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_rotary_resting.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_rotary_resting.kicad_sym">Drehschalter rastend</a></figcaption></h3>    
        <p></p>
        <h4>Switch rotary resting</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_thermal_magnetic.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_thermal_magnetic.kicad_sym">Thermomagnetischer Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Thermal magnetic Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Button_push_to_make.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Button_push_to_make.kicad_sym">Drücken, um den Schalter zu betätigen</a></figcaption></h3>    
        <p></p>
        <h4>Button push to make</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Switch_push_button_limit.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_push_button_limit.kicad_sym">Druckknopf-Endschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch push button limit</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Push_Button_Double_Pole_Limit_Switch.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Push_Button_Double_Pole_Limit_Switch.kicad_sym">Doppelpoliger Endschalter mit Druckknopf</a></figcaption></h3>    
        <p></p>
        <h4>Push Button Double Pole Limit Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_push_button_DPDT.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_push_button_DPDT.kicad_sym">Druckknopf DPDT</a></figcaption></h3>    
        <p></p>
        <h4>Switch push button DPDT</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_delayed_closing.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_delayed_closing.kicad_sym">Verzögerter Schließschalter</a></figcaption></h3>    
        <p></p>
        <h4>Delayed Closing Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_fast_closing.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_fast_closing.kicad_sym">Schnell schließender Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Fast Closing Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Switch_pedal.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_pedal.kicad_sym">Pedalschalter</a></figcaption></h3>    
        <p></p>
        <h4>Pedal switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_emergency.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_emergency.kicad_sym">Not Aus Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Emergency Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_emergency_rotary.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_emergency_rotary.kicad_sym">Not-Drehenschalter</a></figcaption></h3>    
        <p></p>
        <h4>Emergency rotary switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_engine.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_engine.kicad_sym">Motorschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch engine</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Switch_float.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_float.kicad_sym">Schwimmschalter</a></figcaption></h3>    
        <p></p>
        <h4>Float switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_flow.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_flow.kicad_sym">Durchflussschalter</a></figcaption></h3>    
        <p></p>
        <h4>Flow switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_Gas_flow.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_Gas_flow.kicad_sym">Gas Durchflussschalter</a></figcaption></h3>    
        <p></p>
        <h4>Gas Flow switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_pressure.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_pressure.kicad_sym">Druckschalter</a></figcaption></h3>    
        <p></p>
        <h4>Pressure switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_high_pressure.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_high_pressure.kicad_sym">Hochdruckschalter</a></figcaption></h3>    
        <p></p>
        <h4>High pressure switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_low_pressure.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_low_pressure.kicad_sym">Niederdruckschalter</a></figcaption></h3>    
        <p></p>
        <h4>Low pressure switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_high_temperature.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_high_temperature.kicad_sym">Hochtemperaturschalter</a></figcaption></h3>    
        <p></p>
        <h4>High temperature switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_low_temperature.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_low_temperature.kicad_sym">Niedertemperaturschalter</a></figcaption></h3>    
        <p></p>
        <h4>Low temperature switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>     
<li>
        <figure>  
        <img src="images/Switch_thermal.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_thermal.kicad_sym">Temperaturschalter</a></figcaption></h3>    
        <p></p>
        <h4>Thermal Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch-thermal.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch-thermal.kicad_sym">Temperaturschalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch thermal</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Switch_temperature_sensitive.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_temperature_sensitive.kicad_sym">Temperaturempfindlicher Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Temperature sensitive switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>     
<li>
        <figure>  
        <img src="images/Switch_proximity.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_proximity.kicad_sym">Näherungsschalter</a></figcaption></h3>    
        <p></p>
        <h4>Proximity Switch </h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_proximity_touch.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_proximity_touch.kicad_sym">Näherungsschalter berühren</a></figcaption></h3>    
        <p></p>
        <h4>Proximity touch switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_pulse_counter.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_pulse_counter.kicad_sym">Impulszählerschalter</a></figcaption></h3>    
        <p></p>
        <h4>Pulse Counter Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_limit.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_limit.kicad_sym">Endschalter</a></figcaption></h3>    
        <p></p>
        <h4>Limit switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_mercury.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_mercury.kicad_sym">Quecksilberschalter</a></figcaption></h3>    
        <p></p>
        <h4>Mercury Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch-pressure.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch-pressure.kicad_sym">Druckschalter</a></figcaption></h3>    
        <p></p>
        <h4>Pressure switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Switch_Locking_device.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_Locking_device.kicad_sym">Schalter Verriegelungsvorrichtung</a></figcaption></h3>    
        <p></p>
        <h4>Locking device switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Button_telegraph_key.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Button_telegraph_key.kicad_sym">Telegraphen Taste</a></figcaption></h3>    
        <p></p>
        <h4>Telegraph Key</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Buttons_series.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Buttons_series.kicad_sym">Positions Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Buttons series</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr></ol>
<p>
Schalter die ich nicht zuordnen kann. / Switches that I can't assign.
</p>
[Zum Anfang](#top)/[above](#top)<hr> 
<ol> 
<li>
        <figure>  
        <img src="images/Switch1.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch1.kicad_sym">Antrieb</a></figcaption></h3>    
        <p></p>
        <h4>Drive Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch2.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch2.kicad_sym">Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch3.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch3.kicad_sym">Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch4.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch4.kicad_sym">Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch5.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch5.kicad_sym">Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch6.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch6.kicad_sym">Schalter</a></figcaption></h3>    
        <p></p>
        <h4>Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch7.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch7.kicad_sym">Schalter </a></figcaption></h3>    
        <p></p>
        <h4>Switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Switch_disconnectorwith_automatic_release.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Switch_disconnectorwith_automatic_release.kicad_sym">Lasttrennschalter mit selbsttätiger Auslösung</a></figcaption></h3>    
        <p></p>
        <h4>Switch disconnectorwith automatic release</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Wipe_contact_Contact_when_actuated.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Wipe_contact_Contact_when_actuated.kicad_sym">Wischkontakt Kontakt bei Betätigung</a></figcaption></h3>    
        <p></p>
        <h4>Wipe contact Contact when actuated</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Contact_in_case_of_relapse.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Contact_in_case_of_relapse.kicad_sym">Wischkontakt Kontakt bei Rückfall</a></figcaption></h3>    
        <p></p>
        <h4>Contact in case of relapse</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Wipe_contact_Contact_in_case_of_Actuation_and_relapse.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Wipe_contact_Contact_in_case_of_Actuation_and_relapse.kicad_sym">Wischkontakt Kontakt bei Betätigung und Rückfall</a></figcaption></h3>    
        <p></p>
        <h4>Wipe contact Contact in case of Actuation and relapse</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Modular_switch_disconnector_3p.png" height="120"/>
        <img src="images/Modularer_Lasttrennschalter.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Modular_switch_disconnector_3p.kicad_sym">Modularer Lasttrennschalter</a></figcaption></h3>    
        <p></p>
        <h4>Modular switch disconnector</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Circuit_breaker_3p.png" height="120"/>
          <img src="images/MCN316.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Circuit_breaker_3p.kicad_sym">Leitungsschutzschalter</a></figcaption></h3>    
        <p></p>
        <h4>Circuit breaker 3p</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
          <img src="images/Circuit_breaker_2p.png" height="120"/>
        <img src="images/S201-B6NA.png" height="120"/>
        <h3><figcaption><a href="public/Switch/Circuit_breaker_2p.kicad_sym">Leitungsschutzschalter voreilender N</a></figcaption></h3>    
        <p></p>
        <h4>Circuit breaker 2p more forward N</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> <!--
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  -->

</ol>  
