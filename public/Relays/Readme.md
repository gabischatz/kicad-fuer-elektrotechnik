<h1>Relais</h1>
<p> Nachfolgend finden Sie eine vollständige Liste der am häufigsten verwendeten Relaistypen, unterteilt in vier Abschnitte: Messrelais, Steuerrelais, Kontakte und Relaisspulen und schließlich Elektromagnete und Steuerungen, zusammen mit einer einfachen Beschreibung ihrer Funktion und Arbeitsweise und Einsatz in der industriellen Automatisierung. Wenn Sie möchten, können Sie auf das Symbol jedes Relais klicken, um zu unserer Download-Seite zu gelangen und die <b>kicad_sym-Datei</b> jedes Relaistyps herunterzuladen, um sie direkt in den Entwürfen und Diagrammen Ihrer Projekte verwenden zu können.</p>
<h2>Relais Symbol – Messrelais</h2>
<p><a href="https://www.youtube.com/watch?v=qMGvY1m_ejo"><img src="" height="200"/><br> YouTube, Schütz und Relais Erklärung Video </a></p>
<p><a href="https://www.youtube.com/shorts/OOWHt5zdgjc"><img src="" height="200"/><br> YouTube, Schütz und Relais Erklärung Video </a></p>
<ul>
        <li>
        <figure>  
          <img src="images/Low_current_or_synchronization_relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Relay_Low_current_or_synchronization.kicad_sym">Schwachstrom- oder Synchronisierungsrelais</a></figcaption></h3>
<p>Sie dienen zum Öffnen oder Schließen eines Hilfs- oder Sekundärstromkreises, je nachdem, ob die Intensität oder Leistung des zu steuernden Stromkreises im Bereich eines vorgegebenen Werts bleibt und diesen Wert nicht unterschreitet. Sie werden als Synchronrelais bezeichnet, da sie dies gewährleisten ist Synchronität, um zwei Abschnitte einer Schaltung zu verbinden.</p>
<h4>Low power or sync relay</h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
            <figure>  
          <img src="images/Minimum_Impedance_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/relay_maximum_current.kicad_sym">Relais mit niedriger Impedanz</a></figcaption></h3>
<p>In diesem Fall erfolgt die Einstellung des Relais durch Variieren der Windungszahl der Spule, so dass das Drehmoment am Kontaktkopf und das von der Feder ausgeübte Gegendrehmoment in dem Moment ausgeglichen werden, in dem das Verhältnis zwischen Spannung und Intensität herrscht Die an das Relais angelegte Spannung ist gleich der Einstellimpedanz.</p>
<h4>Low impedance relay</h4>
<p></p>
        </figure>
        </li>
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
            <figure>  
           <img src="images/Reverse_Current_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Reverse_Current_Relay.kicad_sym">Rückstromrelais</a></figcaption></h3>
<p>Das Strom- oder Rückstromrelais reagiert empfindlich auf die Richtung des Stromflusses und wird in Systemen verwendet, in denen mehrere elektrische Generatoren synchronisiert sind. Dieses Relais analysiert die Rückleistung der Last, die wir steuern möchten, sodass das Relais invertiert ist, wenn einer der parallel zu anderen Generatoren geschalteten Generatoren das System nicht erzeugt, aber Energie von den anderen Generatoren verbraucht hat ein Signal an die Generator-Synchronisierungssteuerung gesendet, sodass der Generator, der Systemstrom verbraucht, abgeschaltet wird.</p>
<h4>Reverse Current Relay</h4>
<p></p>
        </figure>
        </li>
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
            <figure>  
          <img src="images/Maximum_and_minimum_current_relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Maximum_and_minimum_current_relay.kicad_sym">Maximal- und Minimalstromrelais</a></figcaption></h3>
<p>Diese Relais dienen zur Steuerung des durch einen Stromkreis fließenden Stroms, sodass das Relais vom Ruhezustand in den Erregungszustand wechselt, solange die Intensität, die durch den zu steuernden Stromkreis fließt, außerhalb eines vorgegebenen Bereichs minimaler und maximaler Intensität liegt. Siehe Beispiel eines Hall-Effekt-Stromsensors. Wenn das Relais unter diesen Umständen aktiviert wird, wird ein Kontakt geschlossen, der in einem anderen Hilfsstromkreis zur Aktivierung eines Alarmsignals verwendet werden kann. Wenn also die maximale Intensität geregelt wird, bleibt das Relais solange erregt, bis der zu regelnde Strom niedriger als der eingestellte Wert ist. Bei der Minimalstromregelung bleibt das Relais aktiviert, bis die Stärke des zu messenden Stroms den Schwellenwert überschreitet und wieder einen Wert innerhalb des eingestellten Bereichs erreicht. Normalerweise wird das Relais mit einer Verzögerung von einigen Millisekunden entregt, um Schwingungen zu verhindern, die nicht gesteuert werden sollen, weil sie zu Fehlerregungen führen würden. Diese Verzögerung kann durch Betätigen eines dafür vorgesehenen Potentiometers beliebig variiert und eingestellt werden. Die Hysterese kann auch über ein weiteres Potentiometer gesteuert werden, um sie in Bereichen von 1 bis 45 % des in der Steuerung eingestellten maximalen und minimalen Intensitätsbereichs zu halten.</p>
<h4>Maximum and minimum current relay</h4>
<p></p>
        </figure>
        </li>
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
            <figure>  
          <img src="images/Broken_Conductor_Protection_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Broken_Conductor_Protection_Relay.kicad_sym">Geteiltes Fahrererkennungsrelais</a></figcaption></h3>
<p>Dabei handelt es sich um eine Art von Relais, die dazu dienen, in Drehstromleitungen die Unterschiede zwischen den von den Versorgungsnetzen gelieferten Mit- und Gegensystemströmen zu erkennen, sodass Brüche von Leitern erkannt werden können, die entweder in der Luft hängen oder sich sogar in der Luft befinden oder sogar den Boden berühren. Auf diese Weise kann bei Erkennung des Fehlers die Versorgung unterbrochen werden, um Überlastungen zu vermeiden, die das Netzwerk beschädigen.</p>
<h4>Split Driver Detection Relay</h4>
<p></p>
        </figure>
        </li>
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
            <figure>  
          <img src="images/No_Volt_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/No_Volt_Relay.kicad_sym">Fehlendes Spannungsrelais</a></figcaption></h3>
<p>Es gibt viele Modelle von Relais, die den Spannungsmangel in einem Stromkreis erkennen können, indem sie andere Steuerungen, Zeitgeber, Regler usw. hinzufügen. Allerdings müssen wir nur wissen, ob in einem Stromkreis ein bestimmter Spannungspegel vorhanden ist, beispielsweise 24 Volt Gleichspannung Strom, können wir in diesem Stromkreis ein Relais installieren, dessen Spule bei dieser Nennspannung arbeitet, sodass bei einem Spannungsverlust in diesem Stromkreis das von uns installierte Relais entregt wird und sein Rückwärtskontakt durch Federwirkung seinen Zustand ändert Ein weiterer Hilfsstromkreis würde sich zwischen dem Öffnerkontakt und dem Wechselrichterkontakt schließen, wodurch ein Alarm oder ein anderes für uns interessantes Gerät ausgelöst würde.</p>
<h4>Lack of voltage relay</h4>
<p></p>
        </figure>
        </li>
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
            <figure>  
          <img src="images/Frequency_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Frequency_Relay.kicad_sym">Frequenzrelais</a></figcaption></h3>
<p>Sie dienen zum Schutz der von Generatoren gespeisten Stromkreise, sodass das Relais im Falle einer Erhöhung der Drehzahl der Motorwelle des Generators und damit einer Erhöhung der Frequenz den Stromkreis öffnet und Geräteausfälle verhindert Empfänger.</p>
<h4>Frequency relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Maximum_voltage_relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Maximum_voltage_relay.kicad_sym">Maximalspannungsrelais</a></figcaption></h3>
<p>Sie werden installiert, um die Stromkreise „nach dem Relais“ vor Spannungen zu schützen, die über dem in der Einstellung des Relais selbst eingestellten Wert liegen, sodass das Relais den Stromkreis öffnet und Ausfälle aufgrund von Überspannungen in den Lasten verhindert. Beispielsweise kann in einem einphasigen Haushaltsstromkreis, der mit einer Nennspannung von 220 Volt Wechselstrom betrieben wird, das Relais so eingestellt werden, dass es die Lasten abschaltet, wenn es eine Eingangsspannung im Stromkreis von mehr als 240 Volt Wechselstrom erkennt, die zu Ausfällen führen könnte angeschlossene Geräte.</p>
<h4>Maximum voltage relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Monitoring_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Monitoring_Relay.kicad_sym">Messrelais (der Stern wird durch die zu messende Größe ersetzt)</a></figcaption></h3>
<p>Ein Messrelais (der Stern wird durch die zu messende Größe ersetzt) ist ein Relais, das für die Erfassung und Überwachung einer spezifischen physikalischen Größe ausgelegt ist. Der Sternplatzhalter deutet darauf hin, dass das Relais für verschiedene Messgrößen konfiguriert werden kann, abhängig von den Anforderungen des Anwenders. Das Relais kann somit zur Messung von Parametern wie Spannung, Strom, Widerstand oder anderen physikalischen Größen in einem elektrischen System verwendet werden. Es dient dazu, bei Erreichen oder Überschreiten bestimmter Schwellenwerte Aktionen auszulösen, um die Stabilität und Sicherheit des Systems zu gewährleisten.</p>
<h4>Measuring relay (the asterisk is replaced by the magnitude to be measured)</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Minimum_voltage_relay.png" height="120"/>
          <img src="images/MZ206.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Minimum_voltage_relay.kicad_sym">Unterspannungsauslöser</a></figcaption></h3>
<p>Ein "Minimum Voltage Relay" ist ein Relais, das auf eine minimale Spannung reagiert, um einen elektrischen Schaltvorgang zu initiieren. Dies bedeutet, dass das Relais erst aktiviert wird, wenn die angelegte Spannung einen bestimmten Schwellenwert erreicht oder überschreitet. Es wird normalerweise in Situationen eingesetzt, in denen eine Mindestspannung erforderlich ist, um sicherzustellen, dass das Relais zuverlässig auslöst und den Schaltprozess startet. Diese Art von Relais wird oft in verschiedenen elektrischen Anwendungen eingesetzt, um sicherzustellen, dass die anliegende Spannung einen bestimmten Wert erreicht, bevor das Relais aktiviert wird.</p>
<h4>Minimum voltage relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Automatic_Reclosing_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Automatic_Reclosing_Relay.kicad_sym">Automatisches Wiedereinschaltrelais</a></figcaption></h3>
<p>Ein "Automatic Reclose Relay" ist ein automatisches Wiedereinschaltrelais. Dieses Relais wird in elektrischen Stromnetzen eingesetzt, um automatisch versuchsweise erneute Schaltversuche nach einem Stromausfall durchzuführen. Wenn ein Kurzschluss oder ein anderer Fehler vorübergehend auftritt und dann verschwindet, versucht das Automatic Reclose Relay, die betroffene Leitung oder den betroffenen Schaltkreis automatisch wieder zu schließen, um die kontinuierliche Stromversorgung wiederherzustellen. Dieses Verhalten trägt zur Verbesserung der Zuverlässigkeit und Verfügbarkeit des elektrischen Netzes bei, indem es schnelle automatische Reaktionen auf vorübergehende Störungen ermöglicht.</p>
<h4>Automatic reclose relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Motor_Stalling_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Motor_Stalling_Relay.kicad_sym">Rotorrelais blockiert</a></figcaption></h3>
<p>Das Relais bietet Erdschlussschutz von Generatoren, Motoren, Transformatoren, Einspeisungen usw. und dort, wo ein zeitunabhängiger Schutz erforderlich ist.</p>
<h5>
Funktionsprinzip</h5><p>
Das Relais bietet einpoligen Überstromschutz für Stromversorgungssysteme. Die Betriebszeit des Relais wird durch die eingestellte unabhängige Zeit bestimmt. Das Relais misst den Strom vom Eingangsstromwandler und gibt, wenn er den eingestellten Schwellenwert überschreitet, nach der Betriebszeit, die durch die eingestellte bestimmte Zeit bestimmt wird, ein Auslösesignal aus. Die Relaisausgangskontakte sind vom Typ mit Selbstrückstellung. Die TEST-Taste erleichtert das Testen des Relais.</p>
<h4>Rotor relay locked</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Overcurrent_Relay_wiht_Two_Measuring_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Overcurrent_Relay_wiht_Two_Measuring_Relay.kicad_sym">Überstromrelais mit 2 Elementen und Abtastbereich</a></figcaption></h3>
<p>Es sich bei einem "Überstromrelais mit 2 Elementen und Probereich" um ein Schutzgerät, das speziell entwickelt wurde, um Überströme in einem elektrischen System zu erkennen. Die Bezeichnung "mit 2 Elementen" deutet darauf hin, dass es zwei separate Schutzfunktionen oder -einstellungen bietet.</p>
<h4>Overcurrent relay with 2 elements and sample range</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Overcurrent_Relay_with_Time_Delay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Overcurrent_Relay_with_Time_Delay.kicad_sym">Überstromrelais mit verzögerter Wirkung</a></figcaption></h3>
<p>Ein "Delayed Action Overcurrent Relay" ist ein Relais mit zeitverzögerter Ansprechcharakteristik bei Überstrom. Dieses Relais reagiert auf einen Überstromzustand, jedoch nicht sofort. Es beinhaltet eine Verzögerungsfunktion, die sicherstellt, dass das Relais erst nach einer vordefinierten Zeit anspricht, nachdem der Überstrom erkannt wurde. Diese Zeitverzögerung ermöglicht es, kurzzeitige Überstromzustände zu ignorieren, die normalerweise während des Anlaufens von elektrischen Maschinen oder beim Schalten auftreten können. Solche Relais werden oft in Stromschutzsystemen eingesetzt, um die Stabilität des Netzwerks zu gewährleisten und unnötige Abschaltungen zu vermeiden.</p>
<h4>Delayed Action Overcurrent Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Short_Circuit_Protection_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Short_Circuit_Protection_Relay.kicad_sym">Kurzschlusserkennungsrelais zwischen Spulen</a></figcaption></h3>
<p>Es handelt es sich wahrscheinlich um ein Relais, das dazu dient, Kurzschlüsse zwischen den Spulen einer elektrischen Schaltung zu erkennen und entsprechend zu reagieren.</p>
<h4>Short circuit detector relay between coils</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/3_Phase_Failure_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/3_Phase_Failure_Relay.kicad_sym">Dreiphasiges Leitungsfehlermelderelais</a></figcaption></h3>
<p>
Ein "Three-phase Line Fault Detector Relay" ist ein Relais zur Erkennung von Fehlerzuständen in einem dreiphasigen Stromnetz. Dieses Relais überwacht die drei Phasen des elektrischen Systems und detektiert Unregelmäßigkeiten oder Störungen, insbesondere Fehler in den Stromleitungen.</p>
<p>
Wenn ein Kurzschluss oder ein anderer Fehler auftritt, erkennt das Relais die Anomalie im Stromnetz und löst entsprechende Schutzmechanismen aus. Dies dient dazu, das System vor Schäden zu schützen und die Sicherheit des elektrischen Netzwerks zu gewährleisten.</p>
<p>
Das Relais spielt eine entscheidende Rolle in der zuverlässigen Funktion von Stromnetzen, indem es auf potenzielle Störungen reagiert und so dazu beiträgt, Schäden an Ausrüstung und Unterbrechungen im Stromfluss zu minimieren.</p>
<h4>Three-phase line fault detector relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
  </ul>
<h2>Relaissymbol – Steuerrelais</h2>
  <ul>
    <li>
            <figure>  
          <img src="images/Card_Access_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Card_Access_Relay.kicad_sym">Kartengesteuertes Relais</a></figcaption></h3>
<p>Ein "Card Operated Relay" ist ein Relais, das durch eine Karte oder eine spezielle Kartentechnologie aktiviert oder gesteuert wird. Dieses Relais reagiert auf die Präsenz oder den Einsatz einer Karte und schaltet entsprechend elektrische Schaltkreise ein oder aus.</p>
<p>
Typischerweise wird ein Kartenleser oder ein ähnliches Gerät verwendet, um die Karte zu identifizieren und die Informationen an das Kartenrelais weiterzugeben. Diese Art von Relais wird in verschiedenen Anwendungen eingesetzt, wie beispielsweise Zugangskontrollsystemen, Sicherheitsanlagen oder anderen Situationen, in denen eine Karte als Berechtigungsnachweis oder zur Steuerung von elektrischen Geräten verwendet wird.</p>
<h4>Card operated relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Defective_Voltage_Relay.png" height="120">
          <h3><figcaption><a href="public/Relays/Defective_Voltage_Relay.kicad_sym">Fehlerhafte Spannung des Relaisantriebs</a></figcaption></h3>
<p>Es handelt sich um einen Fehler, im Zusammenhang mit der Spannung des Relaistreibers.</p>
<p>Hier ist eine Beschreibung:<br>
"Fehlerhafte Spannung im Relaistreiber" bedeutet, dass es ein Problem mit der Steuerspannung des Relais gibt. Das Relaistreiber-Modul, das normalerweise für die Ansteuerung des Relais zuständig ist, erhält eine Spannung, die außerhalb des erwarteten oder erforderlichen Bereichs liegt. Dies kann verschiedene Ursachen haben, einschließlich eines defekten Treibermoduls, schlechter Verbindungen oder einer allgemeinen Unregelmäßigkeit in der elektrischen Versorgung des Systems. Es ist wichtig, die genaue Ursache des Spannungsfehlers zu ermitteln, um das Problem zu beheben und die ordnungsgemäße Funktion des Relais sicherzustellen.</p>
<h4>Relay drive faulty voltage</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Relay_with_electromagnetic_control_coil_and_push_button.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Relay_with_electromagnetic_control_coil_and_push_button.kicad_sym">Relais mit elektromagnetischer Steuerspule und Druckknopf</a></figcaption></h3>
<p>Ein Relais mit elektromagnetischer Steuerwicklung und Druckknopf ist ein elektronisches Schaltgerät. Es besteht aus einer Spule, die mit einer elektrischen Spannung versorgt wird und ein elektromagnetisches Feld erzeugt. Durch Betätigen eines Druckknopfs wird der Stromkreis geschlossen und die Spule aktiviert. Dies führt dazu, dass sich die Schaltkontakte im Inneren des Relais schließen oder öffnen, je nach Konfiguration.</p>
<p>Das Relais dient dazu, elektrische Signale zu verstärken oder zu steuern. Der Druckknopf wird als manuelle Steuereingabe verwendet, um den Zustand des Relais zu ändern. Diese Art von Relais wird oft in Schaltungssystemen verwendet, um verschiedene Funktionen auszulösen oder zu steuern, wenn eine manuelle Eingabe erforderlich ist.</p>
<h4>Relay with electromagnetic control coil and push button</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Fast_Switching_Relay.png" height="120">
          <h3><figcaption><a href="public/Relays/Fast_Switching_Relay.kicad_sym">Hochgeschwindigkeits-Relaisverbindung und -trennung</a></figcaption></h3>
<p>"Relais für schnelle Verbindungs- und Trennvorgänge" beschreibt ein Relais, das in der Lage ist, sehr schnell eine elektrische Verbindung herzustellen und zu unterbrechen. Dies könnte darauf hinweisen, dass das Relais in der Lage ist, den Stromkreis in kurzer Zeit zu schließen und zu öffnen, was in Anwendungen wichtig ist, die schnelle Schaltvorgänge erfordern, wie beispielsweise in Hochgeschwindigkeitssteuerungen, schnellen Messungen oder anderen Anwendungen, bei denen eine präzise und schnelle Signalverarbeitung erforderlich ist.</p>
<h4>High speed relay connection and disconnection</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/AC_Relay.png" height="120">
          <h3><figcaption><a href="public/Relays/AC_Relay.kicad_sym">AC-Relais</a></figcaption></h3>
<p>Ein "AC Relay" ist ein Relais, das speziell für Wechselstrom (AC - Alternating Current) ausgelegt ist. Ein Relais ist ein elektrisches Schaltelement, das in der Lage ist, einen oder mehrere elektrische Kontakte zu öffnen oder zu schließen, wenn es durch eine elektrische Steuerspannung aktiviert wird.</p>
<p>
Ein AC-Relais wird typischerweise in Wechselstromschaltkreisen eingesetzt, um Wechselstromsignale zu steuern. Es besteht aus einer Spule, einem Anker, und einem oder mehreren Schaltkontakten. Wenn die Spule mit Wechselstrom versorgt wird, erzeugt sie ein magnetisches Feld, das den Anker anzieht und dadurch die Schaltkontakte bewegt. Dies ermöglicht das Öffnen oder Schließen des Strompfads im AC-Kreislauf.</p>
<p>
AC-Relais finden in verschiedenen Anwendungen Verwendung, wie zum Beispiel in der Steuerung von elektrischen Geräten, Beleuchtungssystemen, Heizungen, Klimaanlagen und anderen elektrischen Systemen, die mit Wechselstrom arbeiten. Sie bieten eine zuverlässige Möglichkeit, Wechselstromkreise zu schalten und zu steuern.</p>
<h4>AC relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/DC_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/DC_Relay.kicad_sym">DC Relais</a></figcaption></h3>
<p>Ein Gleichstromrelais (DC-Relais) ist ein elektronisches Schaltgerät, das speziell für die Steuerung von Gleichstromkreisen entwickelt wurde. Es funktioniert auf ähnliche Weise wie ein herkömmliches elektromechanisches Relais, aber es ist für Gleichstromanwendungen optimiert.</p>
<p>
Ein typisches DC-Relais besteht aus einer Spule, einem Schalter und einem Anker. Wenn Gleichstrom durch die Spule fließt, erzeugt dies ein Magnetfeld, das den Anker anzieht und den Schalter schließt. Wenn der Strom durch die Spule unterbrochen wird, wird das Magnetfeld schwächer, und der Anker kehrt in seine Ausgangsposition zurück, wodurch der Schalter geöffnet wird.</p>
<p>
DC-Relais werden in verschiedenen Anwendungen eingesetzt, wo Gleichstrom verwendet wird, wie in der Steuerung von Elektromotoren, Batterieladeschaltungen, Solarsystemen, und anderen Gleichstromschaltungen. Sie dienen dazu, elektrische Signale zu verstärken oder den Strompfad zu steuern, je nach den Anforderungen der jeweiligen Schaltung.</p>
<h4>DC Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Differential_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Differential_Relay.kicad_sym">Differenz Relais</a></figcaption></h3>
<p>Ein "Differential Relay" (Differentialschutzrelais) ist eine spezielle Art von Relais, das in elektrischen Schutzsystemen verwendet wird, um Unterschiede zwischen elektrischen Strömen oder Spannungen zu erkennen. Es wird häufig in Hochspannungsnetzen eingesetzt, insbesondere in Transformatoren, Generatoren und anderen Systemen, um Fehler und Störungen zu erkennen.</p>
<p>
Die Hauptfunktion eines Differentielschutzrelais besteht darin, einen Alarm oder eine Schutzaktion auszulösen, wenn sich der Strom oder die Spannung an einem Punkt im System von einem vordefinierten Referenzpunkt unterscheidet. Es vergleicht die eingehenden Ströme oder Spannungen an verschiedenen Stellen im System und löst aus, wenn ein Unterschied, der auf einen Fehler hindeuten könnte, erkannt wird.</p>
<p>
Beispielsweise wird ein Differentielschutzrelais in einem Transformator eingesetzt, um sicherzustellen, dass der Strom, der in den Transformator eintritt, dem Strom entspricht, der den Transformator verlässt. Wenn es einen Unterschied zwischen diesen Strömen gibt, könnte dies auf einen internen Fehler im Transformator hinweisen, und das Relais würde dann eine Schutzmaßnahme auslösen, um den Transformator vor Schäden zu bewahren.</p>
<h4>Differential relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Differential_Current_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Differential_Current_Relay.kicad_sym">Differenz Strom Relais</a></figcaption></h3>
<p>Das Differentialstromrelais ist eine spezielle Form eines Relais, das darauf ausgelegt ist, auf Differenzströme zu reagieren. Ein Differentialstromrelais überwacht den Unterschied zwischen zwei Strömen in einem System. Typischerweise wird es in Anwendungen eingesetzt, bei denen es darauf ankommt, Unsymmetrien oder Leckströme zu erkennen.</p>
<p>
Ein typisches Einsatzgebiet ist der Schutz von Stromkreisen in elektrischen Anlagen. Das Differentialstromrelais vergleicht den Eingangsstrom mit dem Ausgangsstrom und reagiert, wenn es einen Unterschied feststellt, was auf einen Fehler im System, wie einen Kurzschluss oder einen Leckstrom, hinweisen könnte. Diese Art von Relais ist wichtig, um die Sicherheit der elektrischen Anlage zu gewährleisten und Schäden an Geräten oder Personen durch unerwünschte Ströme zu verhindern.</p>
<h4>Differential current relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Slow_Deactivation_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Slow_Deactivation_Relay.kicad_sym">Langsames Deaktivierungsrelais</a></figcaption></h3>
<p><a id="Slow_Deactivation_Relay">Ein "Slow Deactivation Relay" ist ein Relais mit verzögerter Deaktivierung. Dies bedeutet, dass es nach dem Auslösen oder Einschalten einer Schaltung nicht sofort, sondern mit einer Verzögerung deaktiviert wird. Diese Verzögerung kann dazu dienen, bestimmte Funktionen oder Zeitabläufe in einem elektrischen System zu steuern oder zu ermöglichen. Ein solches Relais wird häufig verwendet, um sicherzustellen, dass eine Schaltung oder ein Gerät für eine bestimmte Zeit aktiv bleibt, bevor es deaktiviert wird.</p>
<h4>Slow Deactivation Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Slow_Deactivation_Relay1.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Slow_Deactivation_Relay1.kicad_sym">Langsames Deaktivierungsrelais</a></figcaption></h3>
<p>[Siehe oben](#Slow_Deactivation_Relay)/[Please refer](#Slow_Deactivation_Relay)</p>
<h4>Slow Deactivation Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Fast_Deactivation_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Fast_Deactivation_Relay.kicad_sym">Schnellabschaltrelais</a></figcaption></h3>
<p>Es ist möglich, dass "Quick Deactivation Relay" in einem Kontext wie der Fahrzeugtechnik oder Industrieanlagen verwendet wird, um ein Relais zu beschreiben, das dazu dient, den Startvorgang schnell zu deaktivieren oder zu blockieren. Eine mögliche deutsche Übersetzung "Startsperrrelais" oder "Startblockierrelais" sein.</p>
<h4>Quick Deactivation Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Solenoid_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Solenoid_Relay.kicad_sym">Magnetventilrelais</a></figcaption></h3>
<p>Ein "Solenoid Valve Relay" ist ein Relais, das speziell für die Steuerung oder Aktivierung von Magnetventilen (Solenoidventilen) ausgelegt ist. Magnetventile werden in verschiedenen Anwendungen eingesetzt, um den Durchfluss von Flüssigkeiten oder Gasen zu steuern, indem ein elektrisches Signal verwendet wird, um das Magnetventil zu öffnen oder zu schließen.</p>
<p>
Das Solenoid Valve Relay übernimmt die Aufgabe, das Magnetventil durch das Schalten von elektrischem Strom zu steuern. Es handelt sich also um ein spezialisiertes Relais, das dazu dient, den Betrieb von Magnetventilen zu automatisieren oder zu kontrollieren, oft in industriellen oder automatisierten Systemen. Durch das gezielte Ein- und Ausschalten des Relais können Magnetventile präzise gesteuert werden, um den Flüssigkeits- oder Gasfluss nach Bedarf zu regulieren.</p>
<h4>Solenoid valve relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Interlock_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Interlock_Relay.kicad_sym">Mechanisches Verriegelungsrelais</a></figcaption></h3>
<p>Ein "Mechanical Locking Relay" ist ein Relais mit mechanischer Verriegelung. Bei diesem Typ von Relais wird die Verriegelung durch mechanische Mittel erreicht. Das bedeutet, dass das Relais nach dem Einschalten in einem Zustand verbleibt, auch wenn der ursprüngliche Schaltimpuls entfernt wird. Die mechanische Verriegelung sorgt dafür, dass das Relais in seiner aktuellen Position bleibt, bis eine andere Aktion oder ein anderer Impuls erfolgt, um die Verriegelung zu lösen und das Relais in einen anderen Zustand zu versetzen. Diese Art von Relais wird oft in Anwendungen eingesetzt, bei denen es wichtig ist, dass ein bestimmter Schaltzustand aufrechterhalten wird, ohne dass kontinuierlich Energie zugeführt werden muss.</p>
<h4>Mechanical locking relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Pulse_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Pulse_Relay.kicad_sym">Impulsrelais</a></figcaption></h3>
<p>Ein "Pulse Relay" ist ein Relais, das auf Impulse oder kurzzeitige Signale reagiert. Es wird oft so konzipiert, dass es auf kurze elektrische Impulse oder Signale anspricht und basierend auf diesen Impulsen Schaltvorgänge ausführt. Diese Art von Relais wird typischerweise in Anwendungen eingesetzt, bei denen kurze elektrische Signale oder Impulse zum Steuern von Schaltungen oder Geräten verwendet werden.</p>
<h4>Pulse relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Reed_Relay1.png" height="120"/>
          <h3><figcaption><a href="public/Relays/relay_reed1.kicad_sym">Reed-Relais</a></figcaption></h3>
<p>Ein "Reed-Relais" ist ein elektromechanisches Relais, das Reedkontakte verwendet, um den Schaltvorgang zu realisieren. Reedkontakte bestehen aus dünnen, ferromagnetischen Metallstreifen, die in einem Glasrohr eingeschlossen sind. Wenn ein magnetisches Feld in der Nähe des Reed-Relais erzeugt wird, schließen oder öffnen sich die Kontakte.</p>
<p>
Hier sind einige charakteristische Merkmale des Reed-Relais:</p>
<p>
<b>Aufbau:</b></br>
Ein Reed-Relais besteht aus einem oder mehreren Reed-Schaltern, die in einem geschlossenen Gehäuse platziert sind. Die Reed-Kontakte können normalerweise offen (NO) oder normalerweise geschlossen (NC) sein.</p>
<p>
<b>Funktionsweise:</b><br>
Die Arbeitsweise basiert auf dem Prinzip der magnetischen Beeinflussung der Reed-Kontakte. Wenn ein externes Magnetfeld angelegt wird (durch Anlegen einer Spannung an eine Spule im Relais), schließen oder öffnen sich die Reed-Kontakte, was den Stromfluss im Schaltkreis beeinflusst.
<p>
<b>Anwendungen:</b><br>
Reed-Relais werden oft in Anwendungen eingesetzt, bei denen eine galvanische Trennung erforderlich ist oder wenn ein elektrisch isolierter Schaltkontakt gewünscht wird. Sie finden Verwendung in Bereichen wie der Telekommunikation, Medizintechnik, Mess- und Regeltechnik sowie in Sicherheitsanwendungen.</p>
<p>
<b>Vorteile:</b><br>
Zu den Vorteilen gehören die hohe Schaltzuverlässigkeit, die geringe Größe und das Fehlen von mechanischem Verschleiß, da es keine direkte mechanische Berührung zwischen den Kontakten gibt.</p>
<p>
Reed-Relais sind besonders nützlich in Anwendungen, in denen geringe Schaltleistungen und eine hohe Schaltzuverlässigkeit erforderlich sind.</p>
<h4><a id="Reed_Relay">Reed Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Reed_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Reed_Relay.kicad_sym">Reed-Relais</a></figcaption></h3>
<p>[Siehe oben](#Reed_Relay)/[Please refer](#Reed_Relay)</p>
<h4>Reed Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Maximum_current_relay.png" height="120"/>
          <img src="images/ED183.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Maximum_current_relay.kicad_sym">Lastabwurfrelais</a></figcaption></h3>
<p>Ein "Lastabwurfrelais" (Relais für maximalen Strom) ist ein elektronisches Bauteil, das dazu dient, elektrische Ströme in einem Stromkreis zu überwachen. Die Hauptfunktion dieses Relais besteht darin, einen Alarm auszulösen oder den Stromkreis zu unterbrechen, wenn der fließende Strom einen vorher festgelegten Maximalwert überschreitet.</p>
</p>
Dieses Relais ist darauf ausgelegt, Überlastungen oder unzulässig hohe Ströme zu erkennen und zu verhindern, dass diese den angeschlossenen Geräten oder Schaltungen Schaden zufügen. Es findet Anwendung in verschiedenen elektrischen Systemen, um die Sicherheit zu gewährleisten und Schäden durch übermäßigen Stromfluss zu vermeiden.</p>
<h4>Maximum current relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Latching_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Latching_Relay.kicad_sym">Schrittweises Relais oder Impulse</a></figcaption></h3>
<p><b>Schritt-für-Schritt-Relais:</b><br>
Ein Schritt-für-Schritt-Relais ist ein elektronisches Relais, das in aufeinanderfolgenden Schritten oder Schritten aktiviert wird. Es reagiert auf aufeinanderfolgende Impulse oder Signale, um seinen Schaltzustand zu ändern. Dieses Relais kann in Anwendungen eingesetzt werden, in denen eine schrittweise oder gestufte Steuerung erforderlich ist. Es könnte beispielsweise dazu verwendet werden, um eine Serie von Aktionen auszulösen oder um schrittweise durch einen Prozess zu schalten.</p>
<p>
<b>Impulsrelais:</b><br>
Ein Impulsrelais ist ein Relais, das auf kurze elektrische Impulse reagiert. Es wird normalerweise durch einen kurzen Puls oder Impuls aktiviert und ändert seinen Zustand entsprechend. Dieses Relais wird häufig in Anwendungen eingesetzt, bei denen kurze elektrische Signale zur Steuerung von Geräten oder Systemen erforderlich sind. Impulsrelais können in verschiedenen Industriebereichen, Steuerungen und Schaltungen eingesetzt werden, um präzise, kurzzeitige Aktionen auszulösen.</p>
<h4>Step by step relay or impulses</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Remote_control_relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Remote_control_relay.kicad_sym">Hochfrequenzrelais</a></figcaption></h3>
<p>
Ein "Radiofrequenz-Relais" ist ein elektronisches Schaltgerät, das speziell für den Einsatz im Bereich der Hochfrequenz- oder Radiofrequenzsignale entwickelt wurde. Diese Relais werden verwendet, um HF-Signale in verschiedenen elektronischen Anwendungen zu schalten oder zu steuern. Hier sind einige Merkmale und Anwendungsbereiche:
</p>
<h5>Merkmale:</h5>
<ol><li><b>
Hochfrequenzschaltung:</b><br> Das Relais ist darauf ausgelegt, bei Hochfrequenzanwendungen zu arbeiten, was bedeutet, dass es in der Lage ist, Signale im Radiofrequenzbereich zu schalten.</li>
<li><b>
Isolation:</b><br> Es bietet oft eine gute Isolation zwischen den Schaltkreisen, um unerwünschte Interferenzen oder Beeinträchtigungen der HF-Signale zu minimieren.</li>
<li><b>
Geringe Verluste:</b><br> Diese Relais sind darauf optimiert, geringe Verluste bei der Signalübertragung zu gewährleisten, um die Signalqualität aufrechtzuerhalten.</li>
</ol>
<h5>Anwendungsbereiche:</h5>
<ol><li>
<b>Kommunikationssysteme:</b><br> In Radiokommunikationssystemen und drahtlosen Netzwerken werden RF-Relais eingesetzt, um Signale zu schalten und zu steuern.</li>
<li>
<b>Medizintechnik:</b><br> In medizinischen Geräten, die Hochfrequenzsignale verwenden, können RF-Relais für die Signalverarbeitung und -steuerung eingesetzt werden.</li>
<li>
<b>Satellitenkommunikation:</b><br> Bei der Übertragung von Signalen zwischen Satelliten und Bodenstationen können RF-Relais verwendet werden, um die Kommunikation zu steuern.</li>
<li>
<b>Messtechnik:</b><br> In Laboren und Testeinrichtungen werden RF-Relais in Hochfrequenzmessungen und Prüfungen eingesetzt.
</li></ol>
<p>
Diese Relais spielen eine wichtige Rolle in der Übertragung und Steuerung von Hochfrequenzsignalen in verschiedenen technischen Anwendungen.</p>
<h4>Radio Frequency Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Remanence_Relay1.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Remanence_Relay1.kicad_sym">Remanenzrelais</a></figcaption></h3>
<p>
Ein "Remanence-Relais" ist ein Relais, das auf dem Prinzip der Remanenz basiert. Remanenz bezieht sich auf die Fähigkeit eines Materials oder Systems, eine magnetische Restmagnetisierung zu behalten.
</p><p>
In einem Remanence-Relais wird die magnetische Remanenz genutzt, um eine geschaltete Position beizubehalten, nachdem der anfängliche Magnetisierungsstrom unterbrochen wurde. Dieses Relais arbeitet normalerweise mit einem Magnetkern, der durch einen Stromfluss magnetisiert wird. Nach dem Ausschalten des Stroms behält der Kern einen Teil dieser Magnetisierung bei, was dazu führt, dass das Relais in einer geschalteten Position verbleibt.
</p><p>
Diese Art von Relais wird oft in Anwendungen verwendet, in denen eine dauerhafte Position oder ein Schaltzustand beibehalten werden soll, selbst wenn der Steuerstrom ausgeschaltet ist. Es kann in verschiedenen industriellen und elektronischen Systemen eingesetzt werden, wo es aufgrund seiner Remanenz-Eigenschaften bestimmte Vorteile bietet.</p>
<h4>Remanence Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/On_Off_Time_Delay_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/On_Off_Time_Delay_Relay.kicad_sym">Standby-Relais mit verzögertem Betrieb</a></figcaption></h3>
<p>Ein "Standby-Relais mit verzögerter Betriebszeit" ist ein Relais, das normalerweise im Bereitschaftsmodus (Standby) verbleibt und eine Zeitverzögerung aufweist, bevor es aktiviert wird. Das bedeutet, dass nach dem Empfang eines Auslösesignals oder Befehls das Relais nicht sofort reagiert, sondern eine gewisse Verzögerung aufweist, bevor es in den Betriebszustand übergeht. Diese Verzögerung kann nützlich sein, um unerwünschte oder vorzeitige Aktivierungen zu verhindern und eine gezielte und zeitlich gesteuerte Reaktion des Relais zu ermöglichen.</p>
<h4>Standby relay with delayed operation</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Resonance_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/relay_resonance.kicad_sym">Mechanisches Resonanzrelais
</a></figcaption></h3>
<p>Ein "Mechanical Resonance Relay" (Mechanisches Resonanzrelais) ist eine spezielle Art von Relais, das auf den mechanischen Resonanzprinzipien basiert. Dieses Relais nutzt mechanische Schwingungen oder Resonanzen, um Schaltvorgänge auszulösen.
</p><p>
Die grundlegende Funktionsweise beruht darauf, dass das Relais auf eine bestimmte Frequenz oder Schwingungsamplitude eingestellt ist. Wenn die Schwingungen oder Vibrationen im überwachten System diese vordefinierte Frequenz oder Amplitude erreichen, wird das Relais ausgelöst und führt einen Schaltvorgang durch.
</p><p>
Diese Art von Relais findet oft Anwendung in Situationen, in denen die Überwachung mechanischer Vibrationen oder Resonanzen wichtig ist, beispielsweise in Maschinen, um auf ungewöhnliche Vibrationen oder Resonanzen hinzuweisen, die auf mögliche Probleme hindeuten könnten.</p>
<h4>Mechanical resonance relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Double_Coil_Relay.png" height="120">
          <h3><figcaption><a href="public/Relays/Double_Coil_Relay.kicad_sym">Doppelspulenrelais</a></figcaption></h3>
<p><a id="Double_Coil_Relay">Ein "Double Coil Relay" ist ein Relais mit zwei Spulen. Relais sind elektrische Schaltgeräte, die dazu dienen, einen oder mehrere elektrische Kontakte zu öffnen oder zu schließen, wenn eine Steuerspannung angelegt oder entfernt wird. Bei einem "Double Coil Relay" gibt es zwei separate Spulen, jede mit ihrer eigenen elektrischen Wicklung.
</p><p>
Die Verwendung von zwei Spulen ermöglicht es, das Relais in beide Richtungen zu betreiben, was eine flexiblere Steuerung ermöglicht. Je nachdem, welche Spule aktiviert wird, kann das Relais in unterschiedliche Schaltzustände versetzt werden. Diese Art von Relais wird oft in komplexeren Schaltungen oder Anwendungen eingesetzt, bei denen eine bidirektionale Steuerung erforderlich ist.</p>
<h4>Double Coil Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Double_Coil_Relay1.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Double_Coil_Relay1.kicad_sym">Doppelspulenrelais</a></figcaption></h3>
<p>[Siehe oben](#Double_Coil_Relay)/[Please refer](#Double_Coil_Relay)</p>
<h4>Double coil relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Double_Coil_Relay2.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Double_Coil_Relay2.kicad_sym">Doppelspulenrelais</a></figcaption></h3>
<p>[Siehe oben](#Double_Coil_Relay)/[Please refer](#Double_Coil_Relay)</p>
<h4>Double coil relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Double_Coil_Relay3.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Double_Coil_Relay3.kicad_sym">Doppelspulenrelais mit zwei gegenläufigen Wicklungen</a></figcaption></h3>
<p>Ein Relais mit zwei Wicklungen, die in entgegengesetzter Richtung arbeiten, ist ein Relais, bei dem zwei Spulen oder Wicklungen vorhanden sind, und jede davon ist so angeordnet, dass sie in einer entgegengesetzten Richtung arbeitet. Das bedeutet, dass die Magnetfelder, die durch den Strom in den beiden Wicklungen erzeugt werden, in entgegengesetzte Richtungen ausgerichtet sind.
</p><p>
Diese Konfiguration wird oft als "Bistabiles Relais" bezeichnet, da es zwei stabile Zustände haben kann, je nachdem, welche Wicklung aktiviert ist. Die Auswahl der aktiven Wicklung kann den Schaltzustand des Relais beeinflussen. In einer Richtung durchfließt der Strom die eine Wicklung und erzeugt ein Magnetfeld, das das Relais in einem Zustand hält, während in der entgegengesetzten Richtung die andere Wicklung aktiviert wird und das Relais in den anderen Zustand versetzt. Dies wird häufig in Anwendungen verwendet, bei denen es notwendig ist, zwischen zwei stabilen Zuständen zu wechseln.</p>
<h4>Relay with two windings operating in the opposite direction</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Solenoid_Operated_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Solenoid_Operated_Relay.kicad_sym">Allgemeines elektromagnetisches Relaissymbol</a></figcaption></h3>
<p>Ein generisches elektromagnetisches Relais ist eine elektronische Schaltvorrichtung, die auf dem Prinzip der elektromagnetischen Induktion basiert. Das Relais besteht im Wesentlichen aus einer Spule, einem beweglichen Armaturelement (Anker) und mindestens einem Schalterkontakt. Wenn ein elektrischer Strom durch die Spule fließt, erzeugt dies ein magnetisches Feld, das den Anker anzieht und somit den Schaltkontakt aktiviert oder deaktiviert.
</p><p>
Das Relais wird häufig als Schaltelement in elektrischen Schaltungen eingesetzt, um Signale zu verstärken, Stromkreise zu steuern oder zu isolieren. Es ermöglicht die Steuerung eines Hochleistungsstromkreises durch einen Niederspannungsstromkreis. Generische elektromagnetische Relais kommen in verschiedenen Anwendungen vor, von elektronischen Steuerungen in Haushaltsgeräten bis hin zu industriellen Automatisierungssystemen.</p>
<h4>Generic electromagnetic relay symbol</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Electromagnetic_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Electromagnetic_Relay.kicad_sym">Vereinfachtes elektromagnetisches Relaissymbol</a></figcaption></h3>
<p>Das vereinfachte elektromagnetische Relais-Symbol stellt ein elektronisches Schaltelement grafisch dar, das auf elektromagnetischen Prinzipien basiert. Das Symbol zeigt im Allgemeinen einen U-förmigen Kern, der die Spule repräsentiert, um die herum sich der Elektromagnet befindet. Ein beweglicher Schalter, häufig in Form eines beweglichen Kontakts oder Hebels, ist mit dem Elektromagneten verbunden.
</p><p>
Wenn die Spule des Relais mit Strom versorgt wird, erzeugt sie ein elektromagnetisches Feld, das den beweglichen Schalter anzieht oder abstößt. Dieser Vorgang ermöglicht das Öffnen oder Schließen eines elektrischen Stromkreises, was wiederum dazu dient, andere elektrische Geräte oder Schaltkreise zu steuern.
</p><p>
Die vereinfachte Darstellung zielt darauf ab, die grundlegenden Funktionsprinzipien des elektromagnetischen Relais in einem leicht verständlichen Symbol zusammenzufassen, was besonders in Schaltplänen und elektronischen Diagrammen nützlich ist.</p>
<h4>Simplified electromagnetic relay symbol</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Electronic_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Electronic_Relay.kicad_sym">Elektronisches Relais
</a></figcaption></h3>
<p>Ein elektronisches Relais ist ein elektronisches Schaltelement, das dazu dient, elektrische Kontakte zu öffnen oder zu schließen, um den Stromkreis eines elektrischen Geräts oder Systems zu steuern. Im Gegensatz zu elektromechanischen Relais, die auf mechanischen Bewegungen basieren, verwendet ein elektronisches Relais elektronische Schaltelemente wie Transistoren und Halbleiter, um den Schaltvorgang zu steuern.
</p><p>
Elektronische Relais bieten oft Vorteile wie eine schnellere Reaktionszeit, geringeren Energieverbrauch und eine präzisere Steuerung im Vergleich zu herkömmlichen elektromechanischen Relais. Sie werden in einer Vielzahl von Anwendungen eingesetzt, um elektrische Signale zu schalten und zu steuern, sei es in industriellen Schaltkreisen, Automatisierungssystemen, Stromversorgungen oder anderen elektronischen Geräten.</p>
<h4>Electronic relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Flashing_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Flashing_Relay.kicad_sym">Intermittierendes Relais</a></figcaption></h3>
<p>
Ein intermittierendes Relais, auch bekannt als intermittierendes Schaltrelais, ist eine spezielle Art von Relais, das dazu ausgelegt ist, elektrische Verbindungen in einem intermittierenden oder periodischen Muster herzustellen und zu unterbrechen. Anders ausgedrückt, es handelt sich um ein Relais, das in regelmäßigen Abständen ein- und ausgeschaltet wird.
</p><p>
Diese Art von Relais findet Anwendung in verschiedenen elektronischen Schaltungen, bei denen ein wiederholtes Ein- und Ausschalten erforderlich ist. Ein Beispiel könnte die Steuerung von Blinkern in Fahrzeugen sein, bei denen das intermittierende Relais dazu dient, den Wechsel zwischen Ein und Aus zu steuern, um das Blinklicht zu erzeugen.
</p><p>
Die Funktionsweise eines intermittierenden Relais ermöglicht es, periodisch zwischen geschlossenen und geöffneten Zuständen zu wechseln, um den gewünschten intermittierenden Betrieb zu erreichen.</p>
<h4>Intermittent relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Slow_Excitation__Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Slow_Excitation__Relay.kicad_sym">Langsames Weiterleiten zur Erregung</a></figcaption></h3>
<p>
Das "langsam ansprechende Relais für die Erregung" ist ein spezielles Relais, das sich durch eine verzögerte Reaktionszeit bei der Aktivierung (Erregung) auszeichnet. Im Kontext von Relais bedeutet "ansprechend" normalerweise, dass das Relais aktiviert oder geschlossen wird, wenn es eine bestimmte Schwellenspannung oder einen Schwellenstrom erreicht.
</p><p>
Ein "langsam ansprechendes Relais" bedeutet, dass die Zeit, die benötigt wird, um das Relais zu aktivieren, länger ist als bei einem herkömmlichen oder schneller ansprechenden Relais. Dies kann in verschiedenen Anwendungen nützlich sein, wenn eine Verzögerung bei der Aktivierung gewünscht ist, zum Beispiel um bestimmte Schaltvorgänge zu steuern oder um Störungen zu minimieren.
</p><p>
Es ist wichtig zu beachten, dass die genaue Funktionalität des "langsam ansprechenden Relais für die Erregung" von der spezifischen technischen Implementierung und den Anforderungen der Anwendung abhängt.</p>
<h4>Slow relay to excitation</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Unaffected_by_AC__Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Unaffected_by_AC__Relay.kicad_sym">Relais wird nicht durch Wechselstrom beeinflusst</a></figcaption></h3>
<p>Das das Relais ist unempfindlich gegenüber Wechselstrom. Es handelt sich dabei um eine Eigenschaft des Relais, die besagt, dass es nicht durch den Wechselstrom beeinflusst wird. Das Relais kann seine Funktion und Leistung beibehalten, auch wenn Wechselstrom in der Umgebung vorhanden ist. Diese Eigenschaft ist oft wichtig, wenn das Relais in Umgebungen eingesetzt wird, in denen Wechselstrom vorhanden ist, aber das Relais darauf ausgelegt ist, nur auf bestimmte Bedingungen oder Signale zu reagieren.</p>
<h4>Relay not affected by alternating current</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Polarized_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Polarized_Relay.kicad_sym">Polarisiertes Relais</a></figcaption></h3>
<p>Ein "Polarisiertes Relais" ist eine spezielle Art von Relais, bei dem die Richtung des Magnetfelds oder die Polarität bei der Aktivierung des Relais wichtig ist. Diese Relais verwenden einen Magneten, der in einer bestimmten Richtung aktiviert werden muss, um ordnungsgemäß zu funktionieren. Die Polarität des Magnetfelds beeinflusst die Ausrichtung des internen Schalters im Relais und bestimmt, ob der Schalter geöffnet oder geschlossen wird.
</p><p>
Das polarisierte Relais wird oft in Anwendungen eingesetzt, bei denen die Ausrichtung des Schalters eine entscheidende Rolle spielt, beispielsweise in Schaltungen, bei denen die Richtung des Stromflusses oder andere polaritätsabhängige Faktoren berücksichtigt werden müssen. Es ermöglicht eine präzise Steuerung und Anpassung in Systemen, in denen die Polarität von Bedeutung ist.</p>
<h4>Polarized relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Magnetically_Polarized_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Magnetically_Polarized_Relay.kicad_sym">Magnetisch polarisiertes Relais
</a></figcaption></h3>
<p>Ein magnetisch gepoltes Relais ist eine spezielle Art von Relais, bei dem der Schaltvorgang durch die Ausrichtung von magnetischen Feldern gesteuert wird. Das Magnetfeld wird genutzt, um einen schaltbaren Kontakt zu öffnen oder zu schließen. Diese Art von Relais wird oft in Anwendungen eingesetzt, bei denen eine geringe Leistungsaufnahme und eine präzise Steuerung erforderlich sind.</p>
<h4>Magnetically polarized relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Fast_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Fast_Relay.kicad_sym">Schnelles Relais</a></figcaption></h3>
<p>Ein Schnellrelais ist eine Art von Relais, das sich durch eine besonders schnelle Reaktionszeit auszeichnet. Es kann Signale oder Schaltvorgänge mit minimaler Verzögerung übertragen. Die Schnelligkeit eines Relais ist oft wichtig, wenn es darum geht, zeitkritische Prozesse zu steuern oder schnelle Schaltvorgänge in einem elektrischen System zu realisieren.</p>
<h4>Fast relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Remanence_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Remanence_Relay.kicad_sym">Remanenzrelais</a></figcaption></h3>
<p></p>
<h4>Relay Remaining</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
  </ul>
  <h2>Relaissymbol – Kontakte und Relaisspule</h2>
  <ul>
    <li>
            <figure>  
          <img src="images/Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/relay.kicad_sym">Relaisspule</a></figcaption></h3>
<p><a id="Relay_coil"> Die Relaisspule ist das Wicklungsteil eines Relais, das in der Regel aus Draht besteht und um einen magnetisierbaren Kern gewickelt ist. Wenn Strom durch die Spule fließt, erzeugt sie ein Magnetfeld, das wiederum die Schaltkontakte des Relais beeinflusst. Das Magnetfeld zieht die Schaltkontakte an oder stößt sie ab, was zu einem Öffnen oder Schließen des Relais führt und somit den Stromfluss in einem elektrischen Schaltkreis steuert.</p>
<h4>Relay coil</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Relay1.png" height="120"/>
          <h3><figcaption><a href="public/Relays/relay1.kicad_sym">Relaisspule</a></figcaption></h3>
<p>[Siehe oben](#Relay_coil)/[Please refer](#Relay_coil)</p>
<h4>Relay coil</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Relay2.png" height="120"/>
          <h3><figcaption><a href="images/relay2.kicad_sym">Relaisspule</a></figcaption></h3>
<p>[Siehe oben](#Relay_coil)/[Please refer](#Relay_coil)</p>
<h4>Relay coil</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <!--
    <li>
            <figure>  
          <img src="" height="120">
          <h3><figcaption><a href="">Kontakt öffnen</a></figcaption></h3>
<p></p>
<h4>Open contact</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href="">Elektromagnetischer Knopf</a></figcaption></h3>
<p></p>
<h4> </h4>
<p></p>  
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    -->
    <li>
            <figure>  
          <img src="images/Anchor_mechanism_relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Anchor_mechanism_relay.kicad_sym">Ankermechanismus-Relais</a></figcaption></h3>
<p></p>
<h4>Anchor mechanism relay</h4> 
<p></p> 
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Electromagnet_Relay.png" height="120">
          <h3><figcaption><a href="public/Relays/Electromagnet_Relay.kicad_sym">Elektromagnet</a></figcaption></h3>
<p></p>
<h4>Electromagnet</h4>  
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
   </ul> 
   <h2>Relais die ich nicht zuordnen kann.
   </h2>
   <ul>
    <li>
            <figure>  
          <img src="images/Thermal_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/relay_thermal.kicad_sym">Thermorelais</a></figcaption></h3>
<p>Das Funktionsprinzip des Thermorelais besteht darin, dass der in das Heizelement fließende Strom Wärme erzeugt, die zu einer Verformung der Bimetallstreifen mit unterschiedlichen Ausdehnungskoeffizienten führt. Wenn die Verformung einen bestimmten Abstand erreicht, drückt sie auf die Pleuelstange, um den Steuerkreis zu öffnen, wodurch das Schütz aktiviert wird. Bei Stromausfall wird der Hauptstromkreis getrennt, um den Überlastschutz des Motors zu realisieren.

<b>Als Überlastschutzkomponente des Motors werden Thermorelais aufgrund ihrer geringen Größe, einfachen Struktur und geringen Kosten häufig in der Produktion eingesetzt.</b></p>
<h4>Thermal  Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Supressor_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Supressor_Relay.kicad_sym">Unterdrückungsrelais</a></figcaption></h3>
<p>Supressor RelayEin Unterdrückungsrelais wird normalerweise in elektrischen Schaltungen eingesetzt, um bestimmte elektrische Störungen zu unterdrücken oder zu reduzieren. Es könnte beispielsweise dazu dienen, Überspannungen zu dämpfen oder elektromagnetische Interferenzen zu reduzieren. Die genaue Funktion und Anwendung können je nach Kontext variieren.</p>
<h4>Suppressor Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Relay_coil_of_a_polarized.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Relay_coil_of_a_polarized.kicad_sym">Relaisspule eines polarisierten Relais</a></figcaption></h3>
<p>Ein polarisiertes Relais verwendet eine spezielle Schaltung, um die Ausrichtung des Schalters zu beeinflussen. Die Spule im Relais erzeugt ein Magnetfeld, das den Schalter beeinflusst. Das polarisierte Relais bleibt in einem stabilen Zustand, selbst wenn die Stromversorgung unterbrochen wird und später wiederhergestellt wird, da es durch die Polarisation in einem bestimmten Zustand verbleibt. Dies kann in bestimmten Anwendungen nützlich sein, um unerwünschte Zustandsänderungen zu verhindern.</p>
<h4>Relay coil of a polarized</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Distance_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Distance_Relay.kicad_sym">Entfernungsrelais</a></figcaption></h3>
<p>Ein Entfernungsrelais ist ein spezieller Typ von Schutzrelais, das in elektrischen Energiesystemen eingesetzt wird, um den Schutz von Übertragungsleitungen zu gewährleisten.

Die Hauptfunktion des Entfernungsrelais besteht darin, die Entfernung zu einem Fehler oder Kurzschluss in einer Übertragungsleitung zu messen und basierend darauf zu entscheiden, ob eine Schutzaktion ausgelöst werden sollte. Es verwendet Parameter wie Spannung und Strom, um die Impedanz der Leitung zu berechnen und somit die Entfernung zum Fehler zu bestimmen.
</p><p>
Entfernungsrelais spielen eine entscheidende Rolle im Schutz elektrischer Netze, da sie dazu beitragen, Störungen zu lokalisieren und zu isolieren, um Schäden an der Ausrüstung zu minimieren und die Zuverlässigkeit des Stromnetzes zu gewährleisten.</p>
<h4>Distance Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Earth_Fault_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Earth_Fault_Relay.kicad_sym">Erdfehlerrelais</a></figcaption></h3>
<p>Das "Earth Fault Relay" (Erdfehlerrelais) ist ein elektronisches Schutzgerät, das in elektrischen Systemen eingesetzt wird, um Erdfehler zu erkennen und darauf zu reagieren. Ein Erdfehler tritt auf, wenn ein elektrischer Strom ungewollt einen Pfad zur Erde findet, anstatt auf dem vorgesehenen Weg durch das elektrische System zu fließen.
</p><p>
Das Erdfehlerrelais überwacht den Stromfluss im System und detektiert Abweichungen, die auf einen möglichen Erdfehler hindeuten. Wenn ein solcher Fehler erkannt wird, löst das Relais Schutzmechanismen aus, um den Stromkreis zu unterbrechen und potenzielle Gefahren zu verhindern. Dies ist besonders wichtig, um Personen vor elektrischen Unfällen zu schützen und Schäden an elektrischen Anlagen zu verhindern.</p>
<h4>Earth Fault Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Indicator_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Indicator_Relay.kicad_sym">Anzeige-Relais</a></figcaption></h3>
<p>Ein Anzeige-Relais ist ein elektrisches Schaltgerät, das dazu dient, Anzeigesignale zu steuern oder zu überwachen. Es wird häufig in elektrischen Schaltungen eingesetzt, um den Status bestimmter Komponenten oder Zustände zu signalisieren. Das Anzeige-Relais kann dazu verwendet werden, eine visuelle oder akustische Anzeige auszulösen, wenn bestimmte Bedingungen erfüllt sind oder wenn bestimmte Ereignisse auftreten. Es ermöglicht somit die Überwachung oder Anzeige von Informationen im elektrischen System.</p>
<h4>Indicator Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/LED_Relay.png" height="120"/>
          <h3><figcaption><a href="images/LED_Relay.png">LED Relay</a></figcaption></h3>
<p>
Ein "LED-Relais" ist ein Relais, das speziell für die Steuerung von LEDs (Light Emitting Diodes) entwickelt wurde. Hier sind einige typische Eigenschaften und Merkmale, die mit einem LED-Relais verbunden sein könnten:</p>
<ol><li><b>
Niedrige Spannung und Strom:</b><br> Ein LED-Relais ist oft für niedrige Spannungen und Ströme ausgelegt, um den Energieverbrauch zu minimieren und die Anforderungen von LEDs zu erfüllen.</li>
<li><b>
Schutz vor Überspannung:</b><br> Um die empfindlichen LEDs zu schützen, können LED-Relais Schaltungen enthalten, die vor Überspannung oder Spannungsspitzen schützen.</li>
<li><b>
Kompakte Bauweise:</b><br> LED-Relais sind häufig kompakt gestaltet, um Platz zu sparen und die Integration in elektronische Schaltungen zu erleichtern.</li>
<li><b>
Geringes Schaltgeräusch:</b><br> In einigen Anwendungen, insbesondere solchen, die in geräuschsensiblen Umgebungen eingesetzt werden, können LED-Relais darauf ausgelegt sein, ein minimales Schaltgeräusch zu erzeugen.<(li)>
<li><b>
Hohe Lebensdauer:</b><br> LED-Relais können für eine längere Lebensdauer ausgelegt sein, um den zuverlässigen Betrieb von LED-Anwendungen zu gewährleisten.</li>
<li><b>
Steuerung von LED-Beleuchtung:</b><br> Diese Relais können in Schaltungen verwendet werden, um die Beleuchtung von LEDs in verschiedenen Anwendungen zu steuern, sei es in Haushaltsgeräten, Fahrzeugen oder anderen elektronischen Systemen.</li></ol>
<p>
Es ist wichtig, die spezifischen Produktspezifikationen und Anwendungshinweise des jeweiligen LED-Relais zu überprüfen, da die Eigenschaften je nach Hersteller und Modell variieren können.</p>
<h4>LED Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Overvoltage_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Overvoltage_Relay.kicad_sym">Überspannungsrelais</a></figcaption></h3>

<p>Dieses Relais ist darauf ausgelegt, elektrische Anlagen oder Geräte vor zu hohen Spannungen zu schützen. Wenn die Spannung einen vordefinierten Schwellenwert überschreitet, wird das Überspannungsrelais aktiviert und kann verschiedene Schutzmaßnahmen auslösen, um Schäden an elektronischen Komponenten zu verhindern. Überspannungsrelais kommen in verschiedenen Anwendungen vor, beispielsweise in der Energieversorgung, in Elektroinstallationen oder in industriellen Systemen, um die Stabilität und Sicherheit der elektrischen Infrastruktur zu gewährleisten.</p>
<h4>Overvoltage Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Reverse_Power_Relay.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Reverse_Power_Relay.kicad_sym">Rückwärtsleistungsrelais</a></figcaption></h3>
<p>
Ein Rückwärtsleistungsrelais ist eine elektronische Vorrichtung oder Schutzeinrichtung, die in elektrischen Netzwerken eingesetzt wird, um die Richtung des Leistungsflusses zu überwachen. Es erkennt, ob die Leistung in einem System in die "falsche" Richtung fließt, was auf eine ungewöhnliche oder potenziell gefährliche Betriebssituation hinweisen kann.
</p><p>
Normalerweise sollte die elektrische Energie in einem Netzwerk in eine vordefinierte Richtung fließen. Wenn das Rückwärtsleistungsrelais jedoch feststellt, dass die Leistung in die entgegengesetzte Richtung fließt, könnte dies auf Probleme wie Netzrückkopplungen, Fehlfunktionen von Generatoren oder andere Störungen im System hinweisen. In solchen Fällen löst das Rückwärtsleistungsrelais Schutzmaßnahmen aus, um das System vor Schäden zu bewahren, indem es beispielsweise bestimmte Schalter öffnet oder andere Schutzgeräte aktiviert.</p>
<h4>Reverse Power Relay</h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>
 [Zum Anfang](#top)/[above](#top)<hr>
    <li>
            <figure>  
          <img src="images/Relay_Door_opener.png" height="120"/>
          <h3><figcaption><a href="public/Relays/Relay_Door_opener.kicad_sym">Tür Öffner Relais</a></figcaption></h3>

<p>Ein "Door Opener Relay" ist ein Relais, das speziell für die Steuerung von Türöffnern oder Torschließmechanismen entwickelt wurde. Dieses Relais ermöglicht die ferngesteuerte Aktivierung oder Deaktivierung von Türöffnern durch den elektrischen Schaltkreis. Es wird häufig in Zugangskontrollsystemen, Sicherheitssystemen und Gebäudeautomatisierung eingesetzt, um den Zugang zu einem Bereich zu steuern. Durch das Senden eines elektrischen Signals kann das Door Opener Relay den Türöffner aktivieren und somit den Zugang freigeben oder sperren. Es spielt eine wichtige Rolle in der Sicherheit und Kontrolle des Zugangs zu verschiedenen Bereichen in Gebäuden.</p>
<h4>Relay Door opener</h4>
<p></p>
        </figure>
        </li>
     [Zum Anfang](#top)/[above](#top)<hr>
        <li>
            <figure>  
                <img src="images/Relay_Impulse.png" height="120"/>
                <h3><figcaption><a href="public/Relays/Relay_Impulse.kicad_sym">Stromstoß Relais</a></figcaption></h3>
                <p></p>
                <h4>Impulse relay</h4>
                <p></p>
        </figure>
        </li>
      [Zum Anfang](#top)/[above](#top)<hr>  <!-- 
    <li>
            <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
    </li>
    [Zum Anfang](#top)/[above](#top)<hr>-->
</ul>
