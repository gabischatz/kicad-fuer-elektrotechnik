<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Schalter</h1></th>
<th width="50%"><h1>Interrupteurs</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.14</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-14-01.png" height="120"/>
        <h3><figcaption><a href="11-14-01.kicad_sym">Schalter, allgemein</a></figcaption></h3>    
        <p>11-14-01</p>
        <h4>Interrupteur, symbole général</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-02.png" height="120"/>
        <h3><figcaption><a href="11-14-02.kicad_sym">Schalter mit Kontrolleuchte</a></figcaption></h3>    
        <p>11-14-02</p>
        <h4>Interrupteur à lampe témoin</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-03.png" height="120"/>
        <h3><figcaption><a href="11-14-03.kicad_sym">Zeitschalter, einpolig</a></figcaption></h3>    
        <p>11-14-03</p>
        <h4>Interrupteur à temps de fermeture limité, unipolaire</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-04.png" height="120"/>
        <h3><figcaption><a href="11-14-04.kicad_sym">Schalter, zweipolig</a></figcaption></h3>    
        <p>11-14-04</p>
        <h4>Interrupteur, bipolaire</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-05.png" height="120"/>
        <h3><figcaption><a href="11-14-05.kicad_sym">Serienschalter, einpolig</a></figcaption></h3>    
        <p>11-14-05 </p>
        <h4>Commutateur unipolaire</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-06.png" height="120"/>
        <h3><figcaption><a href="11-14-06.kicad_sym">Wechselschalter, einpolig</a></figcaption></h3>    
        <p>11-14-06</p>
        <h4>Interrupteur unipolaire va-et-vient</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-07.png" height="120"/>
        <h3><figcaption><a href="11-14-07.kicad_sym">Kreuzschalter</a></figcaption></h3>    
        <p>11-14-07</p>
        <h4>Commutateur intermédiaire pour va-et-vient</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-07ex.png" height="120"/>
        <h3><figcaption><a href="11-14-07ex.kicad_sym">Kreuzschalter</a></figcaption></h3>    
        <p>11-14-07Ex.</p>
        <h4>Commutateur intermédiaire pour va-et-vient</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-08.png" height="120"/>
        <h3><figcaption><a href="11-14-08.kicad_sym">Dimmer</a></figcaption></h3>    
        <p>11-14-08</p>
        <h4>Interrupteur gradateur</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-09.png" height="120"/>
        <h3><figcaption><a href="11-14-09.kicad_sym">Schalter mit Zugschnur</a></figcaption></h3>    
        <p>11-14-09</p>
        <h4>Interrupteur unipolaire à tirette</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-10.png" height="120"/>
        <h3><figcaption><a href="11-14-10.kicad_sym">Taster</a></figcaption></h3>    
        <p>11-14-10</p>
        <h4>Bouton-poussoir</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-11.png" height="120"/>
        <h3><figcaption><a href="11-14-11.kicad_sym">Taster mit Leuchte</a></figcaption></h3>    
        <p>11-14-11</p>
        <h4>Bouton-poussoir lumineux</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-12.png" height="120"/>
        <h3><figcaption><a href="11-14-12.kicad_sym">Taster mit eingeschränkter Zugänglichkeit</a></figcaption></h3>    
        <p>11-14-12</p>
        <h4>Bouton-poussoir, protégé</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-13.png" height="120"/>
        <h3><figcaption><a href="11-14-13.kicad_sym">Zeitrelais</a></figcaption></h3>    
        <p>11-14-13</p>
        <h4>Minuterie, Appareil limiteur de durée</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-14.png" height="120"/>
        <h3><figcaption><a href="11-14-14.kicad_sym">Schaltuhr</a></figcaption></h3>    
        <p>11-14-14</p>
        <h4>Interrupteur horaire</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-14-15.png" height="120"/>
        <h3><figcaption><a href="11-14-15.kicad_sym">Schlüsselschalter, Wächtermelder</a></figcaption></h3>    
        <p>11-14-15</p>
        <h4>Dispositif de commande ou de contrôle par clé, Dispositif de contrôle par vigile</h4>      
        <p></p>
        </figure>
</li> 
</ul>
