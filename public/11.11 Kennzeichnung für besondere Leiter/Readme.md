<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Kennzeichnung für besondere Leiter</h1></th>
<th width="50%"><h1>Identification de conducteurs particuliers</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.11</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-11-01.png" height="120"/>
        <h3><figcaption><a href="11-11-01.kicad_sym">Neutralleiter (N), Mittelleiter</a></figcaption></h3>    
        <p>11.11.01</p>
        <h4>Conducteur neutre</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-11-02.png" height="120"/>
        <h3><figcaption><a href="11-11-02.kicad_sym">Schutzleiter (PE)</a></figcaption></h3>    
        <p>11-11-02</p>
        <h4>Conducteur de protection</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-11-03.png" height="120"/>
        <h3><figcaption><a href="11-11-03.kicad_sym">Neutralleiter mit Schutzfunktion (PEN)</a></figcaption></h3>    
        <p>11-11-03</p>
        <h4>Conducteur de protection et neutre confondus</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-11-04.png" height="120"/>
        <h3><figcaption><a href="11-11-04.kicad_sym">Drei Leiter, ein Neutralleiter, ein Schutzleiter</a></figcaption></h3>    
        <p>11-11-04</p>
        <h4>Canalisation triphasée avec conducteur neutre et conducteur de protection</h4>      
        <p></p>
        </figure>
</li> 
</ul>
