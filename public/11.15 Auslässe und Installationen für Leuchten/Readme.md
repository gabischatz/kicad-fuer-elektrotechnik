<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Auslässe und Installationen für Leuchten</h1></th>
<th width="50%"><h1>Installations d'éclairage</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2></h2>
<ul>
<li>
        <figure>  
        <img src="images/11-15-01.png" height="120"/>
        <h3><figcaption><a href="11-15-01.kicad_sym">Leuchtenauslaß</a></figcaption></h3>    
        <p>11-15-01</p>
        <h4>Point d'attente d'appareil d'éclairage</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-02.png" height="120"/>
        <h3><figcaption><a href="11-15-02.kicad_sym">Leuchtenauslaß auf Putz</a></figcaption></h3>    
        <p>11-15-02</p>
        <h4>Point d'attente d'appareil d'éclairage en applique murale</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-03.png" height="120"/>
        <h3><figcaption><a href="11-15-03.kicad_sym">Leuchte, allgemein</a></figcaption></h3>    
        <p>11-15-03</p>
        <h4>Lampe, symbole général</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-04.png" height="120"/>
        <h3><figcaption><a href="11-15-04.kicad_sym">Leuchte für Leuchtstofflampe, allgemein</a></figcaption></h3>    
        <p>11-15-04</p>
        <h4>Luminaire, Lampe à fluorescence, symbole général</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-05.png" height="120"/>
        <h3><figcaption><a href="11-15-05.kicad_sym">Leuchte mit drei Leuchtstofflampen</a></figcaption></h3>    
        <p>11-15-05</p>
        <h4>Luminaire à trois tubes fluorescents</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-06.png" height="120"/>
        <h3><figcaption><a href="11-15-06.kicad_sym">Leuchte mit fünf Leuchtstofflampen</a></figcaption></h3>    
        <p>11-15-06</p>
        <h4>Luminaire à cinq tubes fluorescents</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-07.png" height="120"/>
        <h3><figcaption><a href="11-15-07.kicad_sym">Scheinwerfer, allgemein</a></figcaption></h3>    
        <p>11-15-07</p>
        <h4>Projecteur, symbole général</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-08.png" height="120"/>
        <h3><figcaption><a href="11-15-08.kicad_sym">Punktleuchte</a></figcaption></h3>    
        <p>11-15-08</p>
        <h4>Projecteur à faisceau peu divergent</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-09.png" height="120"/>
        <h3><figcaption><a href="11-15-09.kicad_sym">Flutlichtleuchte</a></figcaption></h3>    
        <p>11-15-09</p>
        <h4>Projecteur d'illumination</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-10.png" height="120"/>
        <h3><figcaption><a href="11-15-10.kicad_sym">Vorschaltgerät für Entladungslampen</a></figcaption></h3>    
        <p>11-15-10</p>
        <h4>Appareil auxiliaire pour lampe à décharge</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-11.png" height="120"/>
        <h3><figcaption><a href="11-15-11.kicad_sym">Sicherheitsleuchte, Notleuchte mit getrenntem Stromkreis, Rettungszeichenleuchte</a></figcaption></h3>    
        <p>11-15-11</p>
        <h4>Appareil d'éclairage de sécurité sur circuit spécial</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-15-12.png" height="120"/>
        <h3><figcaption><a href="11-15-12.kicad_sym">Sicherheitsleuchte mit eingebauter Stromversorgung</a></figcaption></h3>    
        <p>11-15-12</p>
        <h4>Bloc autonome d'éclairage de sécurité</h4>      
        <p></p>
        </figure>
</li> 
</ul>
