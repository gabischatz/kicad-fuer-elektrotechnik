<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Verteiler und Richtungskoppler</h1></th>
<th width="50%"><h1>Répartiteurs et coupleurs directifs</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.7 </h2>
<ul>
<li>
        <figure>  
        <img src="images/11-07-01.png" height="120"/>
        <h3><figcaption><a href="11-07-01.kicad_sym">Zweiwegverteiler</a></figcaption></h3>    
        <p>11-07-01</p>
        <h4>Répartiteur à deux voies</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href="">Dreiwegverteiler</a></figcaption></h3>    
        <p>11-07-02</p>
        <h4>Répartiteur à trois voies</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href="">Richtungskoppler</a></figcaption></h3>    
        <p>11-07-03</p>
        <h4>Coupleur directif</h4>      
        <p></p>
        </figure>
</li></ul>
