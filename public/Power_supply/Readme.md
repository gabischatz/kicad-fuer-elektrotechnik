<h1>Stromversorgung/Power symbols</h1>
<p>Stromsymbole sind spezielle Symbole in KiCad, die zur Kennzeichnung globaler Netze für Stromverbindungen verwendet werden.</p><p>Power symbols are special symbols in KiCad used to identify global networks for power connections.</p><p>
[Leiter](#user-content-leiterelectrical-conductor) / [Netzsysteme](#user-content-netzsystemenetwork-systems) / [PV Systeme](#user-content-pv-systemepv-systems)<br>[Electrical conductor](#user-content-leiterelectrical-conductor) / [Network systems](#user-content-netzsystemenetwork-systems) / [PV systems](#user-content-pv-systemepv-systems)</p>        
<h2>Leiter/Electrical conductor</h2>
<ul>
        <li>
        <figure>  
          <img src="images/230V.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/230V.kicad_sym">230 V 50 Hz</a></figcaption></h3>
<p>In Europa beträgt die Netzspannung 230 V ± 23 V bei einer Netzfrequenz von 50 Hz ± 0,2 Hz.[Quelle: Netzspannung, Wikipedia]
Um sicherzustellen, dass die richtige Spannungsversorgung im Schaltplan leicht gefunden werden kann, werden zwei Zahlen im Netz verwendet, die die Laufnummer der Schaltung angeben. Diese Nummern werden im Abschnitt "Seite einrichten unter Kommentar 2" eingetragen und sind anschließend im Beschriftungsfeld des Zeichenblatts unten rechts zu finden. Und der Strompfad. Dieser ist die Spalte, in der sich das Symbol befindet.
</p>
<p>
In Europe, the mains voltage is 230 V ± 23 V with a mains frequency of 50 Hz ± 0.2 Hz. To ensure that the correct power supply can be easily identified in the circuit diagram, two numbers are used in the grid, indicating the circuit's sequence number. These numbers are entered in the section "Page setup under Comment 2" and can then be found in the labeling field of the sheet in the bottom right. Also, the current path, which is the column where the symbol is located.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/400V.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/400v.kicad_sym">400 V 50 Hz</a></figcaption></h3>
<p>Als Dreiphasenwechselstrom – nach Bezug auch als Dreiphasenwechselspannung oder kurz als Drehstrom bezeichnet – wird in der Elektrotechnik eine Form von Mehrphasenwechselstrom benannt, die aus drei einzelnen Wechselströmen oder Wechselspannungen gleicher Frequenz besteht, die zueinander in ihren Phasenwinkeln fest um 120° verschoben sind.
</p><p>
Umgangssprachlich wird der Dreiphasenwechselstrom als Starkstrom bezeichnet, was nicht korrekt oder zumindest ungenau ist.[Quelle: Dreiphasenwechselstrom, Wikipedia]</p>
<h4></h4>
<p>The term "Three-Phase Alternating Current," also referred to as three-phase alternating voltage or simply as three-phase current, is used in electrical engineering to describe a form of multiphase alternating current composed of three individual alternating currents or voltages with the same frequency, each shifted in phase by a fixed 120 degrees relative to each other.
</p><p>
Colloquially, three-phase alternating current is often referred to as "high voltage," which is not correct or, at the very least, imprecise.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/PEN.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/PEN.kicad_sym">PEN</a></figcaption></h3>
<p>
In der Elektrotechnik steht "PEN" für "Protective Earth and Neutral" (Schutzleiter und Neutralleiter) oder "Combined Protective Earth and Neutral Conductor" (kombinierter Schutzleiter- und Neutralleiter). Der PEN-Leiter ist ein gemeinsamer Leiter, der die Funktionen des Neutralleiters (N) und des Schutzleiters (PE) in einem einzigen Leiter kombiniert. Hier sind die Hauptfunktionen dieser beiden Leiter:
</p><ol><li>
<b><a id="Schutzleiter">Schutzleiter (PE)</b>: Der Schutzleiter dient als Erdung für elektrische Geräte und schafft einen sicheren Pfad für den Strom, der im Falle eines Defekts (z. B. einem Kurzschluss) fließen könnte. Dies minimiert das Risiko eines elektrischen Schlags für Personen und sorgt für den Schutz von Geräten.
</li><li>
<b><a id="Neutralleiter">Neutralleiter (N)</b>: Der Neutralleiter ist normalerweise der Rückleiter des Stromkreises. Er leitet den Rückstrom ab und bildet den elektrischen Bezugspunkt für elektrische Geräte. Der Neutralleiter trägt den nicht verbrauchten Strom von den Verbrauchern zurück zum Versorgungsnetz.
</li></ol><p>
Der PEN-Leiter vereint diese Funktionen in einem einzigen Leiter. In bestimmten elektrischen Systemen und Installationsarten wird der PEN-Leiter verwendet, um die Leitungsführung zu vereinfachen und Materialkosten zu reduzieren. Es ist jedoch wichtig zu beachten, dass der PEN-Leiter unter bestimmten Bedingungen getrennt werden kann, um sicherzustellen, dass im Fehlerfall der Schutzleiter weiterhin ordnungsgemäß funktioniert.</p>
<h4></h4>
<p>In electrical engineering, "PEN" stands for "Protective Earth and Neutral" or "Combined Protective Earth and Neutral Conductor." The PEN conductor is a common conductor that combines the functions of the neutral conductor (N) and the protective earth conductor (PE) into a single conductor. Here are the main functions of these two conductors:
</p><ol><li>
<b><a id="Earth_Conductor">Protective Earth Conductor (PE)</b>: The protective earth conductor provides grounding for electrical devices, creating a safe path for current in the event of a fault, such as a short circuit. This minimizes the risk of electric shock to individuals and protects devices.
</li><li>
<b><a id="Neutral_Conductor">Neutral Conductor (N)</b>: The neutral conductor is typically the return path of the circuit, carrying the unutilized current from the consumers back to the power supply. It serves as the electrical reference point for electrical devices.
</li></ol><p>
The PEN conductor combines these functions into a single conductor. In certain electrical systems and installation types, the PEN conductor is used to simplify wiring and reduce material costs. However, it is important to note that the PEN conductor can be separated under specific conditions to ensure that the protective earth conductor continues to function properly in case of a fault.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/PE.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/PE.kicad_sym">PE</a></figcaption></h3>
<p>[Siehe oben](#Schutzleiter)</p>
<h4></h4>
<p>[Please refer](#Earth_Conductor)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/N.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/N.kicad_sym">N</a></figcaption></h3>
<p>[Siehe oben](#Neutralleiter)</p>
<h4></h4>
<p>[Please refer](#Neutral_Conductor)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/L1.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/L1.kicad_sym">L1</a></figcaption></h3>
<p><a id="Leiter_L1">In der Elektrotechnik steht "L1" für "Leiter 1" und bezieht sich oft auf den ersten Außenleiter oder Phase in einem dreiphasigen Wechselstromsystem. In einem dreiphasigen System gibt es normalerweise drei Phasenleiter, die als L1, L2 und L3 bezeichnet werden. Diese Bezeichnungen sind in der Elektrotechnik standardisiert, um die verschiedenen Phasen innerhalb eines Systems zu kennzeichnen.
</p><ul>
Zum Beispiel in einem europäischen System, könnten die Bezeichnungen folgendermaßen verwendet werden:
<li>
L1: Phase 1 (erster Außenleiter)
</li><li>
L2: Phase 2 (zweiter Außenleiter)
</li><li>
L3: Phase 3 (dritter Außenleiter)
</li></ul>
<p>
Diese Phasen sind elektrisch um 120 Grad voneinander versetzt und werden verwendet, um Wechselstromsysteme mit höherer Effizienz und Leistung zu betreiben, insbesondere in industriellen Anwendungen. L1 repräsentiert daher den ersten dieser Phasenleiter.</p>
<h4></h4>
<p><a id="Line_L1">
In electrical engineering, "L1" stands for "Line 1" and often refers to the first phase conductor or line in a three-phase alternating current system. In a three-phase system, there are typically three phase conductors designated as L1, L2, and L3. These designations are standardized in electrical engineering to identify the different phases within a system.
</p><ul>
For example, in a European system, the designations might be used as follows:
<li>
L1: Phase 1 (first phase conductor)
</li><li>
L2: Phase 2 (second phase conductor)
</li><li>
L3: Phase 3 (third phase conductor)
</li></ul>
<p>
These phases are electrically offset by 120 degrees from each other and are utilized to operate three-phase alternating current systems with enhanced efficiency and power, particularly in industrial applications. Therefore, L1 represents the first of these phase conductors.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/L2.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/L2.kicad_sym">L2</a></figcaption></h3>
<p>[Siehe oben](#Leiter_L1)</p>
<h4></h4>
<p>[Please refer](#Line_L1)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/L3.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/L3.kicad_sym">L3</a></figcaption></h3>
<p>[Siehe oben](#Leiter_L1)</p>
<h4></h4>
<p>[Please refer](#Line_L1)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/Earth.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/Earth.kicad_sym">Erde</a></figcaption></h3>
<p>
Die Farbgebung von Stromleitern für Gleichstrom (DC) in Niederspannungssystemen wie 6V, 12V, 24V, 36V, 48V und 52V kann je nach den örtlichen Standards und Vorschriften variieren. Es gibt jedoch einige allgemeine Konventionen, die in vielen Regionen weltweit befolgt werden. Hier sind typische Farbcodes für positive und negative Leiter in Niederspannungsgleichstromsystemen:
</p><ul><li>
<a id="6V">6V, 12V, 24V:
<br>
<b>Positiver Leiter:</b> Rot<br>
<b>Negativer Leiter:</b> Schwarz oder Braun<br>
</li><li>
<a id="36V">36V:
<br>
<b>Positiver Leiter:</b> Rot<br>
<b>Negativer Leiter:</b> Schwarz oder Braun<br>
<b>Alternativ:</b> Blau für den negativen Leiter
</li><li>
<a id="48V">48V:
<br>
<b>Positiver Leiter:</b> Rot<br>
<b>Negativer Leiter:</b> Schwarz oder Braun<br>
<b>Alternativ:</b> Blau für den negativen Leiter<br>
<b>Zusätzlich:</b> Grau für den negativen Leiter
</li><li>
<a id="52V">52V:
<br>
<b>Positiver Leiter:</b> Rot<br>
<b>Negativer Leiter:</b> Schwarz oder Braun<br>
<b>Alternativ:</b> Blau für den negativen Leiter<br>
<b>Zusätzlich:</b> Grau für den negativen Leiter
</li></ul>
<p>
Weitere Alternativen: Grün oder Gelb für den negativen Leiter
Es ist wichtig zu beachten, dass diese Farbcodes Richtlinien und Konventionen sind, aber sie können je nach spezifischem Anwendungsbereich, Hersteller oder lokalen Vorschriften variieren. Daher ist es ratsam, die örtlichen Normen und Vorschriften zu überprüfen, um sicherzustellen, dass die Farbcodierung den örtlichen Standards entspricht.</p>
<h4>Earth</h4>
<p>The color coding of conductors for direct current (DC) in low-voltage systems like 6V, 12V, 24V, 36V, 48V, and 52V may vary depending on local standards and regulations. However, there are some general conventions followed in many regions worldwide. Here are typical color codes for positive and negative conductors in low-voltage DC systems:
</p><ul><li>
<a id="V6">6V, 12V, 24V:
<br>
<b>Positive Conductor:</b> Red<br>
<b>Negative Conductor:</b> Black or Brown
</li><li>
<a id="V36">36V:
<br>
<b>Positive Conductor:</b> Red<br>
<b>Negative Conductor:</b> Black or Brown<br>
<b>Alternative:</b> Blue for the negative conductor
</li><li>
<a id="V48">48V:
<br>
<b>Positive Conductor:</b> Red<br>
<b>Negative Conductor:</b> Black or Brown<br>
<b>Alternative:</b> Blue for the negative conductor<br>
<b>Additional:</b> Gray for the negative conductor
</li><li>
<a id="V52">52V:
<br>
<b>Positive Conductor:</b> Red<br>
<b>Negative Conductor:</b> Black or Brown<br>
<b>Alternative:</b> Blue for the negative conductor<br>
<b>Additional:</b> Gray for the negative conductor<br>
<b>Further Alternatives:</b> Green or Yellow for the negative conductor
</li></ul>
<p>
It's important to note that these color codes are guidelines and conventions, and they may vary depending on the specific application, manufacturer, or local regulations. Therefore, it's advisable to check local standards and regulations to ensure that the color coding aligns with local practices.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/+52v.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/+52v.kicad_sym">+52 V</a></figcaption></h3>
<p>[Siehe oben](#52V)</p>
<h4></h4>
<p>[Please refer](#V52)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/+48v.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/+48v.kicad_sym">+48 V</a></figcaption></h3>
<p>[Siehe oben](#48V)</p>
<h4></h4>
<p>[Please refer](#V48)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/+36v.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/+36v.kicad_sym">+36 V</a></figcaption></h3>
<p>[Siehe oben](#36V)</p>
<h4></h4>
<p>[Please refer](#V36)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/+24v.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/+24v.kicad_sym">+24 V</a></figcaption></h3>
<p>[Siehe oben](#6V)</p>
<h4></h4>
<p>[Please refer](#V6)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/+12v.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/+12v.kicad_sym">+12 V</a></figcaption></h3>
<p>[Siehe oben](#6V)</p>
<h4></h4>
<p>[Please refer](#V6)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/+6v.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/+6v.kicad_sym">+6 V</a></figcaption></h3>
<p>[Siehe oben](#6V)</p>
<h4></h4>
<p>[Please refer](#V6)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        </ul>
<h2>Netzsysteme/Network systems</h2>
<ul>
        <li>
        <figure>  
          <img src="images/Network_TT_system.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/Network_IT_system.kicad_sym">IT Netz</a></figcaption></h3>
<p>Das IT-System (Isoliertes Teilungsnetz) ist eine spezielle Form der elektrischen Stromversorgung in der Elektrotechnik, bei der ein zweipoliges Netz verwendet wird. Hier sind einige grundlegende Merkmale des IT-Netzwerks:
<ol><li><b>
Isolation (I):</b> Das "I" steht für "Isoliert". Im IT-System wird der aktive Leiter (Phase) gegenüber Erde isoliert betrieben. Das bedeutet, dass es keine direkte Verbindung zwischen dem aktiven Leiter und Erde gibt.
</li><li><b>
Zweipoligkeit (T):</b> Das "T" steht für "Teilung" oder "Zweipoligkeit". Das IT-Netz ist zweipolig, was bedeutet, dass es zwei aktive Leiter gibt: einen positiven und einen negativen Leiter. Diese Leiter sind gegenüber Erde isoliert.
</li><li><b>
Erster Fehler bleibt unerkannt:</b> Ein besonderes Merkmal des IT-Netzwerks ist, dass es so konzipiert ist, dass der erste Fehler im System nicht erkannt wird. Wenn ein Fehler auftritt (zum Beispiel ein Kurzschluss zwischen einem aktiven Leiter und Erde), wird dies im ersten Moment nicht erkannt, und der Betrieb wird fortgesetzt. Der Fehler wird jedoch aufgezeichnet und kann später lokalisiert und behoben werden.
</li><li><b>
Anwendungsbereiche:</b> IT-Systeme werden oft in Umgebungen eingesetzt, in denen eine kontinuierliche Stromversorgung unerlässlich ist, wie beispielsweise in Krankenhäusern, Rechenzentren oder anderen kritischen Anwendungen. Die zweipolige Struktur bietet eine erhöhte Betriebssicherheit.
</li></ol>
<p>
Es ist wichtig zu beachten, dass die Wahl des geeigneten Netzsystems von verschiedenen Faktoren, einschließlich Sicherheitsanforderungen und lokalen Vorschriften, abhängt. Das IT-Netzwerk ist eine von mehreren möglichen Konfigurationen in der Elektrotechnik.</p>
<h4>Network IT systems</h4>
<p>The IT system (Isolated System) is a specific form of electrical power supply in electrical engineering that utilizes a two-pole network. Here are some basic features of the IT network:
<ol><li><b>
Isolation (I):</b> The "I" stands for "Isolated." In the IT system, the active conductor (phase) is operated isolated from the ground. This means there is no direct connection between the active conductor and ground.
</li><li><b>
Two-Pole (T):</b> The "T" stands for "Two-Pole" or "Division." The IT network is two-pole, meaning there are two active conductors: one positive and one negative conductor. These conductors are isolated from the ground.
</li><li><b>
First Fault Remains Undetected:</b> A distinctive feature of the IT network is that it is designed so that the first fault in the system remains undetected. If a fault occurs (for example, a short circuit between an active conductor and ground), it is not immediately detected, and operations continue. However, the fault is recorded and can be later located and rectified.
</li><li><b>
Applications:</b> IT systems are often employed in environments where continuous power supply is critical, such as in hospitals, data centers, or other critical applications. The two-pole structure provides increased operational safety.
</li></ol>
<p>
It's important to note that the choice of the appropriate network system depends on various factors, including safety requirements and local regulations. The IT network is one of several possible configurations in electrical engineering.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/Network_IT_systems.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/Network_IT_systems.kicad_sym">IT Netz</a></figcaption></h3>
<p>
In der Elektrotechnik bezieht sich das "IT-Netz" auf ein isoliertes Netzwerk, auch als "Isolationsüberwachungsnetz" bekannt. Diese Art von Netzwerk ist charakterisiert durch:</p>
<ol><li><b>
Isolierte Spannungsquelle:</b> Im IT-Netz gibt es eine isolierte Spannungsquelle, die nicht direkt mit der Erde verbunden ist. Anders als bei anderen Netzwerken, bei denen einer der Leiter geerdet ist (z. B. TN-System), ist bei einem IT-System keiner der Leiter direkt mit der Erde verbunden.
</li><li><b>
Vierpoliges System:</b> Ein IT-Netz besteht aus vier Leitern oder Polen – drei aktive Phasenleitern und einem Neutralleiter. Die vierpolige Struktur ermöglicht die Versorgung von Drehstromverbrauchern.
</li><li><b>
Überwachung der Isolation:</b> Das charakteristische Merkmal eines IT-Netzwerks ist die ständige Überwachung der Isolation zwischen den aktiven Leitern und der Erde. Dies wird durch Isolationsüberwachungseinrichtungen wie Isolationsüberwachungsgeräte (IÜG) erreicht.
</li><li><b>
Erdfehler:</b> Das IT-Netz toleriert einen Einzelerdfehler, ohne dass es zu einem sofortigen Kurzschluss oder Ausfall kommt. Stattdessen wird der Fehler in der Regel überwacht, und im Falle eines Erdfehlers wird ein Alarm ausgelöst, um darauf hinzuweisen, dass eine Reparatur erforderlich ist.
</li></ol>
<p>
IT-Netze werden häufig in Umgebungen eingesetzt, in denen eine besonders hohe Verfügbarkeit und Kontinuität der Stromversorgung erforderlich ist, wie beispielsweise in Krankenhäusern, Rechenzentren oder anderen kritischen Infrastrukturen.</p>
<h4>Network IT systems</h4>
<p>
In electrical engineering, an "IT network" refers to an insulated network, also known as an "Insulation Monitoring Network." This type of network is characterized by:</p>
<ol><li><b>
Isolated Voltage Source:</b> In an IT network, there is an isolated voltage source that is not directly connected to the ground. Unlike other networks where one of the conductors is grounded (e.g., TN system), in an IT system, none of the conductors is directly connected to the ground.
</li><li><b>
Four-Pole System:</b> An IT network consists of four conductors or poles – three active phase conductors and one neutral conductor. The four-pole structure allows for the supply of three-phase loads.
</li><li><b>
Isolation Monitoring:</b> The distinctive feature of an IT network is the continuous monitoring of the insulation between the active conductors and the ground. This is achieved through insulation monitoring devices (IMDs).
</li><li><b>
Earth Fault Tolerance: The IT network tolerates a single earth fault without an immediate short circuit or failure. Instead, the fault is typically monitored, and in the event of an earth fault, an alarm is triggered to indicate that repairs are needed.
</li></ol>
<p>
IT networks are commonly used in environments where high availability and continuity of power supply are crucial, such as in hospitals, data centers, or other critical infrastructures.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/Network_TNC_systems.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/Network_TNC_systems.kicad_sym">TN-C Netz</a></figcaption></h3>
<p>Das TN-C-Netz (auch als PEN-Netz bezeichnet) ist eine Form des elektrischen Erdungssystems, die in der Elektrotechnik verwendet wird. Der Begriff "TN" steht für "Terra-Neutral" und gibt an, wie der Schutzleiter (PE), der Neutralleiter (N) und der Erdleiter (E) in einem System angeordnet sind.</p>
<ol>
In einem TN-C-System sind der Schutzleiter (PE) und der Neutralleiter (N) zu einem einzigen Leiter kombiniert. Der kombinierte Leiter wird als PEN-Leiter bezeichnet. Hier sind die Hauptmerkmale des TN-C-Netzes:
<li><b>
Schutzleiter (PE):</b> Der Schutzleiter ist für die Erdung von Geräten und die Sicherheit von Personen verantwortlich. Im TN-C-System ist er mit dem Neutralleiter kombiniert.
</li><li><b>
Neutralleiter (N):</b> Der Neutralleiter trägt den nicht verbrauchten Strom von den Verbrauchern zurück zum Versorgungsnetz.
</li><li><b>
Erdleiter (E):</b> Der Erdleiter dient als Verbindung zum Erdpotential und gewährleistet die Erdung des Systems.
</li></ol>
<p>
Es ist wichtig zu beachten, dass das TN-C-Netz wegen möglicher Sicherheitsrisiken weniger verbreitet ist. Wenn der PEN-Leiter im Fehlerfall unterbrochen wird, besteht das Risiko, dass leitfähige Teile unter Spannung stehen, was zu gefährlichen Situationen führen kann. In vielen modernen Systemen wird das TN-C-Netz durch sicherere Erdungssysteme wie das TN-S oder TN-C-S-System ersetzt.</p>
<h4>Network TN-C systems</h4>
<p>The TN-C system (also known as PEN system) is a form of electrical grounding system used in electrical engineering. The term "TN" stands for "Terra-Neutral," indicating how the protective earth (PE), neutral (N), and earth (E) conductors are arranged in a system.</p>
<ol>
In a TN-C system, the protective earth (PE) and neutral (N) conductors are combined into a single conductor, referred to as the PEN conductor. Here are the main features of the TN-C system:
<li><b>
Protective Earth (PE):</b> The protective earth is responsible for grounding devices and ensuring the safety of individuals. In the TN-C system, it is combined with the neutral conductor.
</li><li><b>
Neutral Conductor (N):</b> The neutral conductor carries the unutilized current from consumers back to the power supply.
</li><li><b>
Earth Conductor (E):</b> he earth conductor serves as a connection to the earth potential, ensuring the grounding of the system.
</li></ol>
<p>
It is essential to note that the TN-C system is less common due to potential safety risks. If the PEN conductor is interrupted in the event of a fault, there is a risk of conductive parts becoming live, leading to hazardous situations. In many modern systems, the TN-C system is replaced by safer grounding systems such as the TN-S or TN-C-S system.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/Network_TNS_systems.png" height="120"/>
          <h3><figcaption><a href="images/Network_TNS_systems.png">TN-S Netz</a></figcaption></h3>
<p>In der Elektrotechnik steht "TN-S" für eine spezifische Art von Erdungssystem, das in elektrischen Netzwerken verwendet wird. Der Buchstabe "T" steht für das französische Wort "terre", was auf Deutsch "Erde" bedeutet. Der Buchstabe "N" repräsentiert den Neutralleiter, und "S" steht für "Separated" (getrennt).</p>
<ol>Im TN-S-System sind der Schutzleiter (PE) und der Neutralleiter (N) voneinander getrennt. Hier sind die Hauptmerkmale des TN-S-Netzwerks:
<li><b>
Schutzleiter (PE):</b> Der Schutzleiter ist der Erdungskonduktor, der die Metallteile der elektrischen Geräte mit dem Erdboden verbindet. Er stellt einen sicheren Pfad für elektrischen Strom bereit, der im Falle eines Fehlerstroms, beispielsweise bei einem Kurzschluss, abfließen kann.
</li><li><b>
Neutralleiter (N):</b> Der Neutralleiter ist der Rückleiter des Stromkreises. Er trägt den nicht verbrauchten Strom von den Verbrauchern zurück zur Stromquelle.
</li><li><b>
Trennung von PE und N:</b> Im TN-S-System sind der Schutzleiter und der Neutralleiter bis zum Ursprung der Installation getrennt. Das bedeutet, dass sie getrennte Wege zurück zum Transformator oder zum Stromerzeuger haben.
</li></ol>
<p>
Das TN-S-System ist eines von mehreren Erdungssystemen, die in elektrischen Installationen angewendet werden können. Die Auswahl des geeigneten Systems hängt von den örtlichen Vorschriften, den Anforderungen der Anwendung und anderen spezifischen Bedingungen ab.</p>
<h4>Network TN-S systems</h4>
<p>In electrical engineering, "TN-S" refers to a specific type of grounding system used in electrical networks. The letter "T" stands for the French word "terre," which translates to "earth" in English. The letter "N" represents the neutral conductor, and "S" stands for "Separated."</p>
<ol>
In the TN-S system:
<li><b>
Protective Earth (PE):</b> The protective earth is the grounding conductor that connects the metal parts of electrical devices to the ground. It provides a safe path for electrical current to flow in the event of a fault, such as a short circuit.
</li><li><b>
Neutral Conductor (N):</b> The neutral conductor is the return path of the circuit, carrying the unutilized current from the consumers back to the power source.
</li><li><b>
Separation of PE and N:</b> In the TN-S system, the protective earth and the neutral conductor are separated until the origin of the installation. This means they have separate paths back to the transformer or power generator.
</li></ol>
<p>
The TN-S system is one of several grounding systems that can be applied in electrical installations. The choice of the appropriate system depends on local regulations, application requirements, and specific conditions.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/Network_TNCS_systems.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/Network_TNCS_systems.kicad_sym">TN-C-S Netz</a></figcaption></h3>
<p>
Das TN-C-S-Netz ist eine spezifische Art von Erdungssystem in der Elektrotechnik und bezieht sich auf die Art und Weise, wie Schutzleiter und Neutralleiter in einem elektrischen Netzwerk angeordnet sind. Hier sind die Bedeutungen der Buchstaben in "TN-C-S":</p>
<ol><li><b>
T (Terre - Earth):</b> Dies steht für Erdung. In einem TN-C-S-System sind der Schutzleiter (PE) und der Neutralleiter (N) zunächst gemeinsam geführt, also ein kombinierter Leiter.
</li><li><b>
N (Neutral):</b>  Dies steht für den Neutralleiter. Der Neutralleiter führt den nicht verbrauchten Strom von den Verbrauchern zurück zum Versorgungsnetz.
</li><li><b>
C (Combined - Kombiniert):</b>  Dies bedeutet, dass der Schutzleiter und der Neutralleiter zunächst kombiniert sind, jedoch später im Netzwerk getrennt werden.
</li></ol>
<p>
Im Detail bedeutet dies, dass in einem TN-C-S-System der Schutzleiter und der Neutralleiter bis zu einem bestimmten Punkt gemeinsam geführt werden. Dieser Punkt wird als "Kombinationspunkt" bezeichnet. Danach werden der Schutzleiter und der Neutralleiter getrennt. Der Schutzleiter wird dann als separates System fortgeführt und als PE (Protective Earth) bezeichnet, während der Neutralleiter seine ursprüngliche Funktion behält.
</p><p>
Es ist wichtig zu beachten, dass die Auswahl des Erdungssystems von verschiedenen Faktoren abhängt, einschließlich lokaler Vorschriften, Art der Installation und Sicherheitsanforderungen. Verschiedene Erdungssysteme wie TN-C-S, TN-S, TN-C, IT und TT werden je nach den spezifischen Anforderungen und Bedingungen verwendet.</p>
<h4>Network TN-C-S systems</h4>
<p>The TN-C-S system is a specific type of grounding system in electrical engineering, indicating the arrangement of protective conductors and neutral conductors in an electrical network. Here is the breakdown of the letters in "TN-C-S":</p>
<ol><li><b>
T (Terre - Earth):</b>  This represents grounding. In a TN-C-S system, the protective conductor (PE) and the neutral conductor (N) are initially combined, forming a single conductor.
</li><li><b>
N (Neutral):</b>  This stands for the neutral conductor, which carries the unutilized current back from the consumers to the power supply.
</li><li><b>
C (Combined):</b>  This means that the protective conductor and the neutral conductor are initially combined but are later separated in the network.
</li></ol>
<p>
In detail, in a TN-C-S system, the protective conductor and the neutral conductor are combined until a specific point, known as the "combination point." After this point, the protective conductor and the neutral conductor are separated. The protective conductor then continues as a separate system, designated as PE (Protective Earth), while the neutral conductor retains its original function.
</p><p>
It's essential to note that the choice of the grounding system depends on various factors, including local regulations, the type of installation, and safety requirements. Different grounding systems, such as TN-C-S, TN-S, TN-C, IT, and TT, are used based on specific requirements and conditions.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/Network_TT_systems.png" height="120"/>
          <h3><figcaption><a href="images/Network_TT_systems.png">TT Netz</a></figcaption></h3>
<p>Das "TT-System" (Terre-Terre-System) ist eine spezifische Erdungsanordnung in der Elektrotechnik, die zur Gewährleistung der Sicherheit in elektrischen Installationen verwendet wird. Hier sind die grundlegenden Merkmale des TT-Systems:</p>
<ol><li><b>
Lokales Erdungssystem (Terre):</b> Jede elektrische Installation hat ihr eigenes lokales Erdungssystem, das unabhängig von anderen Installationen ist. Dieses lokale Erdungssystem ist mit einer lokalen Erdungselektrode verbunden, die normalerweise in den Boden eingebracht wird.
</li><li><b>
Exponierte leitfähige Teile (Terre):</b> Die exponierten leitfähigen Teile der elektrischen Installation, wie Metallgehäuse von Geräten oder elektrische Leitungen, sind ebenfalls mit dem lokalen Erdungssystem verbunden.
</li></ol>
<p>
Die Idee hinter dem TT-System ist, dass jede elektrische Installation ihren eigenen lokalen Erdungspunkt hat, der unabhängig von anderen ist. Dies ist besonders relevant in Situationen, in denen keine zuverlässige Verbindung zu einem niederohmigen Erdungspunkt verfügbar ist.
</p><p>
Es ist wichtig zu beachten, dass die Wahl des Erdungssystems von verschiedenen Faktoren abhängt, darunter die Art der Installation, Sicherheitsanforderungen und örtliche Vorschriften. Das TT-System ist eine von mehreren Erdungskonfigurationen, die in elektrischen Netzwerken eingesetzt werden.</p>
<p>Der PEN ist grundsätzlich so früh wie möglich aufzuteilen, also am Besten schon am HAK.
(siehe hierzu TAB und DIN 18015, sowie VDE 0100 Teil 444 und 482)
Der PEN muß in gesamter Länge grün gelb sein und an den Enden blau markiert werden!</p>
<h4>Network TT systems</h4>
<p>The "TT System" (Terre-Terre System) is a specific grounding arrangement in electrical engineering used to ensure safety in electrical installations. Here are the fundamental characteristics of the TT system:</p>
<ol><li><b>
Local Grounding System (Terre):</b> Each electrical installation has its own local grounding system that is independent of others. This local grounding system is connected to a local ground electrode, typically inserted into the ground.
</li><li><b>
Exposed Conductive Parts (Terre):</b> The exposed conductive parts of the electrical installation, such as metal enclosures of devices or electrical conductors, are also connected to the local grounding system.
</li></ol>
<p>
The concept behind the TT system is that each electrical installation has its own local grounding point, which is independent of others. This is particularly relevant in situations where a reliable connection to a low-impedance grounding point is not available.
</p><p>
It is important to note that the choice of the grounding system depends on various factors, including the type of installation, safety requirements, and local regulations. The TT system is one of several grounding configurations used in electrical networks.</p><p>The PEN should generally be divided as early as possible, ideally at the HAK.
(see TAB and DIN 18015, as well as VDE 0100 parts 444 and 482)
The PEN must be green yellow along its entire length and marked blue at the ends!</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        [Zum Anfang](#top)/[above](#top)<hr></ul>
<h2>PV Systeme/PV systems</h2>
<ul>
      <li>
        <figure>  
          <img src="images/PV_modul.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/PV_modul.kicad_sym">PV modul</a></figcaption></h3>
<p>Als Photovoltaikmodul oder PV-Modul wird der Verbund mehrerer Solarzellen – die kleinste Einheit einer Photovoltaikanlage – bezeichnet. Innerhalb der Zellen wird die Energie der Sonne in elektrischen Strom umgewandelt.</p>
<h4></h4>
<p>The combination of several solar cells - the smallest unit of a photovoltaic system - is called a photovoltaic module or PV module. Within the cells, the sun's energy is converted into electrical current.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/PV_inverter_one_string.png" height="120"/>
          <h3><figcaption><a href="images/PV_inverter_one_string.png">PV-Wechselrichter ein String</a></figcaption></h3>
<p><a id="Wechselrichter">Ein String-Wechselrichter ist ein Wechselrichter mit einem oder mehreren MPP-Trackern. Pro Tracker können einer oder mehrere Strings (dt.: Stränge) angeschlossen werden. Es können Multistrings, also mehrere Strings mit der gleichen Dachausrichtung und -neigung, an einem MPP-Tracker verschaltet werden.<p>
<h4>PV inverter one string</h4>
<p><a id="Inverter">A string inverter is an inverter with one or more MPP trackers. One or more strings can be connected per tracker. Multistrings, i.e. several strings with the same roof orientation and inclination, can be connected to an MPP tracker.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
     <li>
        <figure>  
          <img src="images/PV_inverter_dual_string.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/PV_inverter_dual_string.kicad_sym">PV-Wechselrichter zwei Strings</a></figcaption></h3>
<p>[Siehe oben](#Wechselrichter)</p>
<h4>PV inverter dual strings</h4>
<p>[Please refer](#Inverter)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="images/PV_inverter_dual_string_3P.png" height="120"/>
          <h3><figcaption><a href="public/Power_supply/PV_inverter_dual_string_3P.kicad_sym">PV-3-Phasiger Wechselrichter zwei Strings</a></figcaption></h3>
<p>[Siehe oben](#Wechselrichter)</p>
<h4>PV 3-phase inverter dual strings</h4>
<p>[Please refer](#Inverter)</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
           <li>
        <figure>  
          <img src="images/PV_storage.png" height="120"/>
          <h3><figcaption><a href="images/PV_storage.png">PV-Stromspeicher</a></figcaption></h3>
<p>Ein Stromspeicher für Solaranlagen, auch als Solarbatterie oder Solarspeicher bezeichnet, ist eine Einrichtung zur Speicherung des von Photovoltaikanlagen erzeugten elektrischen Stroms. Hier sind die grundlegenden Funktionen und Merkmale eines solchen Stromspeichers:</p><ol><li><p><strong>Speicherung von überschüssigem Strom:</strong> Photovoltaikanlagen erzeugen Strom, wenn die Sonne scheint, unabhängig davon, ob dieser Strom sofort benötigt wird. Ein Solarstromspeicher ermöglicht es, den überschüssigen Strom, der bei gutem Sonnenlicht produziert wird, zu speichern und zu einem späteren Zeitpunkt zu nutzen.</p></li><li><p><strong>Eigenverbrauchsoptimierung:</strong> Durch die Speicherung von Solarenergie kann der Eigenverbrauch im Haushalt erhöht werden. Anstatt den erzeugten Strom in das öffentliche Netz einzuspeisen (und möglicherweise nur eine geringe Vergütung zu erhalten), kann der gespeicherte Strom später genutzt werden, wenn die Sonne nicht scheint.</p></li><li><p><strong>Netzunabhängigkeit:</strong> Mit einem Solarstromspeicher kann ein gewisser Grad an Unabhängigkeit vom öffentlichen Stromnetz erreicht werden. In Zeiten, in denen die Sonne nicht scheint oder bei Stromausfällen, kann der gespeicherte Solarstrom genutzt werden.</p></li><li><p><strong>Kostenersparnis:</strong> Durch die Nutzung des gespeicherten Solarstroms zur Deckung des eigenen Strombedarfs können die Stromkosten reduziert werden. Dies kann besonders wirtschaftlich sein, wenn die Netzeinspeisevergütung für Solarstrom niedrig ist.</p></li><li><p><strong>Umweltfreundlichkeit:</strong> Die Nutzung von Solarstrom und dessen Speicherung trägt zur Reduzierung des Bedarfs an konventionell erzeugtem Strom bei, was wiederum zu einer Verringerung der Umweltauswirkungen durch fossile Brennstoffe beiträgt.</p></li></ol><p>Es gibt verschiedene Arten von Solarstromspeichern, darunter Lithium-Ionen-Batterien, Blei-Säure-Batterien und andere Technologien. Die Auswahl hängt von den spezifischen Anforderungen, dem Budget und den Umweltzielen des Anwenders ab.</p><p>
Der PV-Speicherstrom spielt eine entscheidende Rolle in der Effizienz und Nachhaltigkeit von Solaranlagen. Durch die Speicherung von überschüssigem Strom können Solarenergieanlagen auch dann Energie liefern, wenn die Sonne nicht scheint, was die Unabhängigkeit von konventionellen Energiequellen und den Einsatz erneuerbarer Energien fördert.</p>
<h4>PV storage</h4>
<p>A solar power storage system, also known as a solar battery or solar storage, is a device designed to store the electrical energy generated by photovoltaic (PV) solar panels. Here are the basic functions and features of such a solar power storage system:</p><ol><li><p><strong>Storage of Excess Energy:</strong> Photovoltaic systems generate electricity when the sun is shining, regardless of whether the power is immediately needed. A solar power storage system allows the excess energy produced during optimal sunlight conditions to be stored for later use.</p></li><li><p><strong>Optimization of Self-Consumption:</strong> By storing solar energy, self-consumption within a household or facility can be increased. Instead of feeding excess electricity back into the grid (potentially receiving only a low feed-in tariff), stored solar power can be utilized during periods when the sun is not shining.</p></li><li><p><strong>Grid Independence:</strong> A solar power storage system provides a degree of independence from the public power grid. During times when sunlight is insufficient or in the event of power outages, the stored solar energy can be used.</p></li><li><p><strong>Cost Savings:</strong> Utilizing stored solar power to meet on-site electricity needs can lead to reduced electricity costs. This becomes particularly economically advantageous when feed-in tariffs for solar power are low.</p></li><li><p><strong>Environmental Benefits:</strong> The use of solar power and its storage contributes to a reduction in the reliance on conventionally generated electricity, thereby lowering environmental impacts associated with fossil fuels.</p></li></ol><p>There are various types of solar power storage systems, including lithium-ion batteries, lead-acid batteries, and other technologies. The choice depends on specific requirements, budget considerations, and environmental goals.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
 <!--         <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
        <li>
        <figure>  
          <img src="" height="120"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<p></p>
<h4></h4>
<p></p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>

       <!-- [Siehe oben](#)/[Please refer](#) --> 
