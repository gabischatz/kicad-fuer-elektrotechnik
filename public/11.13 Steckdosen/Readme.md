public/11.13 Steckdosen/11-13-01.kicad_sym<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Steckdosen</h1></th>
<th width="50%"><h1>Socles de prises de courant</h1></th>
</tr>
</thead><tbody><tbody></table>
<h2>11.13</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-13-01.png" height="120"/>
        <h3><figcaption><a href="11-13-01.kicad_sym">Steckdose, allgemein</a></figcaption></h3>    
        <p>11-13-01</p>
        <h4>Socle de prise de courant (puissance), symbole général</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-13-02.png" height="120"/>
        <h3><figcaption><a href="11-13-02.kicad_sym">Mehrfachsteckdose</a></figcaption></h3>    
        <p>11-13-02</p>
        <h4>Socle pour plusieurs prises de courant (puissance)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-13-03.png" height="120"/>
        <h3><figcaption><a href="11-13-03.kicad_sym">Schutzkontaktsteckdose</a></figcaption></h3>    
        <p>11-13-03</p>
        <h4>Socle de prise de courant (puissance) avec contact pour conducteur de protection</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-13-04.png" height="120"/>
        <h3><figcaption><a href="11-13-04.kicad_sym">Schutzkontaktsteckdose</a></figcaption></h3>    
        <p>11-13-04</p>
        <h4>Socle de prise de courant (puissance) avec contact pour conducteur de protection</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-13-05.png" height="120"/>
        <h3><figcaption><a href="11-13-05.kicad_sym">Steckdose mit Abdeckung</a></figcaption></h3>    
        <p>11-13-05</p>
        <h4>Socle de prise de courant (puissance) avec volet d'obturation</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-13-06.png" height="120"/>
        <h3><figcaption><a href="11-13-06.kicad_sym">Steckdose, abschaltbar</a></figcaption></h3>    
        <p>11-13-06</p>
        <h4>Socle de prise de courant (puissance) avec interrupteur unipolaire</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-13-07.png" height="120"/>
        <h3><figcaption><a href="11-13-07.kicad_sym">Steckdose mit verriegeltem Schalter</a></figcaption></h3>    
        <p>11-13-07</p>
        <h4>Socle de prise de courant (puissance) avec interrupteur de verrouillage</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-13-08.png" height="120"/>
        <h3><figcaption><a href="11-13-08.kicad_sym">Steckdose mit Trenntrafo</a></figcaption></h3>    
        <p>11-13-08</p>
        <h4>Socle de prise de courant (puissance) avec transformateur de séparation, par exemple: prise pour rasoir</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-13-09.png" height="120"/>
        <h3><figcaption><a href="11-13-09.kicad_sym">Fernmeldesteckdose, allgemein</a></figcaption></h3>    
        <p>11-13-09</p>
        <h4>Socle de prise pour terminal de télécommunication</h4>      
        <p></p>
        </figure>
</li> 
</ul>
