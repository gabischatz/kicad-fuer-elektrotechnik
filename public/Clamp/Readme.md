<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Klemmen</h1></th>
<th width="50%"><h1>Clamps</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
        <p>In der Elektrotechnik bezieht sich der Begriff "Klemmen" auf elektrische Verbindungselemente, die dazu dienen, Leiter miteinander zu verbinden oder mit anderen elektrischen Komponenten zu verbinden. Hier sind einige grundlegende Merkmale und Funktionen von Klemmen:</p><ol><li><p><strong>Verbindung von Leitern:</strong> Klemmen ermöglichen die einfache und sichere Verbindung von elektrischen Leitern, sei es Draht, Kabel oder andere leitende Materialien. Sie stellen eine elektrische Verbindung her, um den Stromfluss zwischen den Leitern zu gewährleisten.</p></li><li><p><strong>Befestigung und Halterung:</strong> Klemmen können dazu dienen, Leiter an bestimmten Stellen zu befestigen oder zu halten. Sie bieten eine stabile und zuverlässige Befestigungslösung für elektrische Leitungen.</p></li><li><p><strong>Isolation:</strong> Einige Klemmen verfügen über isolierende Materialien, die dazu beitragen, die elektrische Verbindung zu schützen und sicherzustellen, dass keine ungewollten Kurzschlüsse oder Berührungen auftreten.</p></li><li><p><strong>Verschiedene Arten:</strong> Es gibt verschiedene Arten von Klemmen, darunter Schraubklemmen, Federklemmen, Klemmleisten und mehr. Die Wahl der Klemme hängt von den spezifischen Anforderungen der Anwendung ab.</p></li><li><p><strong>Anwendungen:</strong> Klemmen werden in einer Vielzahl von Anwendungen eingesetzt, von einfachen Haushaltsinstallationen bis hin zu komplexen industriellen Schaltschränken. Sie sind ein wesentlicher Bestandteil von Schaltungen und elektrischen Verdrahtungssystemen.</p></li><li><p><strong>Wartungsfreundlichkeit:</strong> Klemmen ermöglichen eine relative Leichtigkeit bei der Wartung von elektrischen Installationen. Durch lösbare Verbindungen können Leiter leicht entfernt oder hinzugefügt werden, ohne dass eine Neuverkabelung erforderlich ist.</p></li></ol><p>Es ist wichtig zu beachten, dass die Auswahl der geeigneten Klemme von verschiedenen Faktoren abhängt, einschließlich des Anwendungsbereichs, der Leitergröße und der spezifischen Anforderungen an die elektrische Verbindung. Klemmen sind unverzichtbare Bauteile in der Elektrotechnik, da sie eine sichere und zuverlässige elektrische Verbindung ermöglichen.</p>
      </td>
      <td>
        <p>In electrical engineering, the term "terminals" refers to electrical connection elements used to connect conductors with each other or with other electrical components. Here are some basic features and functions of terminals:</p><ol><li><p><strong>Conductor Connection:</strong> Terminals allow for the simple and secure connection of electrical conductors, whether they are wires, cables, or other conductive materials. They establish an electrical connection to ensure the flow of current between conductors.</p></li><li><p><strong>Fixation and Support:</strong> Terminals can be used to secure or support conductors at specific points. They provide a stable and reliable mounting solution for electrical wiring.</p></li><li><p><strong>Isolation:</strong> Some terminals feature insulating materials to protect the electrical connection and ensure that unintended short circuits or contacts do not occur.</p></li><li><p><strong>Various Types:</strong> There are different types of terminals, including screw terminals, spring terminals, terminal blocks, and more. The choice of terminal depends on the specific requirements of the application.</p></li><li><p><strong>Attachment and Holder:</strong> Terminals may serve to fasten conductors in specific locations or to act as holders for electrical connections. They contribute to the stability of the electrical system.</p></li><li><p><strong>Applications:</strong> Terminals are used in a variety of applications, from simple household installations to complex industrial control cabinets. They are essential components of circuits and electrical wiring systems.</p></li><li><p><strong>Maintenance-Friendly:</strong> Terminals facilitate ease of maintenance in electrical installations. Removable connections allow conductors to be easily added or removed without the need for rewiring.</p></li></ol><p>It's important to note that the selection of the appropriate terminal depends on various factors, including the application, conductor size, and specific requirements for the electrical connection. Terminals are indispensable components in electrical engineering, providing a secure and reliable means of making electrical connections.</p>
      </td>
  </tr><!--
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr> -->
  <tbody>
</table>
<h2><a href="https://hager.com/de/katalog/verteilersysteme/anschlusstechnik/hauptleitungsabzweigklemmen-und-zubehoer">Haupt­lei­tungs­ab­zweig­klem­men</a></h2>
<ul>
<li>
        <figure>  
        <img src="images/Grounding_systems.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Grounding_systems.kicad_sym">Anlagen Erder</a></figcaption></h3>    
        <p>Ein Anlagenerder sorgt für einen Potenzialausgleich zwischen metallenen Teilen einer Anlage und minimiert so die Gefahr von Spannungsunterschieden, die zu gefährlichen Strömen führen könnten.</p>
        <h4>Grounding systems</h4>      
        <p>A system earth electrode ensures potential equalization between metal parts of a system and thus minimizes the risk of voltage differences that could lead to dangerous currents.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_terminal_4p.png" height="120"/> <img src="images/Ferrules.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_terminal_4p.kicad_sym">Haupt-Abzweigklemme 4p</a></figcaption></h3>    
        <p>
Eine Haupt-Abzweigklemme mit 4 Polen (4P) ist ein elektrisches Verbindungselement, das in Stromverteilungssystemen verwendet wird, um den Hauptstromkreis von einem oder mehreren Abzweigkreisen zu trennen oder zu verbinden. Diese Klemme wird spezifisch mit einer Aderendhülse von 16,0 mm² / 18 mm L in Blau oder einer Aderendhülse von 10,0 mm² / 18 mm L in Rot für flexible Aderleitungen genutzt.</p>
        <h4>Clamp terminal 4p</h4>      
        <p>A main branch terminal with 4 poles (4P) is an electrical connecting device used in power distribution systems to separate or connect the main circuit from one or more branch circuits. This terminal is specifically utilized with a wire end sleeve of 16.0 mm² / 18 mm L in blue or a wire end sleeve of 10.0 mm² / 18 mm L in red for flexible wire conductors.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Clamp_terminal_4p_L1-3_N_PE.png" height="120"/><img src="images/Clamp_terminal_4p_L1-L3_N_PE.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_terminal_4p_L1-3_N_PE.kicad_sym">Haupt-Abzweigklemme 4p 5l</a></figcaption></h3>    
        <p>Vereinfachte Zeichnung der obigen Haupt-Abzweigklemme 4p, die es als Einzel oder gesammt Klemme gibt.</p>
        <h4>Clamp terminal 4p L1-3 N PE</h4>      
        <p>Simplified drawing of the main Clamp terminal 4p above, which are available as individual or collective clamps.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Clamp_terminal_4p_L1_L3_N_PE.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_terminal_4p_L1_L3_N_PE.kicad_sym">Haupt-Abzweigklemme 4p 5l</a></figcaption></h3>    
<p>Vereinfachte Zeichnung der obigen Haupt-Abzweigklemme 4p. Diese Art Klemme gibt es auch als 8p  Haupt-Abzweigklemme, verwenden Sie einfach zwei 4p Einzel-Haupt-Abzweigklemmen. </p>
        <h4>Clamp terminal 4p L1-3 N PE</h4>      
        <p>Simplified drawing of the main branch terminal 4p above. This type of terminal is also available as an 8p main branch terminal, just use two 4p single main branch terminals..</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_terminal_6p.png" height="120"/> <img src="images/Clamp_terminal_6p_L1-L3_N_PE.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_terminal_6p.kicad_sym">Haupt-Abzweigklemme 6p 5l</a></figcaption></h3>    
        <p>Vereinfachte Zeichnung der obigen Haupt-Abzweigklemme 6p, die es als Einzel oder gesammt Klemme gibt und die einzel Klemme nach belieben anodnen könen.</p>
        <h4>Clamp terminal 6p L1-3 N PE</h4>      
        <p>Simplified drawing of the main Clamp terminal 6p above, which are available as individual or collective clamps and you can arrange the individual terminals as you wish.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_terminal_6p_L1-3_N_PE.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_terminal_6p_L1-3_N_PE.kicad_sym">Haupt-Abzweigklemme 6p</a></figcaption></h3>    
       <p>Vereinfachte Zeichnung der obigen Haupt-Abzweigklemme 6p, die es als Einzel oder gesammt Klemme gibt.</p>
        <h4>Clamp terminal 4p L1-3 N PE</h4>      
        <p>Simplified drawing of the main Clamp terminal 6p above, which are available as individual or collective clamps.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> <!--
<li>
        <figure>  
        <img src="images/Clamp_terminal_4p_L1_L3_N_PE.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_terminal_4p_L1_L3_N_PE.kicad_sym">Haupt-Abzweigklemme 4p 5l</a></figcaption></h3>    
        <p></p>
        <h4>Clamp terminal 4p 5l</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  -->
</ul>
<h2><a href="https://hager.com/de/katalog/verteilersysteme/anschlusstechnik/pe-und-n-klemmen-quickconnect-und-zubehoer">PE- und N-Klem­men, quick­connect</a></h2>
<ul>
<li>
        <figure>  
        <img src="images/Clamp_VZ455N.png" height="120"/>
        <img src="images/Klemme_VZ455N.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_VZ455N.kicad_sym">Klemme VZ455N</a></figcaption></h3>    
        <p></p>
        <h4>Clamp VZ455N</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_KN06E.png" height="120"/>
        <img src="images/Klemmblock_KN06E.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_KN06E.kicad_symKlemmblock KN06E">Klemmblock KN06E</a></figcaption></h3>    
        <p></p>
        <h4>Clamp KN06E</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_KN10E.png" height="120"/>
        <img src="images/Klemmblock_KN10E.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_KN10E.kicad_sym">Klemmblock KN10E</a></figcaption></h3>    
        <p></p>
        <h4>Clamp KN10E</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_KN14E.png" height="120"/>
        <img src="images/Klemmblock_KN14E.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_KN14E.kicad_sym">Klemmblock KN14E</a></figcaption></h3>    
        <p></p>
        <h4>Clamp KN14E</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_KN18E.png" height="120"/>
        <img src="images/Klemmblock_KN18E.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_KN18E.kicad_sym">Klemmblock KN18E</a></figcaption></h3>    
        <p></p>
        <h4>Clamp KN18E</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_KN22E.png" height="120"/>
        <img src="images/Klemmblock_KN22E.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_KN22E.kicad_sym">Klemmblock KN22E</a></figcaption></h3>    
        <p></p>
        <h4>Clamp KN22E</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Clamp_KN26E.png" height="120"/>
        <img src="images/Klemmblock_KN26E.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Clamp_KN26E.kicad_sym">Klemmblock KN26E</a></figcaption></h3>    
        <p></p>
        <h4>Clamp KN26E</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
        <li>
        <figure>  
          <img src="images/wago.png" height="120"/>
        <img src="images/WAGO_Klemmen_2273.png" title="WAGO Klemmen 2273" height="120"/>
        <img src="images/WAGO_Klemmen_221-61.png" title="WAGO Klemmen 221-61x" height="120"/>
        <img src="images/WAGO_Durchgangsverbinder_221-2411.png" title="WAGO Durchgangsverbinder 221-2411" height="120"/>
        <img src="images/WAGO_Doppelleuchtenklemme.png" title="WAGO Doppelleuchtenklemme 2p" height="120"/>
          <h3><figcaption><a href="public/Power_supply/Wago.kicad_sym">Wago</a></figcaption></h3>
<p><b>WAGO-Klemmen/WAGO-Steckklemmen:</b> Diese sind kompakte elektrische Verbindungselemente, die zum Verbinden von elektrischen Leitern ohne Werkzeug verwendet werden können. Sie bieten eine zuverlässige und wartungsfreie Verbindung.</p><p>Es gibt verschiedene Ausführungen von WAGO-Klemmen. Um die Handhabung zu erleichtern, habe ich sie als Knotenpunkte dargestellt, wobei jeder Leiter eine eigene Farbe hat.</p>
<h4></h4>
<p><b>WAGO terminals/WAGO lever clamps:</b> are compact electrical connection components that can be used to connect electrical conductors without the need for tools. They provide a reliable and maintenance-free connection.</p>
<p>There are various versions of WAGO terminals. To simplify, I've represented them as nodes, assigning a distinct color to each conductor.</p>
        </figure>
        </li> 
        [Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Through_terminal.png" height="120"/>
        <img src="images/Durchgangsklemme.png" height="120"/>
        <img src="images/Reihenklemmen_Hutschiene__0_5-4_mm__Kabeldurchmesser.png" height="120"/>
        <h3><figcaption><a href="public/Clamp/Through_terminal.kicad_sym">Durchgangsklemme</a></figcaption></h3>    
        <p></p>
        <h4>Through terminal</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> <!--
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
[Zum Anfang](#top)/[above](#top)<hr> -->
</ul>
