<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Diverses</h1></th>
<th width="50%"><h1>Miscellaneous</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
        <p>Alles was ich nicht so richtig zuordnen kann.</p>
      </td>
      <td>
        <p>Everything I can't quite place.</p>
      </td>
  </tr> 
  <tr>
      <td></td>
      <td></td>
  </tr><!---->
  <tbody>
</table>
<h2></h2>
<ul>
<li>
        <figure>  
        <img src="images/Junction_box.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Junction_box.kicad_sym">Abzweigdose</a></figcaption></h3>    
        <p>Eine elektrische Abzweigdose ist eine Vorrichtung, die in elektrischen Installationen verwendet wird, um Leitungen zu verbinden, zu verzweigen oder zu verteilen.</p><p>Es ist wichtig zu beachten, dass die Installation und Verwendung von Abzweigdosen den örtlichen elektrischen Vorschriften entsprechen muss, um die Sicherheit zu gewährleisten.</p><p> Ich habe zwei Varianten erstellt: Variante (A) ohne Klemmen. Variante (B) mit Klemmen.</p>
        <h4>Junction box</h4>      
        <p>An electrical junction box is a device used in electrical installations to connect, branch or distribute lines.</p><p>It is important to note that the installation and use of junction boxes is subject to local regulations must comply with electrical regulations to ensure safety.</p><p> I created two variants: Variant (A) without terminals. Variant (B) with clamps.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Counter_3p.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Counter_3p.kicad_sym">Zähler</a></figcaption></h3>    
        <p>Ein 3-phasiger Stromzähler ist ein elektrisches Messgerät, das dazu dient, die elektrische Energie in einem dreiphasigen Stromsystem zu messen. Der gemessene Stromverbrauch wird in Kilowattstunden (kWh) angezeigt.</p><p>Für die verschiedenen Netz-Systeme kann man durch doppel-klick die Pinbezeichnung ändern.</p>
        <h4>Counter</h4>      
        <p>A 3-phase electricity meter is an electrical measuring device used to measure electrical energy in a three-phase power system. The measured electricity consumption is displayed in kilowatt hours (kWh).</p><p>You can change the pin name for the different network systems by double-clicking.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Counter_1p.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Counter_1p.kicad_sym">Zähler</a></figcaption></h3>    
        <p>Ein Einphasen-Stromzähler ist für die Messung des Energieverbrauchs in einem einzelnen Stromkreis oder einer Phase ausgelegt. Dies ist typisch für Haushalte und kleinere elektrische Anwendungen.</p><p>Für die verschiedenen Netz-Systeme kann man durch doppel-klick die Pinbezeichnung ändern.</p>
        <h4>Counter</h4>      
        <p></p>A single-phase electricity meter is designed to measure energy consumption in a single circuit or phase. This is typical for households and smaller electrical applications.<p>You can change the pin name for the different network systems by double-clicking.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Room_thermostat.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Room_thermostat.kicad_sym">Raumthermostat</a></figcaption></h3>    
        <p>Ein Raumthermostat ist ein Gerät, das in Heizungs-, Lüftungs- und Klimaanlagensystemen (HLK) eingesetzt wird, um die Raumtemperatur zu messen und zu steuern. Dieser Sensor kann verschiedene Technologien wie Bimetall, elektronische Sensoren oder Halbleiter verwenden.</p>
        <h4>Room thermostat</h4>      
        <p>A room thermostat is a device used in heating, ventilation and air conditioning (HVAC) systems to measure and control room temperature. This sensor can use different technologies such as bimetal, electronic sensors or semiconductors.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Room_thermostat_NFC.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Room_thermostat_NFC.kicad_sym">Raumthermostat</a></figcaption></h3>    
        <p>Die Integration von NFC ermöglicht es, den Raumthermostat drahtlos über kurze Distanzen mit anderen NFC-fähigen Geräten, wie z. B. Smartphones oder Tablets, zu kommunizieren. Dies kann es dem Benutzer ermöglichen, den Raumthermostat von unterwegs zu steuern.
Moderne Raumthermostate können über verschiedene Sensoren verfügen, wie z. B. Temperatur-, Feuchtigkeits- oder Bewegungssensoren, um die Temperaturregelung noch präziser zu gestalten.</p>
        <h4>Room thermostat</h4>      
        <p>The integration of NFC makes it possible to connect the room thermostat wirelessly over short distances to other NFC-enabled devices, such as: E.g. smartphones or tablets to communicate. This can allow the user to control the room thermostat on the go.
Modern room thermostats can have various sensors, such as: B. Temperature, humidity or motion sensors to make temperature control even more precise.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Heater_actuator.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Heater_actuator.kicad_sym">Heizung Stellantrieb</a></figcaption></h3>    
        <p>Ein Heizung-Stellantrieb (auch als Thermostatventilantrieb oder Heizungsventilantrieb bezeichnet) ist eine elektrische Vorrichtung, die dazu dient, Heizkörper oder Fußbodenheizungen in einem Raum zu steuern.</p>
        <h4>Heater actuator</h4>      
        <p>A heating actuator (also known as a thermostatic valve actuator or heater valve actuator) is an electrical device used to control radiators or underfloor heating systems in a room.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Temperature_sensor.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Temperature_sensor.kicad_sym">Temperaturfühler</a></figcaption></h3>    
        <p><img src="images/YouTube.png"/><a href="https://www.youtube.com/watch?v=snS_qNkcomE">Wie sind klassische Kabelfühler aufgebaut?</a></p>
        <h4>Temperature sensor</h4>      
        <p><img src="images/YouTube.png"/><a href="https://www.youtube.com/watch?v=qxEclOy6jpI">DS18B20 Temperature Sensor Tutorial</a></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Automatic_emergency_power_switch_RSTN.png" height="120"/><img src="images/Automatischer_Notstromschalter_RSTN.png" height="120"/>
        <h3><figcaption><a href="images/Automatic_emergency_power_switch_RSTN.png">Automatischer Notstromschalter RSTN</a></figcaption></h3>    
        <p>Der RSTN Automatischer Transferschalter ermöglicht einen automatischen Wechsel zwischen zwei Stromquellen, typischerweise dem normalen Netzstrom und einer Notstromquelle wie einer USV (Unterbrechungsfreie Stromversorgung) oder einem Wechselrichter.</p>
        <h4>Automatic emergency power switch RSTN</h4>      
        <p>The RSTN Automatic Transfer Switch enables automatic switching between two power sources, typically regular mains power and an emergency power source such as a UPS (Uninterruptible Power Supply) or an inverter.</p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Automatic_emergency_power_switch_RST.png" height="120"/>
        <img src="images/Automatischer_Notstromschalter_RST.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Automatic_emergency_power_switch_RST.kicad_sym">Notstrom Umschalter</a></figcaption></h3>    
        <p></p>
        <h4>Automatic emergency power switch RST</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Automatic_emergency_power_switch.png" height="120"/><img src="images/Automatischer_Notstromschalter.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Automatic_emergency_power_switch.kicad_sym">Notstrom Umschalter</a></figcaption></h3>    
        <p></p>
        <h4>Automatic emergency power switch</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Beacon.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Beacon.kicad_sym"></a>Rotierendes Licht</figcaption></h3>    
        <p></p>
        <h4>Beacon</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Busser.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Busser.kicad_sym">Summer</a></figcaption></h3>    
        <p></p>
        <h4>Busser</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Siren.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Siren.kicad_sym"></a>Sirene</figcaption></h3>    
        <p></p>
        <h4>Siren</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Horn.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Horn.kicad_sym">Horn</a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Gong.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Gong.kicad_sym">Klingel</a></figcaption></h3>    
        <p></p>
        <h4>Gong</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Lamp.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Lamp.kicad_sym">Leuchte</a></figcaption></h3>    
        <p></p>
        <h4>Lamp</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Lamp_3p.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Lamp_3p.kicad_sym">Leuchte 3p</a></figcaption></h3>    
        <p></p>
        <h4>Lamp 3p</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Pilot_Light.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Pilot_Light.kicad_sym">Zündflamme</a></figcaption></h3>    
        <p></p>
        <h4>Pilot Light</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>   
<li>
        <figure>  
        <img src="images/Pilot_Light_Blink.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Pilot_Light_Blink.kicad_sym">Kontrollleuchte blinkt</a></figcaption></h3>    
        <p></p>
        <h4>Pilot Light Blink</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Pilot_Light_Transformer.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Pilot_Light_Transformer.kicad_sym">Pilotlichttransformator</a></figcaption></h3>    
        <p></p>
        <h4>Pilot Light Transformer</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/rcbo_2p.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/rcbo_2p.kicad_sym">RCBO</a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/rcbo_4p.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/rcbo_4p.kicad_sym">RCBO</a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>    
<li>
        <figure>  
        <img src="images/Power_outlet.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Power_outlet.kicad_sym">Steckdose</a></figcaption></h3>    
        <p></p>
        <h4>Power outlet</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <p><a href="https://youtu.be/LO0q51o6Kgw"><img src="images/Leitungen_in_der_Gebaeudeinstallation.png" height="200"/></a>
        <h3><figcaption>YouTube, Leitungen in der Gebäudeinstallation</figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Neutral_conductor_N.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Neutralleiter_N.kicad_sym">Neutralleiter N</a></figcaption></h3>    
        <p></p>
        <h4>Neutral conductor N</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Neutral_conductor_with_PEN_protection_function.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Neutral_conductor_with_PEN_protection_function.kicad_sym">Neutralleiter mit Schutzfunktion PEN</a></figcaption></h3>    
        <p></p>
        <h4>Neutral conductor with PEN protection function</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/PE_protective_conductor.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/PE_protective_conductor.kicad_sym">Schutzleiter PE</a></figcaption></h3>    
        <p></p>
        <h4>PE protective conductor</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Outside_ladder.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Outside_ladder.kicad_sym">Außen Leiter</a></figcaption></h3>    
        <p></p>
        <h4>Outside ladder</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Three_outside_conductors.png" height="120"/>
        <h3><figcaption><a href="public/Miscellaneous/Three_outside_conductors.kicad_sym">Drei Außen Leiter</a></figcaption></h3>    
        <p></p>
        <h4>Three outside conductors</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> <!--
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> -->
</ul>
