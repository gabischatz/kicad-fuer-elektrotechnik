<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Flugplatzbefeuerung und Anzeiger</h1></th>
<th width="50%"><h1>Feux et indicateurs de navigation pour aéroports</h1></th>
</tr>
</thead><tbody><!--
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> 
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.18 </h2>
<ul>
<li>
        <figure>  
        <img src="images/11-18-01.png" height="120"/>
        <h3><figcaption><a href="11-18-01.kicad_sym">Flugplatzfeuer, Überflur, allgemein </a></figcaption></h3>    
        <p>11-18-01</p>
        <h4>Feu aéronautique au sol, en hauteur</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-02.png" height="120"/>
        <h3><figcaption><a href="11-18-02.kicad_sym">Flugplatzfeuer, Unterflur, allgemein</a></figcaption></h3>    
        <p></p>
        <h4>Feu aéronautique au sol, en surface</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-03.png" height="120"/>
        <h3><figcaption><a href="11-18-03.kicad_sym">Flugplatzfeuer, weiss, einseitig strahlend</a></figcaption></h3>    
        <p>11-18-03</p>
        <h4>Feu aéronautique au sol, faisceau blanc et unidirectionnel (en hauteur)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/1-18-04.png" height="120"/>
        <h3><figcaption><a href="11-18-04.kicad_sym">Flugplatzfeuer, weiss, einseitig strahlend </a></figcaption></h3>    
        <p>1-18-04</p>
        <h4>Feu aéronautique au sol, faisceau blanc et unidirectionnel (en surface)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-05.png" height="120"/>
        <h3><figcaption><a href="11-18-05.kicad_sym">Flugplatzfeuer,weiss/weiss, zweiseitig strahlend;</a></figcaption></h3>    
        <p>11-18-05</p>
        <h4>Feu aéronautique au sol, faisceau blanc/blanc et bidirectionnel (en hauteur)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/10-10-06.png" height="120"/>
        <h3><figcaption><a href="11-18-06.kicad_sym">Flugplatzfeuer,weiss/weiss, zweiseitig strahlend</a></figcaption></h3>    
        <p>11-18-06</p>
        <h4>Feu aéronautique au sol, faisceau blanc/blanc et bidirectionnel (en surface)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-07.png" height="120"/>
        <h3><figcaption><a href="11-18-07.kicad_sym">Flugplatzfeuer, weiss, rundstrahlend</a></figcaption></h3>    
        <p>11-18-07</p>
        <h4>Feu aéronautique au sol, faisceau blanc et omnidirectionnel (en hauteur)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-08.png" height="120"/>
        <h3><figcaption><a href="11-18-08.kicad_sym"></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-09.png" height="120"/>
        <h3><figcaption><a href="11-18-09.kicad_sym">Kurvenfeuer, grün/grün, zweiseitig strahlend</a></figcaption></h3>    
        <p>11-18-09</p>
        <h4>Feu de virage, faisceau vert/vert et bidirectionnel (en surface)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-10.png" height="120"/>
        <h3><figcaption><a href="11-18-10.kicad_sym">Kurvenfeuer, weiss, einseitig strahlend</a></figcaption></h3>    
        <p>11-18-10</p>
        <h4>Feu de virage, faisceau blanc et unidirectionnel (en surface)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-11.png" height="120"/>
        <h3><figcaption><a href="11-18-11.kicad_sym">Flugplatzfeuer, oben weisses rundstrahlendes Licht, unten weisses einseitig strahlendes Licht</a></figcaption></h3>    
        <p>11-18-11</p>
        <h4>Feu aéronautique au sol, faisceau omnidirectionnel blanc en haut et unidirectionnel blanc en bas (en hauteur)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-12.png" height="120"/>
        <h3><figcaption><a href="11-18-12.kicad_sym">Flugplatzfeuer, oben weisses rundstrahlendes Licht, unten weiss/weisses zweiseitig strahlendes Licht</a></figcaption></h3>    
        <p></p>
        <h4>Feu aéronautique au sol, faisceau omnidirectionnel blanc en haut et bidirectionnel blanc/blanc en bas (en hauteur)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-13.png" height="120"/>
        <h3><figcaption><a href="11-18-13.kicad_sym">Flugplatzfeuer, weiss blitzend, einseitig strahlend</a></figcaption></h3>    
        <p>11-18-13</p>
        <h4>Feu d'approche à éclats, faisceau unidirectionnel blanc (en hauteur)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-14.png" height="120"/>
        <h3><figcaption><a href="11-18-14.kicad_sym">Flugplatzfeuer, weiss blitzend, einseitig strahlend</a></figcaption></h3>    
        <p>11-18-14</p>
        <h4>Feu d'approche à éclats, faisceau unidirectionnel blanc (en surface)</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-15.png" height="120"/>
        <h3><figcaption><a href="11-18-15.kicad_sym">Anfluggleitwinkelfeuer, weiss/rot, einseitig strahlend</a></figcaption></h3>    
        <p>11-18-15</p>
        <h4>Indicateur de trajectoire d'approche de précision, faisceau unidirectionnel blanc/rouge</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-16.png" height="120"/>
        <h3><figcaption><a href="11-18-16.kicad_sym">Windrichtungsanzeiger</a></figcaption></h3>    
        <p>11-18-16</p>
        <h4>Indicateur de direction du vent</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-17.png" height="120"/>
        <h3><figcaption><a href="11-18-17.kicad_sym">Landerichtungsanzeiger</a></figcaption></h3>    
        <p>11-18-17</p>
        <h4>Indicateur de direction d'atterrissage</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-18.png" height="120"/>
        <h3><figcaption><a href="11-18-18.kicad_sym">Hindernisfeuer,Gefahrenfeuer, rot blitzend, rund strahlend</a></figcaption></h3>    
        <p>11-18-18</p>
        <h4>Feu d'obstacle, Feu de danger, faisceau omnidirectionnel, à éclats, rouge</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-19.png" height="120"/>
        <h3><figcaption><a href="11-18-19.kicad_sym">Flugplatzfeuer, weiss blitzend, rund strahlend</a></figcaption></h3>    
        <p>11-18-19</p>
        <h4>Feu aéronautique au sol, faisceau omnidirectionnel, à éclats, blanc</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-20.png" height="120"/>
        <h3><figcaption><a href="11-18-20.kicad_sym">Verkehrszeichen, Wegweiser, allgemein</a></figcaption></h3>    
        <p>11-18-20</p>
        <h4>Plaque d'avertissement Panneau de guidage, symbole général</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-21.png" height="120"/>
        <h3><figcaption><a href="images/11-18-21.png">Verkehrszeichen, Wegweiser, 4000/9000 feet</a></figcaption></h3>    
        <p>11-18-21</p>
        <h4>Plaque d'avertissement de distance: "4000/9000 pieds"</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-18-22.png" height="120"/>
        <h3><figcaption><a href="11-18-22.kicad_sym">Verkehrszeichen, Wegweiser, RAMP</a></figcaption></h3>    
        <p>11-18-22</p>
        <h4>Panneau de guidage pour le roulage: "RAMP"</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
</ul>
