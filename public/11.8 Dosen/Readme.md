<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Dosen</h1></th>
<th width="50%"><h1>Dérivations d'usager et prises du réseau</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.8</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-08-01.png" height="120"/>
        <h3><figcaption><a href="11-08-01.kicad_sym">Abzweigdose, allgemein</a></figcaption></h3>    
        <p>11-08-01</p>
        <h4>Dérivateur d'usager</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-08-02.png" height="120"/>
        <h3><figcaption><a href="11-08-02.kicad_sym">Stichdose</a></figcaption></h3>    
        <p>11-08-02</p>
        <h4>Prise d'usager</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-08-03.png" height="120"/>
        <h3><figcaption><a href="11-08-03.kicad_sym">Durchschleifdose</a></figcaption></h3>    
        <p>11-08-03</p>
        <h4>Prise directe, Prise boîte à boîte</h4>      
        <p></p>
        </figure>
</li></ul>
