<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Einspeisungen</h1></th>
<th width="50%"><h1>Canalisations</h1></th>
</tr>
</thead>
</table>
<h2>11.12</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-12-01.png" height="120"/>
        <h3><figcaption><a href="11-12-01.kicad_sym">Leitung, nach oben führend</a></figcaption></h3>    
        <p>11-12-01</p>
        <h4>Canalisation montante</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-12-02.png" height="120"/>
        <h3><figcaption><a href="11-12-02.kicad_sym">Leitung, nach unten führend</a></figcaption></h3>    
        <p>11-12-02</p>
        <h4>Canalisation descendante</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-12-03.png" height="120"/>
        <h3><figcaption><a href="11-12-03.kicad_sym">Leitung, nach unten und oben durchführend</a></figcaption></h3>    
        <p>11-12-03</p>
        <h4>Canalisation traversant verticalement</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-12-04.png" height="120"/>
        <h3><figcaption><a href="11-12-04.kicad_sym">Dose, Leerdose, allgemein</a></figcaption></h3>    
        <p>11-12-04</p>
        <h4>Boîte, symbole général</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-12-05.png" height="120"/>
        <h3><figcaption><a href="11-12-05.kicad_sym">Anschlußdose, Verbindungsdose</a></figcaption></h3>    
        <p>11-12-05</p>
        <h4>Boîte de connexions, Boîte de dérivation</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-12-06.png" height="120"/>
        <h3><figcaption><a href="11-12-06.kicad_sym">Hausanschlußkasten, allgemein, dargestellt mit Leitung</a></figcaption></h3>    
        <p>11-12-06</p>
        <h4>Coffret de branchement</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-12-07.png" height="120"/>
        <h3><figcaption><a href="11-12-07.kicad_sym">Verteiler</a></figcaption></h3>    
        <p>11-12-07</p>
        <h4>Coffret de répartition</h4>      
        <p></p>
        </figure>
</li> 
</ul>
