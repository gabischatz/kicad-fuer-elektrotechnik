<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Entzerrer und Dämpfungsglied</h1></th>
<th width="50%"><h1>Égaliseurs et affaiblisseurs</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.9</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-09-01.png" height="120"/>
        <h3><figcaption><a href="11-09-01.kicad_sym">Entzerrer</a></figcaption></h3>    
        <p>11-09-01</p>
        <h4>Egaliseur</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-09-02.png" height="120"/>
        <h3><figcaption><a href="11-09-02.kicad_sym">Entzerrer, veränderbar</a></figcaption></h3>    
        <p>11-09-02</p>
        <h4>Egaliseur variable</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-09-03.png" height="120"/>
        <h3><figcaption><a href="11-09-03.kicad_sym">Dämpfungsglied (zur Darstellung auf einem topographischen Plan)</a></figcaption></h3>    
        <p>11-09-03</p>
        <h4>Affaiblisseur</h4>      
        <p></p>
        </figure>
</li></ul>
