<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Fern-Stromversorgungen</h1></th>
<th width="50%"><h1>Dispositifs d'alimentation</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
      </td>
      <td>
      </td>
  </tr> <!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2>11.10</h2>
<ul>
<li>
        <figure>  
        <img src="images/11-10-01.png" height="120"/>
        <h3><figcaption><a href="11-10-01.kicad_sym">Fernspeise-Netzgerät</a></figcaption></h3>    
        <p>11-10-01</p>
        <h4>Dispositif d'alimentation de ligne</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-10-02.png" height="120"/>
        <h3><figcaption><a href="11-10-02.kicad_sym">Trennstelle</a></figcaption></h3>    
        <p>11-10-02</p>
        <h4>Dispositif de blocage d'alimentation</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/11-10-03.png" height="120"/>
        <h3><figcaption><a href="11-10-03.kicad_sym">Einspeisepunkt</a></figcaption></h3>    
        <p>11-10-03</p>
        <h4>Point d'injection de l'alimentation</h4>      
        <p></p>
        </figure>
</li> 
</ul>
