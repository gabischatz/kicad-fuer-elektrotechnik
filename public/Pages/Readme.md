<link rel="stylesheet" href="public/style.css">
<nav id="top">
   <ul>
      <li>[Deutsch](#seitendesigns) </li>
      <li>[English](#page-designs) </li>
  </ul>
</nav>

<h1>Seitendesigns</h1>
<p>Unten sehen Sie eine umfassende Liste der am häufigsten genutzten Layoutdateien für Seitendesigns, aufgeteilt in zwei Abschnitte: Deutsche und Englische Layoutdateien. Wenn Sie interessiert sind, können Sie auf jede Vorlage klicken, um zur Download-Seite zu gelangen und die <b>kicad_sym-Datei</b> jeder Vorlage direkt für Ihre Projekte in Entwürfen und Diagrammen herunterzuladen.</p>
<h2>Deutsch</h2>
            <figure>  
          <img src="images/Elektrik_A_5.png" />
          <figcaption>
          </figcaption>
          <p align="center">
         <table align="center">
  <tr>
        <td>
                <ul>
                <li><dl><dt>Erstellungsdatum:</dt><dd>Erstellt: ${ISSUE_DATE}</dd></dl></li>
                <li><dl><dt>Revision:</dt><dd>Zeichen Nr.:${REVISION}</dd></dl></li>
                <li><dl><dt>Titel:</dt><dd>${TITLE}</dd></dl></li>
                <li><dl><dt>Firma:</dt><dd>${COMPANY}</dd></dl></li>
                <li><dl><dt>Bezeichnung </dt><dd>${COMMENT1}</dd></dl></li>
                </ul>
        </td>
        <td>
                <ul>
                <li><dl><dt>Lfd-Nr. der Baugruppe</dt><dd>${COMMENT2}</dd></dl></li>
                <li><dl><dt>Seite der Baugruppe </dt><dd>${COMMENT3}</dd></dl></li>
                <li><dl><dt>Anzahl der Seiten der Baugruppe</dt><dd>${COMMENT4}</dd></dl></li>
                <li><dl><dt>Ersteller-/Urhebername</dt><dd>Von: ${COMMENT5}</dd></dl></li>
                <li><dl><dt>Kommissions-Nr.</dt><dd>Kom-Nr. ${COMMENT6}</dd></dl></li>
                </ul>
        </td>
        <td>        
                <ul>
                <li><dl><dt>Geändert Name</dt><dd>Von: ${COMMENT7}</dd></dl></li>
                <li><dl><dt>Datum Auftrag</dt><dd>Datum: ${COMMENT8}</dd></dl></li>
                <li><dl><dt>Auftraggeber Name</dt><dd>Gepr: ${COMMENT9}</dd></dl></li>
                <li><dl><dt>Seite im Dokument</dt><dd>${#}</dd></dl></li>
                <li><dl><dt>Anzahl der Seiten im Dokument</dt><dd>${##}</dd></dl></li>
                </ul>
        </td>
  </tr>
</table>
</p>
</figure>   
    <hr>
<ul>     
    <li>    
        <figure>  
          <img src="images/Elektrik_A5.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Elektrik_A5.kicad_wks">Querformat A5</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Elektrik_A4_Deckblatt_Heftrand.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Elektrik_A4_Deckblatt.kicad_wks">Deckblatt Querformat A4 mit Heftrand</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Elektrik_A4_Heftrand.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Elektrik_A4_Heftrand.kicad_wks">Querformat A4 mit Heftrand</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Elektrik_A4.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Elektrik_A4.kicad_wks">Querformat A4</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Elektrik_A4_Hochkant.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Elektrik_A4_Hochkant.kicad_wks">Hochkant Format A4</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Elektrik_A3_Deckblatt.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Elektrik_A3_Deckblatt.kicad_wks">Deckblatt Querformat A3</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Elektrik_A3.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Elektrik_A3.kicad_wks">Querformat A3</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
</ul>
[oben](#top)
<h1>Page designs</h1>
<p>Below you can see a comprehensive list of the most commonly used layout files for page designs, divided into two sections: German and English layout files. If you are interested, you can click on each template to go to the download page and download the <b>kicad_sym file</b> of each template directly for your projects in designs and diagrams.</p>

<h2>English</h2>
<ul>
    <li>
    <hr>
        <figure>  
          <img src="images/Elektrik_A_5.png"/>
          <figcaption></figcaption><
        <table>
                <tr>
                <td><ul>
          <li><dl><dt>Creation Date:</dt><dd>Created: ${ISSUE_DATE}</dd></dl></li>
          <li><dl><dt>Revision:</dt><dd>Mark Nr.:${REVISION}</dd></dl></li>
          <li><dl><dt>Title:</dt><dd>${TITLE}</dd></dl></li>
          <li><dl><dt>Company:</dt><dd>${COMPANY}</dd></dl></li>
          <li><dl><dt>Description</dt><dd>${COMMENT1}</dd></dl></li>
          </ul></td>
                <td><ul>
          <li><dl><dt>Serial no. the assembly</dt><dd>${COMMENT2}</dd></dl></li>
          <li><dl><dt>Side of the assembly</dt><dd>${COMMENT3}</dd></dl></li>
          <li><dl><dt>Number of pages of the assembly</dt><dd>${COMMENT4}</dd></dl></li>
          <li><dl><dt>Creator/author name</dt><dd>From: ${COMMENT5}</dd></dl></li>
          <li><dl><dt>Kommissionsnr.</dt><dd>Com-Nr. ${COMMENT6}</dd></dl></li>
          </ul></td>
                <td><ul>
          <li><dl><dt>Changed name</dt><dd>From: ${COMMENT7}</dd></dl></li>
          <li><dl><dt>Date order</dt><dd>Date: ${COMMENT8}</dd></dl></li>
          <li><dl><dt>Client name</dt><dd>Tested: ${COMMENT9}</dd></dl></li>
          <li><dl><dt>Page in the document</dt><dd>${#}</dd></dl></li>
          <li><dl><dt>Number of pages in the document</dt><dd>${##}</dd></dl></li>
          </ul></td>
                </tr>
        </table>          
        </figure>            
        <figure>  
          <img src="images/Elektrik_A5.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Elektrik_A5.kicad_wks">Landscape format A5 German</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Electrical_engineering_A4_cover_sheet.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Electrical_engineering_A4_cover_sheet.kicad_wks">Landscape format A4</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Electrical_engineering_A4_sheet.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Electrical_engineering_A4.kicad_wks">Landscape format A4</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Electrical_engineering_A3_cover_sheet.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Electrical_engineering_A3_cover_sheet.kicad_wks">Landscape format A3</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <li>
            <figure>  
          <img src="images/Electrical_engineering_A3_sheet.png" height="160"/>
          <h3><figcaption><a href="public/Pages/Electrical_engineering_A3.kicad_wks">Landscape format A3</a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    <!--
    <li>
            <figure>  
          <img src="" height="160"/>
          <h3><figcaption><a href=""></a></figcaption></h3>
<h4> </h4>  
        </figure>
<p></p>
    </li>
    <hr>
    -->
</ul>
[above](#top)
