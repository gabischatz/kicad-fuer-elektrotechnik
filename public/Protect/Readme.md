<table width="100%">
<thead>
<tr>
<th width="50%"><h1>Schütze</h1></th>
<th width="50%"><h1>Protect</h1></th>
</tr>
</thead><tbody>
  <tr>
      <td>
        <p>Ein Schütz ist ein elektrisches Schaltgerät, das dazu dient, einen elektrischen Stromkreis zu öffnen oder zu schließen. Es wird häufig in industriellen Anwendungen eingesetzt, um elektrische Motoren, Heizungen, Beleuchtungssysteme und andere elektrische Lasten zu steuern. Schütze sind Teil von Schaltanlagen und elektrischen Steuersystemen.</p>
      </td>
      <td>
 <p>A contactor is an electrical switching device that is used to open or close an electrical circuit. It is widely used in industrial applications to control electric motors, heaters, lighting systems and other electrical loads. Contactors are part of switchgear and electrical control systems.</p>       
      </td>
  </tr> 
  <tr>
      <td> 
<h2>Schütz Bezeichnungen</h2>     
       <figure>  
        <img src="images/Contactor_designation.png" height="120"/>
        <figcaption>
        <h3><a href=""></a>E01 => </h3>    
        <p>0 Schließer und 1 Öffner</p>
        </figcaption>
        <figcaption>
        <h3>E24 => </h3>      
        <p>2 Schließer und 4 Öffner</p>
        </figcaption>
        </figure></td>
      <td>      
<h2>Contact designations</h2>  
       <figure>  
        <img src="images/Contactor-designation.png" height="120"/>
        <figcaption><h3><a href=""></a>E01 => </h3>    
        <p>0 NO and 1 NC</p></figcaption>
        <figcaption><h4>E24 => </h4>      
        <p>2 NO contacts and 4 NC contacts</p>
        </figcaption>
        </figure>
      </td>
  </tr><!--
  <tr>
      <td></td>
      <td>[](#)</td>
  </tr>
  <tr>
      <td>[](#)</td>
      <td>[](#)</td>
  </tr>-->
  <tbody>
</table>
<h2><img src="images/Protects.png"/></h2>
<p><a href="https://youtu.be/K9c-k_ksNzs"><img src="images/Hilfsschalterbaustein_erstellen.png" height="200"/><br> YouTube Hilfsschalterbaustein 2p, 1NC, 1NO, erstellen.</p>
<ul>
<li>
       
<li>
        <figure>  
        <img src="images/Protects_main_contacts.png" height="120"/>
        <h3><figcaption><a href="public/Protect/Protects_main_contacts.kicad_sym">Schütz Hauptkontakte</a></figcaption></h3>    
        <p></p>
        <h4>Protects main contacts NC</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Protects_main_contacts_NO.png" height="120"/>
        <h3><figcaption><a href="public/Protect/Protects_main_contacts_NO.kicad_sym">Schütz Hauptkontakte</a></figcaption></h3>    
        <p></p>
        <h4>Protects main contacts NO</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Protects_main_contacts_4p.png" height="120"/>
        <h3><figcaption><a href="public/Protect/protects_main_contacts_4p.kicad_sym">Schütz Hauptkontahte 4p</a></figcaption></h3>    
        <p></p>
        <h4>Protects main contacts 4p NC</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Protects_main_contacts_4p_NO.png" height="120"/>
        <h3><figcaption><a href="public/Protect/Protects_main_contacts_4p_NO.kicad_sym">Schütz Hauptkontahte 4p NO</a></figcaption></h3>    
        <p></p>
        <h4>Protects main contacts 4p NO</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Protects_tax_accounts_1p_NC.png" height="120"/>
        <h3><figcaption><a href="public/Protect/Protects_tax_accounts_1p_NC.kicad_sym">Shütz Steuerkontakt Schließer</a></figcaption></h3>    
        <p></p>
        <h4>Protects tax accounts 1p NC</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>
<li>
        <figure>  
        <img src="images/Protects_tax_accounts_1p_NO.png" height="120"/>
        <h3><figcaption><a href="public/Protect/Protects_tax_accounts_1p_NO.kicad_sym">Shütz Steuerkontakt Öffner</a></figcaption></h3>    
        <p></p>
        <h4>Protects tax account NO</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Protect_tax_accounts_4p_2NC_2NO.png" height="120"/>
        <h3><figcaption><a href="public/Protect/Protect_tax_accounts_4p_2NC_2NO.kicad_sym">Shütz Steuerkontakte</a></figcaption></h3>    
        <p></p>
        <h4>Protect tax accounts</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr>  
<li>
        <figure>  
        <img src="images/Protect_auxiliary_switch_module_2p_1NC_1NO.png" height="120"/>
        <img src="images/Hilfsschalterbaustein_2p_1NC_1NO.png" title="Hilfsschalterbaustein 2p 1NC 1NO" height="120"/>
        <h3><figcaption><a href="public/Protect/Protect_auxiliary_switch_module_2p_1NC_1NO.kicad_sym">Shütz Hilfsschalterbaustein 2p 1NC 1NO</a></figcaption></h3>    
        <p></p>
        <h4>Protect auxiliary switch module 2p 1NC 1NO</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Protect_auxiliary_switch_module_2p_1NO_1NC.png" height="120"/>
        <img src="images/Schuetz_LA8-DN11_F8-11_Hilfs_kontakt.png" height="120"/>
        <img src="images/Eaton_Z-HK_Hilfsschalter.png" height="120"/>
        <h3><figcaption><a href="public/Protect/Protect_auxiliary_switch_module_2p_1NO_1NC.kicad_sym">Shütz Hilfsschalterbaustein 2p 1NO 1NC</a></figcaption></h3>    
        <p></p>
        <h4>Protect auxiliary switch module 2p 1NO 1NC</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="images/Protect_auxiliary_Timer_module_2p_1NC_1NO_F5-D2.png" height="120"/>
        <img src="images/Hilfsschalterbaustein_Timer.png" height="120"/>
        <h3><figcaption><a href="public/Protect/Protect_auxiliary_Timer_module_2p_1NC_1NO_F5-D2.kicad_sym">Schütz Hilfsschalterbaustein Timer F5-D2</a></figcaption></h3>    
        <p></p>
        <h4>Protect auxiliary Timer module 2p 1NC 1NO F5-D2</h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> <!--
<li>
        <figure>  
        <img src="" height="120"/>
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> 
<li>
        <figure>  
        <img src="" height="120"/>
        <h3><figcaption><a href=""></a></figcaption></h3>    
        <p></p>
        <h4></h4>      
        <p></p>
        </figure>
</li> 
[Zum Anfang](#top)/[above](#top)<hr> -->
</ul>
